\chapter{F\# Implementation}
\label{fs_implementation}

It was considered of interest, beside the use of \aCheck, implementing the list-machine model in a functional language and validate it with a general purpose PBT library based on random testing. F\# is an open source cross-platform functional programming language, belonging to the ML family. The adopted PBT library is FsCheck, a porting of QuickCheck to F\# with the addition of automatic data generators thanks to the reflective capabilities of the host language.

The F\# list-machine implementation, while preserving the same semantics, is quite different from the \aProlog\ one. There, variables and labels are indexed by natural numbers instead of being drawn from finite sets. Moreover, stores, environments and program typing are encoded using maps instead of lists of pairs, so much less code for managing insertion and lookup was necessary, compared to the \aProlog\ implementation. Moreover, typechecking implementation was inspired from the algorithmic one provided in Coq, instead of the inductive relation.

Listings presented in this chapter are not strictly F\# code: they were adapted to be better represented on paper and may not compile. Moreover, type annotations were added to function arguments for documentation purpose. Full implementation code of the list-machine model and checks is available in the project repository on BitBucket, on the \texttt{fsharp} branch\footnote{\url{https://bitbucket.org/fkomauli/list-machine/branch/fsharp}}.

\section{Top-Level Checks}

FsCheck DSL is the same of QuickCheck and thus it provides means to express conditional statements that resemble inference rules written on paper. As can be seen in Listing \ref{lst:list_machine_fscheck_top_level}, we use as checks the theorem statements given in Section \ref{checks}, but with one important difference, that is the presence of explicit generators. Generators are specified through the \texttt{Prop.forAll} function, which lets a proposition use a custom generator instead of a default one.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fscheck_top_level},caption={list-machine top-level checks written with FsCheck}]
// Override the default 100 test iterations of Check.Quick
// to improve likelihood of finding counterexamples
let config = { Config.Quick with MaxTest = 10000 }

Check.One ("progress", config,
  Prop.forAll (Generators.InstructionContext()) (fun (p, pi, r, g, l, i) ->
    (
      (typecheck-program pi p) &&
      (typecheck-block pi g i) &&
      (store-has-type r g)
    ) ==>
      (lazy step-or-halt p r i)
))

Check.One ("preservation", config,
  Prop.forAll (Generators.InstructionContext()) (fun (p, pi, r, g, l, _) ->
    let i = program-lookup l p
    (
      (typecheck-program pi p) &&
      (store-has-type r g) &&
      (typecheck-block pi g i) &&
      (canStep p r i)
    ) ==>
      (lazy let (r', i') = step r i p in
         existsTEnvForStoreAndBlock pi r' i' (updateTEnv pi g i))
))

Check.One ("soundness", config,
  Prop.forAll (Generators.MatchingProgramAndTyping()) (fun (p, pi) ->
    Prop.forAll (arbitrarySteps 50) (fun n ->
      let r = Env (Map.add (Variable.Name 0) Nil Map.empty)
      let i = program-lookup (Label.Id 0) p
      (
        (typecheck-program pi p &&)
        (trySteps p r i n)
      ) ==>
        (lazy let (r', i') = steps p r i n in step-or-halt p r' i')
)))
\end{lstlisting}
\end{figure}

For example, the \emph{progress} check uses  the \texttt{InstructionContext} custom generator, which randomly generates a tuple containing a program \texttt{p}, a program typing \texttt{pi} that typechecks with \texttt{p}, a store \texttt{r} and a compatible environment \texttt{g}, a label \texttt{l} that belongs to program \texttt{p} and the instruction \texttt{i} associated to label \texttt{l}. This tuple is passed to a lambda expression that returns a property using the generated values. Three preconditions are grouped in a boolean disjunction, while the postcondition is preceded by the keyword \texttt{lazy}, so the postcondition predicate is evaluated only if all preconditions are true.

Other properties follow the same definition pattern, sometimes with nested generator calls as in the \emph{soundness} check. In this check, compared to the \aCheck\ implementation, an additional parameter is present that controls the number of steps the machine must do before checking whether it can advance another step forward. The \texttt{arbitrarySteps} generator returns a number \texttt{n} in the range 0-50 (inclusive), while \texttt{steps} applies the step function \texttt{n} times, as shown in Listing \ref{lst:list_machine_fs_steps}.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fs_steps},caption={list-machine \texttt{steps} function}]
let rec steps (p: Program) (r: Environment) (i: Instruction) (n: int)
    : Environment * Instruction =
  if n <= 0
  then (r, i)
  else let (r', i') = step r i p in steps p r' i' (n - 1)
\end{lstlisting}
\end{figure}

This implementation resembles the definition of an indexed big-step semantics in \citep{softwarefoundations}. The \aProlog\ definition of the \emph{steps} relation yields all the machine states encountered after an arbitrary number of steps for a given program and initial state, until \textbf{halt} instruction is reached. However, in a functional setting, the number of steps must be expressed explicitly or a list of successive states must be returned. We opted for the first solution because choosing an arbitrary number of steps was the most trivial implementation.

\section{Naive Default Generators}

As mentioned before, FsCheck takes advantage of F\# reflection to build default generators for user defined datatypes. Default generators are also capable of populating collections present in the language library, such as maps. This feature helps speed up writing checks, until the required data does not have to satisfy strict preconditions and is not related to other generated values in the same property. In fact, random data of default generators is uniformly distributed: in case of certain preconditions, most values are discarded leading to an insufficient amount of input data that weakens check effectiveness. This behavior is clearly described by the checks execution average measures resumed in Table \ref{tbl:list_machine_fscheck_zero_coverage}. Checks were run on the debugged list-machine model and no counterexample was found. Iterations count represents the times input values satisfied the preconditions, before reaching the arguments exhaustion. Average was calculated from statistics collected during 20 executions of each check.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | r | r |}
    \hline
    Check & Iterations & Execution time\\
    \hline
    \hline
    \mbox{\emph{progress} (default generators)} & 0 & 24.48\ s\\
    \hline
    \mbox{\emph{progress} (custom generators)} & 150 & 0.3291\ s\\
    \hline
    \mbox{\emph{preservation} (default generators)} & 0 & 24.65\ s\\
    \hline
    \mbox{\emph{preservation} (custom generators)} & 3742 & 7.577\ s\\
    \hline
    \mbox{\emph{soundness} (default generators)} & 0 & 96.51\ s\\
    \hline
    \mbox{\emph{soundness} (custom generators)} & 1905 & 2.154\ s\\
    \hline
  \end{tabular}
\]
\caption{check execution times using naive default generators}
\label{tbl:list_machine_fscheck_zero_coverage}
\end{table}

Using naive default generators, checks with strong preconditions on input data have their effectiveness undermined by an insufficient number of test iterations. In the case of top-level checks, naive generation of data never supplied values that satisfied the preconditions.

Experimental top-level checks implementation with naive generators had as result a zero or nearly zero test iterations count, because values satisfying all preconditions were too rare. This made necessary the development of custom generators for programs and relative program typings, stores and their matching environments, and instruction with all the surrounding constructs needed to typecheck them.

The major reason for bad coverage of checks using default generators is the correlation that preconditions of some property impose on input values. In the \emph{soundness} check, for example, program and program typing have to satisfy the \texttt{typecheck-program} predicate, and pairs of such values are very rare in the uniform distribution of data. Therefore custom generators yield pairs or tuples of correlated values, designed in a way that the predicates mostly used in preconditions are always satisfied by the generated data.

\section{Smart Generators}

The problem of generating meaningful programs was already treated in \citep{ifc}, where the authors used QuickCheck to validate information-flow control security of a stack register machine. Even in those cases, using naive generators for programs lead to an high discard rate of input data, indicating that machines often failed to reach an halted state. Moreover, the average execution steps were less than $0.5$, meaning that most programs cannot be executed at all. The adopted solution was varying the distribution of instructions to generate programs that are likely to run longer, and thus have a chance to get into machine states where an error may be found.

In our case, the approach is to generate programs where \texttt{cons} instructions are more likely to appear. Such instructions may populate new variables in the store and produce longer chains of cons cells, giving a chance for other instructions to behave correctly, such as the \texttt{fetch-field} instruction, or to have a different behavior, such as the \texttt{branch-if-nil} instruction.

The authors in \citep{ifc} also used an approach they called the \emph{generation by execution} strategy: \emph{``we generate a single instruction or a small sequence of instructions, as before, except that now we restrict generation to instructions that do not cause the machine to crash in the current state''}. This idea was slightly adapted to our machine, which, differently from the above, has a type system. Programs are iteratively generated by adding instructions, but they are not checked whether they cause the machine to crash starting from the initial state. Instead, a program is generated together with a program typing, and every instruction is added only if the typechecking algorithm is satisfied. This generation strategy yields couples of well-typed programs and program typings which always satisfy the \texttt{typecheck-program} predicate.

\subsection{Writing Generators}

FsCheck provides a set of functions that allow writing generators together with a monadic notation very similar to the Haskell \emph{do notation}. However, as explained in \citep{fszoo}, F\# \emph{computation expressions} are not tied to a single abstraction, providing a greater syntactic flexibility compared to the Haskell case. The FsCheck developer exploited this feature to define an implementation of generator environment tied to computation expressions.

Inside the \texttt{gen\{\}} expression, values can be bound from other generators by using the \texttt{let!} keyword, then manipulated and combined to define the generator data distribution. As example, Listing \ref{lst:list_machine_fscheck_block_generator} shows the core of a program generator, which keeps also type information for generating a program typing together with the program. The complete code for FsCheck custom generators is available in the BitBucket repository.

The \texttt{genInstruction} function yields an instruction generator
that randomly chooses an instruction between \texttt{Cons},
\texttt{BranchIfNil} and \texttt{FetchField}, using their relative
custom generators. The instruction is always chosen to be safe if
executed in the given typing environment. With the instruction itself,
the generators produces the environment updated after the execution of
the instruction. The \texttt{BranchIfNil} and \texttt{FetchField}
cannot be always generated safely in relation to the given
environment: in this case the \texttt{Cons} instruction is generated
as a fallback. This implies, as said before, that \texttt{Cons}
instructions are more frequent. As a side effect, there is an higher
probability of having more variables with different types introduced
by \texttt{Cons} instructions in the program typing and execution
environment.

The \texttt{genInstructions} function generates a list of non-terminal instructions (i.e.\ containing neither \texttt{Jump} nor \texttt{Halt}). The generated environment is the result of applying the instructions list to the source environment. The given label is the current block label.

The \texttt{genBlockEnd} function generates a block terminal
instruction (\texttt{Jump} or \texttt{Halt}). The jump is always
directed forward to a random label with id greater than the current
block one. If the chosen landing block has an incompatible environment
(it is not a supertype of the current environment), then an
\texttt{Halt} instruction is generated instead. Similarly, if there is
no label to jump forward to, an \texttt{Halt} instruction is
generated. If the landing block was not already the target of a
previous \texttt{Jump} instruction, then the environment of the target
block is set equal to the current environment. If the current block
environment is empty (because no previous jumps landed on it), then
only an \texttt{Halt} instruction is generated: a \texttt{Jump}
instruction in a source empty environment can land only on blocks that
have an empty environment too, therefore it would cause a sequence of
empty blocks with just \texttt{Jump}s leading to the last one with an
\texttt{Halt} instruction; so directly generating here an
\texttt{Halt} instruction is considered equivalent and removes
possible chains of blocks containing only \texttt{Jump}
instructions. Recapitulating, there is a chance that \texttt{Halt}
instructions may appear elsewhere in the program other than in the
last block.

The last generator presented here, \texttt{genBlock}, generates an entire block and updates the program typing. If the last instruction is a \texttt{Jump}, then the landing block typing is updated with the environment resulting from the instructions list, except when the landing block has already a non-empty environment.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fscheck_block_generator},caption={example of a generator for program blocks written using FsCheck features}]
let genInstruction (l: Label) (pi: pi) (env: tenv)
    : Gen<Instruction * tenv> =
  Gen.oneof [
    genCons env
    genBranchIfNil l pi env
    genFetchField env
  ]

let rec genInstructions (l: Label) (pi: pi) (env: tenv) (size: int)
    : Gen<(Instruction list) * tenv> =
  match size with
  | 0 -> Gen.constant ([], env)
  | _ -> gen {
    let! (instructions, newEnv) = genInstructions l pi env (size - 1)
    if List.isEmpty (envVariables newEnv)
    then return (instructions, newEnv)
    else let! (i, finalEnv) = genInstruction l pi newEnv
      return ((List.append instructions [i]), finalEnv)
  }

let genBlockEnd (Id from: Label) (PI pi: pi) (env: tenv)
    : Gen<Instruction> =
  let labels = Map.toList pi |> List.length
  let forwardLabels = labels - (from + 1)
  if forwardLabels = 0 || (List.isEmpty (envVariables env))
  then Gen.constant Halt
  else gen {
    let! target = Gen.choose (1, forwardLabels)
               |> Gen.map (fun id -> Label.Id (id + from))
    if check-env-sub env (block-typing-lookup (PI pi) target)
    then return Jump target
    else return Halt
  }

let genBlock (l: Label) (pi: pi) (size: int)
    : Gen<Instruction * pi> =
  let env = block-typing-lookup pi l
  gen {
    let! (instructions, env') = genInstructions l pi env size
    let! last = genBlockEnd l pi env'
    let block = rollInstructions instructions last
    match last with
    | Jump l' -> return (block, setBlockTypingIfEmpty pi l' env')
    | _ -> return (block, pi)
  }
\end{lstlisting}
\end{figure}

The smart generators developed for this benchmark reach over a hundred lines of F\# code. Even if it looks like a small number, the time spent behind it was hours of dedicated work. Most time was spent finding a working structure for generators, studying data correlation and distribution to generate meaningful programs, typings, instructions, environments and stores. The process continued by improving generated data distribution and fixing bugs that were found out by analyzing checks results and coverage.

\subsection{Checking Generators}

FsCheck can be used not only to validate properties about the model under test, but also to validate the properties of data generators themselves, such as their soundness as explained in Section \ref{random_generation}. Complex generators developed for this benchmark are checked to see whether they satisfy the most strict preconditions of top-level checks, like \texttt{typecheck-program}, \texttt{typecheck-instr} and \texttt{store-has-type}. Listing \ref{lst:list_machine_fscheck_generators_checks} shows three straightforward checks that validate such conditions for the \texttt{InstructionContext} generator, which generates a tuple containing an instruction and a program that contains it at a given label, with related program typing, store and matching environment.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fscheck_generators_checks},caption={checking that custom made generators satisfy the preconditions used by \emph{progress}, \emph{preservation} and \emph{soundness} properties}]
Check.Quick ("instruction context generator yields a well-typed program",
  Prop.forAll (Generators.InstructionContext()) (fun (p, pi, r, g, l, i) ->
    typecheck-program pi p
))

Check.Quick ("instruction context generator yields a well-typed instruction",
  Prop.forAll (Generators.InstructionContext()) (fun (p, pi, r, g, l, i) ->
    (nonTerminalInstruction i) ==> lazy (typecheck-instr pi g i <> None)
))

Check.Quick ("instruction context generator yields a well-typed store",
  Prop.forAll (Generators.InstructionContext()) (fun (p, pi, r, g, l, i) ->
    store-has-type r g
))
\end{lstlisting}
\end{figure}

\section{Shrinkers}

Shrinkers were already mentioned in Section \ref{shrinking}. They go along with random generators, transforming large counterexamples into small meaningful ones. After the development of data generators for this benchmark, shrinkers became essential in bug fixing the list-machine model, and were useful in the study of counterexamples produced by checks on the various mutations. Writing shrinkers, such as generators, was not a straightforward task and took several hours of design and development, considering various shrinking strategies and refinements. It was moreover complicated by the fact that certain values have to remain correlated even after being shrunk, e.g.\ programs and program typings still must typecheck.

Listing \ref{lst:list_machine_fscheck_shrinkers} shows the core structure of a shrinker for programs and program typings which typecheck after the shrinking process. A shrunk sequential composition instruction has one of its sub-instructions removed. A shrunk program has one of its instructions shrunk. After a program is shrunk, its program typing, if it exists, is generated with the \texttt{typeInference} function (which is not included in the listing due to its length).

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fscheck_shrinkers},caption={examples of shrinkers for program and program typing}]
let rec shrinkInstruction (i: Instruction): Instruction list =
  match i with
  | Seq (i', i'') -> i'' :: (shrinkInstruction i''
                             |> List.map (fun i''' -> Seq (i', i''')))
  | _ -> []

let rec shrinkProgram (Prog p: Program): Program list =
  match p with
  | [] -> []
  | (l, i) :: p' ->
    let sp = shrinkProgram (Prog p')
             |> List.map (fun (Prog p'') -> Prog ((l, i) :: p''))
    let sp' = shrinkInstruction i
              |> List.map (fun i' -> Prog ((l, i') :: p'))
    List.append sp sp'

let shrinkProgramAndTyping ((p, pi): Program * pi): (Program * pi) list =
  match shrinkProgram p with
  | [] -> []
  | ps -> ps
          |> List.map (fun p' -> (p', typeInference p'))
          |> List.filter (fun (_, pi') -> Option.isSome pi')
          |> List.map (fun (p', pi') -> (p', Option.get pi'))
          |> List.filter (fun (p', pi') -> typecheck-program pi' p')
\end{lstlisting}
\end{figure}

\section{Existentials Encoding}

As discussed in Section \ref{acheck_existentials_encoding}, the possible strategies for dealing with existential quantifiers are skolemization and exhaustive search with iterative deepening. In a functional PBT setting, skolemization is the natural choice, as discussed in \citep{smallcheck}. For example, the \emph{progress-env} lemma in \citep{listmachine} is stated as
\[
  \begin{array}{c}
    \begin{array}{c}
      \mathit{\Gamma(v) = \tau} \quad \mathit{r: \Gamma}\\
      \hline
      \exists a.\ r(v) = a\ \wedge\ a: \tau
    \end{array} \quad \mbox{progress-env}
  \end{array}
\]
and the relative check is implemented by defining a predicate that encodes the property conclusion, shown in Listing \ref{lst:list_machine_fscheck_skolemization}. The skolemization function that has been chosen is trivially the equation $a = r(v)$ that appears in the postcondition of the formula above. In this simple case, skolemization was trivially achieved thanks to the presence of an univocal and finite way of finding the existentially quantified value, implemented by the \texttt{existsVariableWithType} function.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fscheck_skolemization},caption={skolemization of an existential quantifier ($\exists a.\ r(v) = a\ \wedge\ a: \tau$)}]
let existsVariableWithType (Env r: Environment) (v: Variable) (t: ty)
    : bool =
  match Map.tryFind v r with
  | Some a -> value-has-ty a t
  | None -> false

Check.One ("progress-env", config,
  Prop.forAll (Generators.MatchingStoreAndEnvironment()) (fun (r, g)->
    Prop.forAll (variableFromTEnv g) (fun v ->
      let (t, g') = env-lookup g v
      (store-has-type r g) ==> (lazy existsVariableWithType r v t)
)))
\end{lstlisting}
\end{figure}

The \texttt{existsTEnvForStoreAndBlock} function does not search for an existential environment $\mathit{\Gamma'}$ by itself, but accepts a list of candidates which are provided by the \texttt{updateTEnv} function. Listing \ref{lst:list_machine_fscheck_existentials} shows how these functions are implemented. The \texttt{updateTEnv} function returns the empty environment and possible evolutions of the initial environment, based on the involved instruction. Therefore in the case of a branch instruction, such as \texttt{Jump} or \texttt{BranchIfNil}, also the target environment associated with the jump label is included.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fscheck_existentials},caption={skolemization of existential quantifier on environments in the \emph{preservation} property}]
// Used to satisfy the existential updated environment g
// in existsTEnvForStoreAndBlock(pi, r', i')
let updateTEnv (pi: pi) (g: tenv) (i: Instruction): tenv list =
  let empty = TEnv (Map.empty)
  empty :: (
    match i with
    | Halt -> []
    | Jump l -> [block-typing-lookup pi l]
    | Seq (BranchIfNil (v, l), _) ->
      match typecheck-instr pi g (BranchIfNil (v, l)) with
      | Some g' -> [g'; block-typing-lookup pi l]
      | None -> [block-typing-lookup pi l]
    | Seq (i', _) ->
      match typecheck-instr pi g i' with
      | Some g' -> [g']
      | None -> []
    | _ -> []
  )

let existsTEnvForStoreAndBlock
    (pi: pi)
    (r': Environment)
    (i': Instruction)
    (g: tenv list)
    : bool =
  g
  |> List.filter (fun g' ->
        (store-has-type r' g') &&
        (typecheck-block pi g' i'))
  |> List.isEmpty
  |> not
\end{lstlisting}
\end{figure}

\section{Unit Testing}

As in the \aProlog\ implementation of the benchmark, unit tests were ported from Redex to F\# code. They are encoded in a slightly unnatural way with FsCheck, but it was preferred not to import other testing frameworks, such as NUnit, in the project to keep its management simple. Some utility functions are defined to run propositions in a declarative way, while others include default generators for parametric tests. Examples of those functions and some unit tests are given in Listing \ref{lst:list_machine_fscheck_unit_tests}. We employed random generation of values for parametric tests to simulate, in a straightforward and naive way, the use of meta-variables in \aProlog\ and in PLT-Redex unit tests.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_fscheck_unit_tests},caption={unit tests implemented with FsCheck}]
(* Utilities *)

// actually just an expedient to obtain a pretty printed
// test execution result
let test (f: unit -> bool) =
  Check.One ({ Config.Quick with MaxTest = 1 }, Prop.ofTestable f)

let ensure (b: bool) =
  test (fun () -> b)

// forAllX functions work the same way for X values
let forAll<'T> (f: 'T -> Property) =
  Prop.forAll Arb.from<'T> f |> Check.Quick

(* Unit Tests *)

test (fun () ->
  let p = Prog []
  let r = store []
  let i = Seq(Seq(Jump (Id 0), Jump (Id 1)), Jump(Id 2))
  step r i p = (store [], Jump (Id 0) @@ (Jump (Id 1)) @@ (Jump (Id 2)))
)

ensure (check_subType (Ty_listcons Ty_nil) (Ty_list (Ty_list Ty_nil)))

(* Parametric Test *)

forAll2<Variable, Label> (fun v l ->
  let pi = typing [l, env [v, Ty_nil]]
  let g = env [v, Ty_nil]
  let i = BranchIfNil(v, l)
  typecheck-instr pi g i = (Some (env [v, Ty_nil])) |> Prop.ofTestable
)
\end{lstlisting}
\end{figure}

\section{Experimental Results}

Not all mutations were applicable to the F\# implementation, as mentioned before. Table \ref{tbl:list_machine_fscheck_test_results} depicts the results of test execution on the mutated models. PBT beats unit testing in killing mutants just by one, with a score of 8/11 against 7/11. The average execution time for unit tests is approximately 5 seconds, while the whole BPT suite completes in around 5 minutes: roughly 10 seconds are taken by top-level checks and about 40 seconds by the auxiliary checks, while the remaining time is taken by the execution of checks regarding lemmas in \citep{listmachine}.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | c | l |}
    \hline
    Mutant & Failing unit tests & Counterexamples found by checks\\
    \hline
    \hline
    1 & 1 & preservation\\
    \hline
    3 & 1 & preservation, soundness\\
    \hline
    6 & 1 & \ \\
    \hline
    7 & 1 & progress-instr, soundness\\
    \hline
    8 & 0 & soundness\\
    \hline
    9 & 0 & progress, soundness\\
    \hline
    13 & 1 & lub-subtype-left, lub-subtype-right\\
    \hline
    15 & 0 & preservation\\
    \hline
    16 & 1 & preservation\\
    \hline
    17 & 1 & \ \\
    \hline
    18 & 0 & \ \\
    \hline
  \end{tabular}
\]
\caption{test results for each mutation, listing all checks that find a counterexample}
\label{tbl:list_machine_fscheck_test_results}
\end{table}

The \emph{soundness} property alone scores 4 mutants killed, the same amount of the \emph{preservation} check. Together, the top-level properties find 7 counterexamples for different mutations. With the addition of properties based on lemmas, the score is increased just by one. The auxiliary checks are not relevant in this case. By adding unit tests, only the 18th mutation cannot be killed by any of them.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | c | c | c | c |}
    \hline
    Mutant & 3 theorems & 12 lemmas & 10 auxiliary checks & 43 unit tests\\
    \hline
    \hline
    1 & \checkmark & \ & \ & \checkmark\\
    \hline
    3 & \checkmark & \ & \ & \checkmark\\
    \hline
    6 & \ & \ & \ & \checkmark\\
    \hline
    7 & \checkmark & \ & \checkmark & \checkmark\\
    \hline
    8 & \checkmark & \ & \ & \ \\
    \hline
    9 & \checkmark & \ & \ & \ \\
    \hline
    13 & \ & \checkmark & \ & \checkmark\\
    \hline
    15 & \checkmark & \ & \ & \ \\
    \hline
    16 & \checkmark & \ & \ & \checkmark\\
    \hline
    17 & \ & \ & \ & \checkmark\\
    \hline
    18 & \ & \ & \ & \ \\
    \hline
  \end{tabular}
\]
\caption{comparing the results of checks about top-level theorems and lemmas, auxiliary checks and unit tests}
\label{tbl:list_machine_fscheck_test_comparison}
\end{table}

The collected results showed that checks about lemmas and auxiliary checks contribute just a little in killing mutants: by removing them, the whole execution time becomes about 10 seconds, while previously it was close to 5 minutes. Therefore, here we discuss the performances regarding top-level checks only. Table \ref{tbl:list_machine_fscheck_test_execution_times} summarizes each top-level check behavior: whether it killed the mutant indicated in the leftmost column, how many test iterations it performed before killing a mutant or exhausting generated input arguments, and how much time it took to execute all test iterations. Test iterations and execution times are calculated on an average of 100 executions for each check.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | c | r | r | c | r | r | c | r | r |}
    \hline
    \ & \multicolumn{3}{c|}{Progress} & \multicolumn{3}{c|}{Preservation} & \multicolumn{3}{c|}{Soundness}\\
    \hline
    Mutant & \checkmark & Test & Time & \checkmark & Test & Time & \checkmark & Test & Time\\
    \hline
    \hline
    \ & \ & 150 & 327.7\ ms & \ & 3739 & 7.195\ s & \ & 1871 & 2.028\ s\\
    \hline
    \hline
    1 & \ & 139 & 326.6\ ms & \checkmark & 724 & 265.4\ ms & \ & 1191 & 1.122\ s \\
    \hline
    3 & \ & 153 & 344.0\ ms & \checkmark & 4 & 880.6\ $\mu$s & \checkmark & 118 & 40.93\ ms \\
    \hline
    6 & \ & 153 & 337.7\ ms & \ & 3758 & 7.283\ s & \ & 1882 & 2.066\ s \\
    \hline
    7 & \ & 150 & 324.4\ ms & \ & 3005 & 4.734\ s & \checkmark & 179 & 73.91\ ms \\
    \hline
    8 & \ & 151 & 328.0\ ms & \ & 3756 & 7.268\ s & \checkmark & 88 & 29.98\ ms \\
    \hline
    9 & \checkmark\ & 1 & 1.854\ ms & \ & 3752 & 7.962\ s & \checkmark & 33 & 12.20\ ms \\
    \hline
    13 & \ & 21 & 257.5\ ms & \ & 277 & 411.5\ ms & \ & 189 & 317.5\ ms \\
    \hline
    15 & \multicolumn{3}{c|}{stuck} & \checkmark & 2 & 119.4\ ms & \ & 1884 & 2.218\ s \\
    \hline
    16 & \ & 149 & 344.9\ ms & \checkmark & 299 & 60.32\ ms & \ & 1421 & 1.427\ s \\
    \hline
    17 & \ & 0 & 237.7\ ms & \ & 0 & 243.1\ ms & \ & 0 & 221.7\ ms \\
    \hline
    18 & \ & 151 & 362.0\ ms & \ & 3757 & 6.459\ s & \ & 1876 & 1.935\ s \\
    \hline
  \end{tabular}
\]
\caption{average test iterations count and execution times of top-level checks}
\label{tbl:list_machine_fscheck_test_execution_times}
\end{table}

Mutation 15 has encountered problems in data generation. The mutation affects the \texttt{value-has-ty} predicate in a way that the listcons type cannot be attributed to any value. The \texttt{InstructionContext} generator uses this predicate when generating a store from a given environment. For each variable contained in the environment, values are generated until one is found that matches the variable type. When the variable type is a listcons, however, no value exists that could match it and therefore the generator gets stuck while indefinitely searching. The \emph{preservation} check uses this generator, but it finds counterexamples after few test iterations (2 on average), usually before the moment when the generator gets stuck.

As can be seen in the table, every check has an average number of test iterations that is similar in many mutants: \emph{progress} is repeated about 150 times, \emph{preservation} runs over 3700 iterations, and \emph{soundness} test is executed over 1800 times. The average test iterations count drops when the check is likely to find a counterexample. However, there are some mutants that register a significant drop in test iterations even when no counterexample is found. The mutants that cause such major drops are the 13th and, more dramatically, the 17th. Beyond superstition issues, the cause of such behavior has his roots in how test data is generated.

Mutation 13 removes a clause from the subtyping predicate: a listcons type cannot be a subtype of an other listcons anymore. The subtyping relation is used by the environment subtyping relation, which is used by the typechecking algorithm when checking that the target environment of a jump is a supertype of the current one. The program and program typing generator is not affected by this mutation and keeps generating well-typed couples which follow the correct definitions.

Mutation 17 affects the \texttt{typecheck-blocks} predicate when checking an instruction sequence: the first instruction is checked as usual, but instead of making a recursive call to typechecking the second block, the first instruction is checked as a block instead. However, only \texttt{Seq}, \texttt{Halt} and \texttt{Jump} instructions are valid blocks: the only "well-typed" programs under these conditions have all their blocks made only of \texttt{Halt} or \texttt{Jump} instructions. There is only a very small chance that such programs are generated, with a greater probability for small generation sizes.

Even mutation 1 and 16 are slightly affected by generated pairs of well-typed programs and program typings that do not always satisfy the \texttt{typecheck-program} predicate. Table \ref{tbl:list_machine_fscheck_test_iterations_drops} shows the percentages of program and program typing pairs that typecheck for any mutant affected by a drop in test iterations count. The last three columns show the relative drop for each top-level check, based on the average test iterations count of checks run when no mutation is applied. Checkmarks indicate that counterexamples were found and the same reasoning cannot be applied in such cases.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | c | c | c | c |}
    \hline
    Mutant & Well-typed & Progress & Preservation & Soundness\\
    \hline
    \hline
    1 & 80.5\% & 92.7\% & \checkmark & 63,7\%\\
    \hline
    13 & 7.17\% & 14.0\% & 7.41\% & 10.1\%\\
    \hline
    16 & 87.9\% & 99.3\% & \checkmark & 75.9\%\\
    \hline
    17 & 1.01\% & 0.00\% & 0.00\% & 0.00\%\\
    \hline
  \end{tabular}
\]
\caption{percentages of generated well-typed program and program typing pairs that satisfy the \texttt{typecheck-program} predicate after the application of a mutation}
\label{tbl:list_machine_fscheck_test_iterations_drops}
\end{table}

In conclusion, the data distribution of custom generators may become ineffective when an error that affects check preconditions is introduced. When there is a zero or nearly zero test iterations count, it is clear that some error is involved, however it may not be as evident whether it affects the implemented model or the generator itself. At least it is an obvious evidence that something is wrong, but it still has to be discovered. A simple way of measuring how good a generator behaves is to collect percentages of data samples that satisfy the more strict preconditions of involved tests.
