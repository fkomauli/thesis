Reasons of space and the desire not to bore the reader silly suggest
to  present only a selection of all the experiments that we have
carried out. Full details can be found in~\cite{PBTAM}.

\subsection{A cautionary tale}
\label{ssec:value-has}

A testing approach is any good only if it uncovers bugs, but we were
not expecting to find any in the model presented in the paper, which
came equipped with a type soundness proof formalized in two proof
assistants. So, imagine our surprise when FsCheck, and later on \aProlog came up with this counterexample to
type preservation:
\[
\begin{array}{l}
  p = (l_0: \mbox{cons}(v_0,v_0,v_0);\ \mbox{jump}\ l_1);\ (l_1: \mbox{fetch-field}(v_0,0,v_0);\ \mbox{jump}\ l_2);\ (l_2; \mbox{halt})\\[.2cm]
  \Pi = (l_0: [v_0 \mapsto \mbox{nil}]);\ (l_1: [v_0 \mapsto \mbox{listcons nil}]);\ (l_2: [v_0 \mapsto \mbox{nil}])\\[.2cm]
  r = [v_0 \mapsto \mbox{cons}(\mbox{cons}(\mbox{nil},\mbox{nil}),\mbox{nil})]\\[.2cm]
  i = \mbox{fetch-field}(v_0,0,v_0);\ \mbox{jump}\ l_2\qquad (\mbox{\emph{instruction at}}\ l_1)
\end{array}
\]
which after a single step yields:
\[
\begin{array}{l}
  r' = [v_0 \mapsto \mbox{cons}(\mbox{nil},\mbox{nil})]\\[.1cm]
  i' = \mbox{jump}\ l_2
\end{array}
\]
However, there can be no $\Gamma'$ satisfying the postconditions: any
such type environment must contain either the binding
$[v_0 \mapsto \mbox{listcons}\ \tau]$ or
$[v_0 \mapsto \mbox{list}\ \tau]$ to accommodate $r'$, but both would
not be compatible with what is required by the jump.

% The counterexample was further
% investigated by analyzing the possible shapes an environment $\Gamma'$
% must have to satisfy the property postconditions:
% \begin{itemize}
%   \item $\Gamma' = [v_0 \mapsto \mbox{nil}]$: an environment with this shape
%   satisfies the \emph{check-block} predicate, because the target environment of
%   the $\mbox{jump}\ l_2$ instruction is a supertype of $\Gamma'$ (actually it
%   is the same), but it is not a compatible environment for the store $r'$, so
%   this case is excluded;
%   \item $\Gamma' = [v_0 \mapsto \mbox{listcons}\ \tau]$: on the contrary, this
%   environment has the correct type for store $r'$, for any $\tau$ as follows
%   from the inference rules, but $\Gamma'$ is not a sub-environment of the jump
%   target environment;
%   \item $\Gamma' = [v_0 \mapsto \mbox{list}\ \tau]$: in this case, the same
%   reasoning of the previous point can be applied.
% \end{itemize}

% The addition of more entries to environment $\Gamma'$ does not affect the points
% above. The removal of $v_0$ instead, that is $\Gamma' = [\ ]$, is not applicable
% because such environment is not a sub-environment of the jump target
% environment. Therefore no $\Gamma'$ exists such that the postconditions are
% satisfied.

After a fair amount of soul searching, we zeroed on the encoding of
the the judgment \emph{value-has-ty}, (page 481 of~\cite{listmachine}).
\[
\begin{array}{c}
  \infer[]{ \mbox{nil}:\ \mbox{nil}}{}\qquad
  \infer[]{  \mbox{nil}:\ \mbox{list}\ \tau}{}\qquad
  \infer[]{      \mbox{cons}(a_0,\ a_1):\ \mbox{listcons}\ \tau}{}\qquad
  \infer[]{   a:\ \mbox{list}\ \tau}{a:\ \mbox{listcons}\ \tau}
\end{array}
\]
% \[
%   \begin{array}{c c c c}
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{nil}:\ \mbox{nil}
%     \end{array}&\qquad
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{nil}:\ \mbox{list}\ \tau
%     \end{array}&\qquad
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{cons}(a_0,\ a_1):\ \mbox{listcons}\ \tau
%     \end{array}&\qquad
%     \begin{array}{c}
%       a:\ \mbox{listcons}\ \tau\\
%       \hline
%       a:\ \mbox{list}\ \tau
%     \end{array}
%   \end{array}
% \]
This is an essential component of the definition of $r: \Gamma$, which
can be stated as for all $v\in \mathrm{dom}(r)$, it holds
$r(v) : \Gamma(v)$. The \emph{listcons} case sounds fishy, since it
makes no assumption about the types of $a_0$ and $a_1$. Once we
changed that case to:
$$ \infer[]{      \mbox{cons}(a_0,\ a_1):\ \mbox{listcons}\ \tau}{      a_0 : \tau \quad a_1 : \mbox{list}\ \tau}$$
the counterexample failed to show up.  This change was also confirmed
by inspecting the Coq implementation (it is left as an exercise in the
Twelf one), which defines \emph{value-has-ty} exactly like that.

Less dramatically, the \emph{check-instr-branch-listcons} typing rule
in the original paper contains additional preconditions similar to
those in the \emph{check-instr-branch-list} rule, regarding the jump
target environment; however in the Coq implementation they are not
present, and they could not be, considering the proof of equivalence
with the algorithmic version of type checking, where they are notably
absent. Several typos also present, viz.\ rule
\emph{check-instr-cons0} (Section 8.1) and in the type checking
algorithm (instruction
$\mathtt{typecheck\_instr\ \Pi\ \Gamma\ \iota}$). Typos were spotted
through formalization, not PBT.


The moral is, of course, that if there is no formal connection between
a formalization and the paper reporting it, Isabelle/HOL and literate
Agda being the precious exceptions, you may want to be skeptical of the
latter. Similar findings appear in~\cite{Klein12}.


%% commented out
\ignore{
The first implementation of the \emph{preservation} property with
\aCheck\ did not find any counterexample. That was caused by a bug
contained in the implementation of the existential quantifier present
in the property postcondition. Then, counterexamples emerged with the
FsCheck version of the property.

\begin{lstlisting}
p = Prog [
  (Id 0, Seq (
    Cons (Name 0, Name 0, Name 0),
    Jump (Id 1)
  ));
  (Id 1, Seq (
    FetchField (Name 0, Head, Name 0),
    Jump (Id 2)
  ));
  (Id 2, Halt)
]

pi = PI [
  (Id 0, TEnv [(Name 0, Ty_nil)]);
  (Id 1, TEnv [(Name 0, Ty_listcons Ty_nil)]);
  (Id 2, TEnv [(Name 0, Ty_nil)])
]

l = Id 1
i = Seq (FetchField (Name 0, Head, Name 0), Jump (Id 2))
g = TEnv [(Name 0, Ty_listcons Ty_nil)]
r = Env [(Name 0, Cell (Cell (Nil, Nil), Nil))]

// after step
i' = Jump (Id 2)
r' = Env [(Name 0, Cell (Nil, Nil))]
\end{lstlisting}

The counterexample was further investigated by analyzing the possible shapes an environment $\Gamma'$ must have to satisfy the property postconditions:

\begin{itemize}
  \item $\Gamma' = [(v_0,\ \mbox{nil})]$: an environment with this shape satisfies the \emph{check-block} predicate, because the target environment of the \texttt{Jump (Id 2)} instruction is a supertype of $\Gamma'$ (actually it is the same), but it is not a compatible environment for the store $r'$, so this case is excluded;
  \item $\Gamma' = [(v_0,\ \mbox{listcons}\ \tau)]$: on the contrary, this environment has the correct type for store $r'$, for any $\tau$ as follows from the inference rules, but $\Gamma'$ is not a sub-environment of the jump target environment;
  \item $\Gamma' = [(v_0,\ \mbox{list}\ \tau)]$: in this case, the same reasoning of the previous point can be applied.
\end{itemize}

The addition of more entries to environment $\Gamma'$ does not affect the points above. The removal of $v_0$ instead, that is $\Gamma' = [\ ]$, is not applicable because such environment is not a sub-environment of the jump target environment. Therefore no $\Gamma'$ exists such that the postconditions are satisfied.

This counterexample was ported to \aProlog:

\begin{lstlisting}
? Pi = [
    (l0, [(v0 ,ty_nil)]),
    (l1, [(v0, ty_listcons(ty_nil))]),
    (l2, [(v0, ty_nil)])
  ],
  I = instr_jump(l2),
  R = [(v0, value_cons(value_nil, value_nil))],
  G = [(v0, T)],
  env_ok(G),
  store_has_type(R, G),
  not(check_block(Pi,G,I)).
% Yields "Yes.", with T = ty_listcons(ty_nil)
\end{lstlisting}
After this evidence, the \emph{preservation} property for \aCheck\
was reformulated by using an exhaustive bounded search for the
existential quantifier. The new version of the check began finding
counterexamples, therefore the inference rules of \emph{value-has-ty}
were changed by introducing a recursive call to the listcons case,
which are shown below: the last rules now take in consideration the
types of the values contained by a cons.
}
% \[
%   \begin{array}{c c c c}
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{nil}: \mbox{nil}
%     \end{array}&
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{nil}: \mbox{list}\ \tau
%     \end{array}&
%     \begin{array}{c}
%       a_0: \tau \quad a_1: \mbox{list}\ \tau \\
%       \hline
%       \mbox{cons}(a_0, a_1): \mbox{listcons}\ \tau
%     \end{array}&
%     \begin{array}{c}
%       a_0: \tau \quad a_1: \mbox{list}\ \tau \\
%       \hline
%       \mbox{cons}(a_0, a_1): \mbox{list}\ \tau
%     \end{array}
%   \end{array}
% \]



\subsection{Mutation analysis}
\label{ssec:exp}


\pgfplotstableread[row sep=\\,col sep=&]{
    Suite            & aCheck & FsCheck \\
    Theorems         & 9      & 7      \\
    Lemmas           & 2      & 1      \\
    Auxiliary Checks & 2      & 1      \\
    Unit Tests       & 7      & 7      \\
    }\datacomparison
\begin{small}
\begin{figure}[ht]
\begin{tikzpicture}
    \begin{axis}[
            ybar,
            bar width=1cm,
            width=\textwidth,
            height=.5\textwidth,
            legend style={at={(0.5,1)},
                anchor=north,legend columns=-1},
            symbolic x coords={Theorems,Lemmas,Auxiliary Checks,Unit Tests},
            xtick=data,
            nodes near coords,
            nodes near coords align={vertical},
            ymin=0,ymax=11,
            ylabel={Killed Mutants},
        ]
        \addplot table[x=Suite,y=aCheck]{\datacomparison};
        \legend{\aCheck, FsCheck}
        \addplot table[x=Suite,y=FsCheck]{\datacomparison};
    \end{axis}
\end{tikzpicture}
\caption{Mutation analysis for all properties and unit tests}
\label{fig:comparisonchart}
\end{figure}
\end{small}

We used our ``home-baked'' mutation testing to assess the
efficacy of PBT suites implemented with \aCheck\ and FsCheck w.r.t.\
the 11 mutations applicable to both
implementation. 
Checks are divided in three groups: top-level theorems as in our
Section~\ref{ssec:checks}, intermediate lemmas collected in Section 8
of~\cite{listmachine} and auxiliary checks regarding low-level details
of the machine specification. We have also ported from PLT-Redex to
both \aProlog and F\# few dozens unit tests, as an additional baseline
for mutant killing. In doing, so we have made some of them
\emph{parametric}, so that exhaustive and random generation of those
parameters could make those tests more far-reaching.



Checks that took more than a minute to find a counterexample are
considered timed-out\footnote{Checks executed on machine with Intel®
  Core™ i5--4--200U CPU,a clock speed of 1.60GHz and 8GB of RAM, with 64
  bit Ubuntu 17.10 Artful and Linux kernel 4.13.0}. We do not compare
times to find counterexamples in \aCheck vs.~FsCheck, because it is a
serious case of oranges and apples. We did take timing information's
separately, and again we refer to~\cite{PBTAM} for the full breakdown.
In summary, the FsCheck implementation of PBT is quick (as promised,
although not as much as you would expect), not only because of the
efficiency of the host language, but also thanks to the flexibility of
the configuration of how checks are executed.  The whole FsCheck BPT
suite completes in around 5 minutes: roughly 10 seconds are taken by
top-level checks and about 40 seconds by the intermediate lemmas,
while the remaining time is taken by the auxiliary checks.  Instead,
execution time of the \aCheck\ suite is not as predictable: we
manually set the depth bound so as to make the  execution stay withing
10 seconds.  Exception to this discipline are the \emph{progress} and
\emph{preservation} checks, whose search space size tend to explode %  before
% reaching a sufficient depth where counterexamples may be found, 
 several minutes to complete.  Unit tests, even in a parametric
fashion, execute in just few seconds in both % F\# and \aProlog\
implementations.

What we can reasonably compare is the rate of mutants killed by the
two testing approaches (Fig.~\ref{fig:comparisonchart}) and the
\aCheck\ implementation comes marginally ahead. Lemmas and low-level
checks did not perform well, while top-level theorems were capable of
killing most mutants. In fact, the \emph{soundness} property was the
one which killed most, as we can see in
Fig.~\ref{fig:theoremschart} and it is a good countermeasure against
possible errors contained in the model. This is consistent to the
PLT-Redex model, where soundness is the only property checked.

While we do not want to read too much in such a limited
experimentation, it is safe to say that \aCheck keeps its ground, considering
how inexpensive it is to set up.

\pgfplotstableread[row sep=\\,col sep=&]{
    Theorem      & aCheck & FsCheck \\
    Progress     & 1      & 1       \\
    Preservation & 5      & 4       \\
    Soundness    & 7      & 4       \\
    Together     & 9      & 7       \\
    }\theoremscomparison
    \begin{small}
\begin{figure}[t]
\begin{tikzpicture}
    \begin{axis}[
            ybar,
            bar width=1cm,
            width=\textwidth,
            height=.5\textwidth,
            legend style={at={(0.5,1)},
                anchor=north,legend columns=-1},
            symbolic x coords={Progress,Preservation,Soundness,Together},
            xtick=data,
            nodes near coords,
            nodes near coords align={vertical},
            ymin=0,ymax=11,
            ylabel={Killed Mutants},
        ]
        \addplot table[x=Theorem,y=aCheck]{\theoremscomparison};
        \legend{\aCheck, FsCheck}
        \addplot table[x=Theorem,y=FsCheck]{\theoremscomparison};
    \end{axis}
\end{tikzpicture}
\caption{Mutation analysis for top-level theorems}
\label{fig:theoremschart}
\end{figure}
\end{small}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pbt"
%%% End:

%  LocalWords:  ty FsCheck cex listcons dom Twelf typecheck PBT sep
%  LocalWords:  postcondition postconditions supertype aCheck PLT BPT
