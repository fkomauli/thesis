Does this sound familiar? You are in the middle of a long but supposedly
straightforward formal proof trying to drag your favourite proof
assistant %-- or at least, it used like that until this very moment --
to confirm the blindingly obvious truth of the result, when you get
stuck in an unprovable part of the derivation; you realize that the
statement needs to be adjusted or more commonly that there is
something afoul, typically a banality, in your specification. If only
you had a way to realize that the theorem was unprovable before
wasting precious time in a doomed proof attempt, perhaps some kind of
\emph{testing} \dots

This is where, with due respect to Dijkstra and his ``thou shall not
test'' commandment, \emph{validation} prior to theorem proving comes
in: as a matter of fact, such tools have been available for some time
in the leading proof assistants: to cite only two of them, the
\emph{NitPick/QuickCheck} combination~\cite{BlanchetteBN11} (and
descendants) in Isabelle/HOL and
\emph{QuickChick}~\cite{foundationalpbt} in Coq.

Our angle is not verification vs.\ validation in general, but in a
particular domain: the mechanization of the meta-theory (MMT) of
programming languages (PL) and related calculi. As any practitioner may
testify, the main properties of interest are well known and
have mathematically shallow proofs. The difficulties lies in the
potential magnitude of the cases one must consider and in the
trickiness that some encodings require, typically those having to deal
with binding signatures. Here, very minor mistakes in the
specification, even at the level of what we would consider a typo,
may severely frustrate the verification effort, to the point to make
it not cost-effective.
%
Further, we aim to support the ``working
semantics'' in her work in \emph{developing}, and eventually formally
proving correct, such calculi, rather than concentrating on models that
we already know to be correct.

The model checking technique we have adopted is \emph{property-based
  testing} (PBT):  while it originated as a testing techniques for
concrete (functional) programs, it is also applicable at the
meta-theoretical level, especially in the PL domain, as shown by the
success~\cite{Klein12} of tools such as \emph{PLT-Redex} (\url{https://redex.racket-lang.org}) and, ``si
parva licet componere magnis'', \aCheck~\cite{alphacheck}. PBT's
data generation strategy comes in several flavours:
random~\cite{quickcheck}, exhaustive~\cite{SmallCheck}, a combination
of the two~\cite{Feat}, with an ever increasing emphasis on
coroutining the generation and testing
phases~\cite{FetscherCPHF15,LampropoulosPP18}. An analysis of some of
those strategies as applied to MMT was carried out
in~\cite{FachiniM17}.


In this paper, we abandon the comfort of high-level object languages,
which have been investigated extensively, and address the validation
of abstract machines and typed assembly languages. That seems more
challenging, since instructions operating at a low level provide less
``structure'' for counterexamples, which then tend to be substantially
more complex. Further, it is hard to generate sequences of machine
states that yield meaningful machine runs. Similar remarks have
appeared in the context of random generation of C
programs~\cite{csmith}. Not coincidentally, the authors of~\cite{ifc}
suggest that ``[counterexamples to properties such as non-interference
may be] well beyond the scope of naive exhaustive testing'' (pag.~12).

We take on Appel et al.'s list-machine benchmark~\cite{listmachine},
dubbed by the authors CIVmark, standing for \textbf{C}ompiler
\textbf{I}mplementation \textbf{V}erification. We concentrate mostly
on its version $1.0$, as originally presented at LFMTP'06, but we also
touch on $2.0$, see Section~\ref{ssec:lm2.0}. CIVmark was conceived as
a benchmark for ``machine-checked proofs about real compilers'',
spurred by a civilized criticism of POPLMark as being too biased
towards issues of binders. It consists of a basic pointer machines
with instructions such as \texttt{car}, \texttt{cdr}, \texttt{cons}
and (un)conditional jump, endowed with a standard SOS, but with a
reasonably sophisticated type system: the latter, being able to make
static predictions about lists being empty or not, guarantees that a
well typed machine's run does not get stuck. Two implementations,
including sketch of proofs of type soundness in Twelf and Coq come
with the paper.

A version of this model is also part of the benchmarks distribution of
PLT-Redex~\cite{redexlm}
and as such being subjected to some  PBT. All the
more reasons to tackle with two very distinct PBT's approaches, which
fits well with the existing formalizations:
\begin{enumerate}
\item An encoding in the nominal logic programming
  $\alpha$Prolog~\cite{nominallogicb}, to be tested with its model
  checker \aCheck~\cite{alphacheck};
\item A functional encoding with the PBT library
  \emph{FSCheck} for \emph{F\#}.
\end{enumerate}
%
This allows us to compare the relative merits of exhaustivity-based
PBT in a logic programming style versus the more usual randomized
functional setting polarized by QuickCheck. It is also a stress test
for \aCheck, since CIVmark does not exploit any of the features
offered by nominal logic that makes \aProlog effective, see the case
studies at \url{https://github.com/aprolog-lang/checker-examples},
which are all devoted to high-level languages, including some studied
in~\cite{Klein12}; instead, the CIVmark benchmark brings to the fore
the naivete of \aProlog's search strategy.

A testing approach is ``good'' only if it uncovers bugs, and so we
did: we falsified the type preservation property as presented in the
paper, caused by an incorrect specification of typing for values.
This is particularly striking, considering the paper comes with two
\emph{formalized} proof of type soundness.  The mystery disappears
once we realized that the Coq implementation of that judgment was
different from the paper and coincided with the definition that we had
reverse-engineered from the counter-example to
preservation\footnote{The Twelf ``proof'' is left as an exercise and
  the above typing judgment undefined.}.  We also found several typos
and ambiguities in the typing rules in the published paper, but this
is not unusual where there is no connection between the text in the
paper and the model verified by the proof assistant.

That was encouraging, but to asses further the trade-off between
exhaustive and randomized data generation, we resorted to some
\emph{mutation testing}~\cite{softwaretesting} on the given debugged
model. As we will see in Section~\ref{sec:exper}, exhaustive
generation, in all its naivety, tends to be more cost effective than
pure random PBT. Finally, we report on some ongoing work on the
list-machine $2.0$ and on replaying the first section of~\cite{ifc},
which deals with an abstract machine for dynamic information-flow
control. Here again, exhaustive PBT shows its colours.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pbt"
%%% End:

%  LocalWords:  NitPick Quickcheck QuickChick PBT MMT PLT si parva
%  LocalWords:  licet componere magnis Appel CIVmark ompiler POPLMark
%  LocalWords:  mplementation erification cdr Twelf FSCheck FsCheck
%  LocalWords:  QuickCheck
