(TeX-add-style-hook
 "pbt"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("eptcs" "submission" "copyright" "creativecommons")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "intro"
    "lm"
    "aprolog"
    "fs"
    "experiment"
    "ongoing"
    "conc"
    "eptcs"
    "eptcs10"
    "breakurl"
    "underscore"
    "url"
    "xspace"
    "amssymb"
    "amsmath"
    "amsthm"
    "listings"
    "proof"
    "graphicx"
    "cancel"
    "pgfplots")
   (TeX-add-symbols
    '("envok" 1)
    '("checkprogram" 2)
    '("checkblocks" 2)
    '("checkblock" 3)
    '("checkinstr" 4)
    '("kleenestep" 5)
    '("smallstep" 5)
    "event"
    "aProlog"
    "aCheck"
    "ednote"
    "ignore"
    "titlerunning"
    "authorrunning")
   (LaTeX-add-labels
    "sec:lm"
    "aprolog"
    "fs"
    "sec:exper"
    "sec:ongoing"
    "sec:end")
   (LaTeX-add-environments
    "metanote")
   (LaTeX-add-bibliographies
    "this"))
 :latex)

