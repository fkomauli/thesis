# Contents

1. **Introduction**
    1. Verify VS Validate in PLT
        * PBT
    2. Previous work
        * Development and experiments with tools for full-scale languages
        * Binders in alphaProlog and Haskell+
    3. Current Work: PBT for Abstract Machines
        * Less structure
        * No binders (negligible in this project): registers have identity, contrary to variables
    4. List Machine
        * Logic PBT
        * Functional PBT
2. **What is PBT**
    1. Generative Only
        1. Random
            * FsCheck: QuickCheck porting with builtin naive generators
            * Lazy VS strict: efficiency, differences in generation
        2. Exhaustive
    2. Constrained Generation
        * In alphaProlog
        * Developments made by QuickChick, Feat, Redex + ESOP'15
    3. alphaProlog
        * Prolog + nominal primitives
        * alphaCheck: model checking realised with iterative deepening and negation-as-failure
            - Taking advantage of relational style, unification and search
            - Citations of case studies: TPLP, GitHub
3. **Case study**
    1. Step and Typing Rules
    2. Checks
        * Top-level
        * Mentioning unit testing
    3. Mutations (with table)
4. **alphaProlog Implementation**
    * Inspired by Twelf implementation
    * Type declarations
    * Portion of step and typechecking implementations (referencing files in the repo for full implementation)
    * Top-level checks implementation
    * Implementation of existentials
    * Example of a goals used as unit tests
    * Experimental results
5. **F# Implementation**
    * Inspired by algorithmic implementation in Coq
    * Typechecking algorithm implementation (if needed)
    * Top-level checks and implementation of existentials
    * Naive generation: insufficient coverage, discussion about checks formulation
    * Smart generators:
        - generators code example (program generator)
        - simultaneous generation as tuples
        - checking generators
        - example of parametric unit testing
        - experimental results
6. **Lessons learnt and conclusions**
    * Typos in the paper and inconsistencies with Twelf and Coq code
    * Functional VS logical PBT: random/exhaustive, prototyping/confidence
7. **Post Scriptum: NIQ**
