
It is natural to consider, in addition to the  well
trodden type soundness property, more intensional properties, such as
\emph{dynamic secure information-flow control} (IFC). This choice is
not casual, since this is extensively studied in~\cite{ifc}, using
random testing with QuickCheck. As we said, that paper challenges
naive exhaustive testing in this domain and we wanted to have a
say. We mainly concentrate on Section 1--4 of~\cite{ifc} and refer the
reader to the paper for full details of the model and their testing
outcome.


The starting point is a simple, but not simplistic, abstract
stack-and-pointer machine with instructions such as \emph{push},
\emph{pop}, \emph{load} and \emph{store}.  A machine state $\langle pc,s,m,i\rangle$ consists of
a program counter, a stack, a random-access memory, and a fixed
instructions sequence. In dynamic IFC, security levels (labels, here
only public $\perp$ and private $\top$) are attached to run-time
values and propagated during the execution, with the goal of making
sure that private data does not leak. In this model, the values
manipulated by the machines are labeled integers $x@L$.
%
The property that the abstract machine must satisfy is
\emph{end-to-end noninterference}:  let us call \emph{indistinguishable}
two machine states if they differ only in their private values; noninterference guarantees that in any execution starting with
indistinguishable states and ending in a halted state, the final states
are indistinguishable as well.

In~\cite[Section 2.3]{ifc}, the authors present an operational
semantics for the machine, which, while intuitive, turns out to be
incorrect; then (Section 4) they detail  the sophisticated testing strategies
they had to program to catch the \emph{four} bugs inserted. To give an
idea, we list the buggy rule for \emph{Load} and below the
fixed one, where \textit{i(pc) = Load} and the box signals where the
bug is/was.
\[
  \begin{small}
    \begin{array}{c}
      \infer[\mathit{Load^*}]{\langle pc,(x@L : s),m \rangle\Longrightarrow\langle pc,(\fbox{\textit{m(x)}} : s),m \rangle}{}\\
      \\
      \infer[\mathit{Load^{OK}}]{\langle pc,(x@L : s),m \rangle\Longrightarrow\langle pc,(\fbox{\textit{m(x)@L}} : s),m \rangle}{}
    \end{array}
  \end{small}
\]

QuickCheck managed to locate the first two bugs with a relatively naive
generation strategy by which two indistinguishable states were generated
together by randomly creating the first and modifying the second in
their secret part. The other two bugs necessitated a far more complex
strategy for generating meaningful sequences of instructions and
addresses to trigger the bugs% , akin to the ones we used for the \emph{FsCheck}
% implementation of the list machine
.

\aCheck found the first two bugs in less than a minute without any
setup.  The third one required writing a generator that yields more
structured programs, such as sequences of \emph{push} and \emph{store}
so that values in memory can change during execution, a necessary
condition to find distinguishable states. This generator is a
simplification of the \emph{weighted} and \emph{sequence} strategy
in~\cite{ifc}.
%
The fourth bug seemed, however, out of reach of \aCheck, until we got
it instead using a (bounded) \aProlog query for noninterference rather than a check, the
difference being the search strategy: depth-first vs.\ un-optimized
iterative deepening. After the last bug has been fixed, the query did
not find any counterexample and completes in about 5 minutes.

\cite[Section 2]{ifc} ends by introducing three additional and more
outlandish bugs. We got them all with each of the previous techniques (vanilla
check, check with a generator and query with a generator) with queries
being the more efficient one.  Additionally, we have also carried out
some experiments with a second version of the machine that includes
a \emph{jump} instruction (\cite[Section 5]{ifc}), but the results are
too preliminary to draw any conclusion.
% which sets the PC with the value on top of the stack. The
% \emph{end-to-end noninterference} property run without a generator did
% not find any counterexample after nearly one hour of execution
% time. The program generator used by the query had to be adapted to the
% new definition of the abstract machine. A new generator was designed
% to include \emph{jump} instructions and to manage numbers size
% directly: \emph{push} parameters can be limited up-to the program
% length to make them safe jump addresses. After about 25 minutes the
% query finds a counterexample similar to the one represented in
% \cite[Figure 11]{ifc}.  The solution proposed by the authors of the
% benchmark is to label the PC too. With the new definition, machine
% states are indistinguishable when either both PC are labeled
% \emph{high}, or when bot PC are labeled \emph{low} and stores and
% programs are indistinguishable. After about 15 seconds of execution,
% the query finds a counterexample where the machines terminate with
% different PC labels. However in \cite[Section 5.2]{ifc} the authors
% weaken the \emph{end-to-end noninterference} property by considering
% only ending states that are both halting and with a PC labeled
% \emph{low}. With this new weakened property, the \aProlog\ query is
% not capable of finding a counterexample within several hours of
% execution.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pbt"
%%% End:

%  LocalWords:  IFC PBT QuickCheck FsCheck ccc pc
