\chapter{{\aProlog} Implementation}
\label{aprolog_implementation}

The list-machine model has been implemented in \aProlog\ following as reference the Twelf implementation proposed by \citep{listmachine}. The relational structure of predicates follows directly the definitions of static and dynamic semantics rules given in Section \ref{semantics}. Definitions of list-machine values, types and instructions are shown in Listing \ref{lst:list_machine_aprolog_types}.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_aprolog_types},caption={list-machine type declarations in \aProlog}]
value : type.
value_nil : value.
value_cons : (value, value) -> value.

instr : type.
instr_jump : label -> instr.
instr_branch_if_nil : (var, label) -> instr.
instr_fetch_field : (var, flag, var) -> instr.
instr_cons : (var, var, var) -> instr.
instr_halt : instr.
seq : (instr, instr) -> instr.

ty : type.
ty_nil : ty.
ty_list : ty -> ty.
ty_listcons : ty -> ty.
\end{lstlisting}
\end{figure}

Variable and label types have been implemented as a finite enumeration instead of using natural numbers to speed up checks execution. Initially they were implemented as \emph{name types} with $\alpha$-equivalence. However, the list-machine model requires variable and label identity for $l_0$ and $v_0$, which are needed to define the program initial environment, and this clashes with $\alpha$-equivalence.

\section{Step and Typechecking}

Listing \ref{lst:list_machine_aprolog_step} shows the implementation of the step predicate. The code can be interpreted as plain Prolog code, while the first line is the predicate type declaration. The functional notation which appears in this listing, as explained in section \ref{aprolog}, is just syntactic sugar for predicates: the variables that seem to be assigned by the \texttt{=} operator are unified as usual.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_aprolog_step},caption={list-machine step predicate}]
pred step (program, store, instr, store, instr).
step(P, R, seq(seq(I1, I2), I3), R, seq(I1, seq(I2, I3))).
step(P, R, seq(instr_fetch_field(V1, zf, V2), I), R', I) :-
    value_cons(A, _) = var_lookup(R, V1),
    R' = var_set(R, V2, A).
step(P, R, seq(instr_fetch_field(V1, of, V2), I), R', I) :-
    value_cons(_, A) = var_lookup(R, V1),
    R' = var_set(R, V2, A).
step(P, R, seq(instr_cons(V1, V2, V3), I), R', I) :-
    A1 = var_lookup(R, V1),
    A2 = var_lookup(R, V2),
    R' = var_set(R, V3, value_cons(A1, A2)).
step(P, R, seq(instr_branch_if_nil(V, L), I), R, I) :-
    var_lookup(R, V) = value_cons(_, _).
step(P, R, seq(instr_branch_if_nil(V, L), I), R, I') :-
    var_lookup(R, V) = value_nil,
    I' = program_lookup(P, L).
step(P, R, instr_jump(L), R, I') :-
    I' = program_lookup(P, L).
\end{lstlisting}
\end{figure}

One of the most valuable characteristics of the relational style of encoding is the similarity with the inference rules given as formulas. That can be clearly seen by comparing the previous listings with the step inference rules given in Figure \ref{fig:list_machine_small_step_semantics}. Therefore, translating inference rules to \aProlog\ code and showing that the translation is adequate can be a straightforward task. As another example, Listing \ref{lst:list_machine_aprolog_typing} can be compared with Figure \ref{fig:list_machine_instruction_typing}.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_aprolog_typing},caption={list-machine instruction typing}]
pred check_instr (program_typing, env, instr, env).
check_instr(Pi, G, seq(I1, I2), G2) :-
    check_instr(Pi, G, I1, G1),
    check_instr(Pi, G1, I2, G2).
check_instr(Pi, G, instr_branch_if_nil(V, L), [(V, ty_listcons(T)) | G']) :-
    (ty_list(T), G') = env_lookup(G, V),
    G1 = block_typing_lookup(Pi, L),
    env_sub([(V, ty_nil) | G'], G1).
check_instr(Pi, G, instr_branch_if_nil(V, L), G) :-
    (ty_listcons(T), _) = env_lookup(G, V).
check_instr(Pi, G, instr_branch_if_nil(V, L), G) :-
    (ty_nil, G') = env_lookup(G,V),
    G1 = block_typing_lookup(Pi, L),
    env_sub(G, G1).
check_instr(Pi, G, instr_fetch_field(V, zf, V'), G') :-
    (ty_listcons(T), _) = env_lookup(G,V),
    G' = env_set(G, V', T).
check_instr(Pi, G, instr_fetch_field(V, of, V'), G') :-
    (ty_listcons(T), _) = env_lookup(G, V),
    G' = env_set(G, V', ty_list(T)).
check_instr(Pi, G, instr_cons(V0, V1, V), G') :-
    (T0, _) = env_lookup(G, V0),
    (T1, _) = env_lookup(G, V1),
    ty_list(T) = lub(ty_list(T0), T1),
    G' = env_set(G, V, ty_listcons(T)).
\end{lstlisting}
\end{figure}

\section{Top-Level Checks}

Properties can be written in a straightforward way. Listing \ref{lst:list_machine_acheck_checks} shows how properties implemented for \aCheck\ are the encoding of theorem statements given in Section \ref{checks} that the developer of an abstract machine wants to prove; however here are used for model checking instead of formal verification.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_acheck_checks},caption={list-machine top-level properties written for \aCheck}]
#check "progress" 10:
    check_program(P, Pi),
    check_block(Pi, G, I),
    store_has_type(R, G)
    =>
    step_or_halt(P, R, I).

#check "preservation" 13:
    check_program(P, Pi),
    env_ok(G),
    store_has_type(R, G),
    check_block(Pi, G, I),
    step(P, R, I, R', I')
    =>
    exists_env_for_store_and_block(5, Pi, R',I').

#check "soundness" 25:
    check_program(P, Pi),
    R = [(v0, value_nil)],
    I = program_lookup(P, l0),
    steps(P, R, I, R', I')
    =>
    step_or_halt(P, R', I').
\end{lstlisting}
\end{figure}

\aCheck\ allows to specify custom data generators. However, after several tries, the custom generators we tried to write resulted ineffective compared to the default ones, thus this feature was not employed.

\subsection{Existentials Encoding}
\label{acheck_existentials_encoding}

More attention must be payed when an existential is involved. \aCheck\ does not implement a primitive for handling existential quantification: every variable appearing in the property statement is universally quantified. In general, there are two possible ways of dealing with an existential quantifier, which are \emph{skolemization} and search.

Skolemization is performed by replacing an existentially quantified variable with a function whose arguments are the outer quantifiers. For example, the formula $\forall x \exists y. P(x, y)$ can be replaced with the equisatisfiable formula $\exists f. \forall x P(x, f(x))$. That means finding a function that provides the correct existential value given the universally quantified parameters. Here, the skolemization approach was inapplicable because of the relational structure of \aProlog\ programs.

The existential quantifier can be easily encoded in logic programming with a new predicate where the existential variable is local to the clause body. For example, the \emph{step-or-halt} definition given in Section \ref{semantics} has two existentially quantified variables, corresponding to the postconditions of the small-step relation. The predicate is translated to \aProlog\ in Listing \ref{lst:list_machine_step_or_halt}: here the variables \texttt{R'} and \texttt{I'} appear only in the clause body, and being in output mode in the \texttt{step} predicate they are correctly grounded.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_step_or_halt},caption={\aProlog\ encoding of \emph{step-or-halt} predicate}]
pred step_or_halt (program,store,instr).
step_or_halt(P,R,instr_halt).
step_or_halt(P,R,I) :- step(P,R,I,R',I').
\end{lstlisting}
\end{figure}

This approach cannot be applied if the existentials are not grounded, otherwise the goal would loop indefinitely.

Another approach is to exhaustively generate all possible values for the existential quantifier, until one that satisfies the predicate is found, or a maximum search depth is reached. When applied to a check, this technique leads to three possible outcomes:
\begin{itemize}
  \item a value that unifies with the existential quantified variable exists and it is within the reach of the search depth, thus the condition is verified;
  \item such value does not exist and after the maximum depth is reached the condition fails;
  \item a value exists, but is just out of reach of the specified depth bound, thus the condition fails with a false negative.
\end{itemize}

For example, in the case of the existential quantifier present in the \emph{preservation} property a bounded exhaustive search was used instead. Listing \ref{lst:list_machine_acheck_search} shows a first incorrect implementation, followed by the current one.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_acheck_search},caption={exhaustive bounded search of an existentially quantified variable}]
% implementation that causes the preservation check to loop indefinitely
pred exists_env_for_store_and_block (program_typing, store, instr).
exists_env_for_store_and_block(Pi, R, I) :-
    store_has_type(R, G),
    env_ok(G),
    check_block(Pi, G, I).

% exhaustive bounded search of existential
pred exists_env_for_store_and_block (int, program_typing, store, instr).
exists_env_for_store_and_block(N, Pi, R, I) :-
    N > 0,
    exists_env_for_store_and_block(N - 1, Pi, R, I).
exists_env_for_store_and_block(N, Pi, R, I) :-
    N >= 0,
    build_env(N, G),  % generates all possible environments of depth N
    store_has_type(R, G),
    env_ok(G),
    check_block(Pi, G, I).
\end{lstlisting}
\end{figure}

Applying the same approach used with the \emph{step-or-halt} encoding, however without considering that none of the predicates appearing in the first implementation has the environment \texttt{G} as an output variable, makes the goal loop while trying to unify it. In fact, \aProlog\ employs a depth first search strategy for goal resolution and therefore it may end in a loop that continues indefinitely.

An intrinsic problem is that \emph{store-has-type} and \emph{check-block} predicates perform typechecking, thus expecting \texttt{G} as an input variable, while type inference would have the environment \texttt{G} as an output variable instead.

As mentioned before, checks that involve searching for an existentially quantified variable may fail with a false negative. When a counterexample is found, and therefore no value was found that unifies with the existentially quantified variable, it is recommended to increase the search depth bound: this can give more confidence on the check outcome when no value for the existential quantifier is found, still there is no guarantee that such value does not exist.

\section{Unit Testing}

As mentioned in Section \ref{checks}, the benchmark includes a set of unit tests taken from the PLT-Redex implementation: 43 out of 44 unit tests were ported to \aProlog, while one was discarded because it actually was a property-based test\footnote{\url{https://github.com/racket/redex/blob/70dfd4c5565603e948154788e87b846c91341a1e/redex-examples/redex/examples/list-machine/test.rkt\#L75}}. These tests were not implemented using \aCheck, but as simple \aProlog\ goals. Parametric tests were coded exploiting the language unification of terms and negation.

Given a predicate $q(\vec{X})$ that we want to validate with a finite enumeration of input values $p(\vec{X})$, it would be incorrect to write a query like
\begin{align*}
  \texttt{? p(X), q(X).}
\end{align*}
In fact, \aProlog\ tries to discover whether the goal is satisfiable, returning a substitution for the \texttt{X} variable, which can be expressed with
\begin{align*}
  \exists \vec{X}.\ p(\vec{X}) \wedge q(\vec{X})
\end{align*}

Instead, a parametric test can be represented by the formula
\begin{align*}
  \forall \vec{X} \in p.\ q(\vec{X})
\end{align*}
where $p$ is a finite enumeration of test samples, which are used to populate variables in $\vec{X}$ and validate the predicate $q$. If we consider $p$ as the predicate that is satisfied by the values belonging to the enumeration, the test can be expressed as
\begin{align*}
  \forall \vec{X}.\ p(\vec{X}) \Rightarrow q(\vec{X})
\end{align*}
If we define $p$ as an \aProlog\ predicate that grounds all variables in $\vec{X}$, acting like a finite enumerator, we can apply a double negation to the formula, which can be rearranged as
\begin{align*}
  \neg (\exists \vec{X}.\ p(\vec{X}) \wedge \neg q(\vec{X}))
\end{align*}
thus the query can be written in \aProlog\ as
\begin{align*}
  \texttt{? not(p(X), not(q(X))).}
\end{align*}
Listing \ref{lst:list_machine_aprolog_unit_tests} shows two unit tests, with the second one being a parametric test implemented with the schema explained above.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_aprolog_unit_tests},caption={list-machine unit tests written in \aProlog}]
? subtype(ty_listcons(ty_nil), ty_list(ty_list(ty_nil))).

? not(not_same_var(X,Y), not(check_block(
    [],                                        % program typing
    [(X,ty_listcons(ty_nil))],                 % environment
    seq(instr_fetch_field(X,zf,Y),instr_halt)  % instruction
  ))).
\end{lstlisting}
\end{figure}

The first unit test contains only constants, thus is implemented as a plain query. In the second example, variables \texttt{X} and \texttt{Y} are made ground by the \texttt{not\_same\_var} predicate and used to validate the \texttt{check\_block} predicate.

\section{Experimental Results}

While developing the abstract machine in \aProlog, \aCheck\ has been a valuable tool for finding subtle bugs that can occur in a Prolog-like program, despite the benefit of static typing added to the language, such as typos in variable names and missing or mixed up predicates. The \emph{preservation} check was useful even in finding a bug related to a wrong specification of a predicate used by the \emph{preservation} theorem statement in the paper \citep{listmachine}, but this case will be further discussed in Section \ref{not_only_mutants} of the conclusive chapter.

But how well do certain properties behave compared to other ones in finding both trivial bugs and wrong abstractions? And is PBT a better choice over unit testing? The working hypothesis assumed that both top-level properties, such as the triad \emph{progress}, \emph{preservation} and \emph{soundness}, and more specific properties regarding lower level lemmas would be necessary to guarantee an adequate test suite. Moreover, it assumed that the use of an exhaustive generation strategy would also cover the cases checked by unit tests.

Mutation testing was applied to the \aProlog\ list-machine implementation, as summarized in Table \ref{tbl:list_machine_mutations}, to find out whether the forementioned hypothesis would be observed when applied to different kinds of bugs. Table \ref{tbl:list_machine_aprolog_test_results} recaps the results obtained by running all test suites on each of the twenty mutations considered in this benchmark. For example, the first mutant was killed by a unit test, the \emph{progress} check at depth 13 and the \emph{soundness} check at depth 19.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | c | p{10cm} |}
    \hline
    ID & Failing unit tests & Counterexamples found by checks (at depth)\\
    \hline
    \hline
    1 & 1 & progress(13), soundness(19)\\
    \hline
    2 & 0 & var-set-lookup(9), step\_deterministic R(9), preservation(13), soundness(23)\\
    \hline
    3 & 1 & preservation(13), soundness(23)\\
    \hline
    4 & 0 & step\_deterministic R(6)\\
    \hline
    5 & 0 & block-typing-lookup-uniq(6), progress-typing-dom-match(7), progress-check-program(18), soundness(25)\\
    \hline
    6 & 1 & preservation(13), soundness(23)\\
    \hline
    7 & 1 & progress(13), soundness(23)\\
    \hline
    8 & 0 & soundness(23)\\
    \hline
    9 & 0 & progress(13), safety(13), soundness(13)\\
    \hline
    10 & 0 & env-lookup removes variable*(7)\\
    \hline
    11 & 3 & env-set-lookup(5), progress-env(7), progress(13), subsumption-env(7), preservation-env-lookup(7)\\
    \hline
    12 & 4 & env-set-lookup(3), preservation(13)\\
    \hline
    13 & 1 & lub-subtype-left(3), lub-subtype-right(3)\\
    \hline
    14 & 1 & lub\_idem(1), lub\_of\_subtype(1)\\
    \hline
    15 & 0 & preservation(13)\\
    \hline
    16 & 1 & preservation(13)\\
    \hline
    17 & 1 & \ \\
    \hline
    18 & 0 & preservation-block-typing(4), preservation-program-typing(12), preservation-step-branch-taken(12), preservation(12), run-prog-finite-well-typed(12), run-well-typed(12), safety(12), soundness(12)\\
    \hline
    19 & 0 & soundness(19)\\
    \hline
    20 & 0 & run-prog-finite-well-typed(13), run-well-typed(13), safety(13), soundness(13)\\
    \hline
  \end{tabular}
\]
\caption{test results for each mutation, listing all checks that find a counterexample}
\label{tbl:list_machine_aprolog_test_results}
\end{table}

The "\emph{env-lookup removes variable}" check is marked with an asterisk (*) because it was not present in the original benchmark but it was written after the mutation, knowing exactly where the bug is. It was added to find out a property that could cover this case.

Checks are set to a maximum search depth so as to make the check execution time close to 10 seconds. Exception are the \emph{progress} check and the various preservation checks, such as \emph{preservation-step-branch-not-taken}, \emph{preservation-step-branch-taken}, and the \emph{preservation} check itself. This last one takes several minutes to complete the last depth iteration, having a particular search space structure that explodes at depth 13, while to completely search depth 12 it takes about few milliseconds.

The implemented checks are grouped in three categories:
\begin{itemize}
  \item 3 properties derived from top-level theorems in \citep{listmachine};
  \item 13 from lemmas contained in the same paper;
  \item 34 auxiliary checks ported from lower level lemmas contained in the Twelf benchmark implementation.
\end{itemize}

Comparisons between top-level checks, checks from lemmas, auxiliary checks and unit tests are represented in Table \ref{tbl:list_machine_aprolog_test_comparison}. Ticks marked with a double asterisk (**) took several minutes, on the machine used to run the benchmark referenced in Section \ref{project_details}, to find a counterexample. Therefore they can be counted out if the check execution speed is considered essential, and in the case of rapid prototyping and development of abstract machine models it is. The single asterisk (*) means, as forementioned, that the counterexample was found by a check that was not included in the original benchmark \citep{listmachine}.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | c | c | c | c |}
    \hline
    ID & Theorems & Lemmas & Lower level lemmas & Unit tests\\
    \hline
    \hline
    1 & \checkmark & \ & \ & \checkmark\\
    \hline
    2 & \checkmark & \ & \checkmark & \ \\
    \hline
    3 & \checkmark & \ & \ & \checkmark\\
    \hline
    4 & \ & \ & \checkmark & \ \\
    \hline
    5 & \checkmark ** & \checkmark & \checkmark & \ \\
    \hline
    6 & \checkmark & \ & \ & \checkmark\\
    \hline
    7 & \checkmark & \ & \ & \checkmark\\
    \hline
    8 & \checkmark & \ & \ & \ \\
    \hline
    9 & \checkmark & \ & \checkmark & \ \\
    \hline
    10 & \ & \ & \checkmark * & \ \\
    \hline
    11 & \checkmark ** & \checkmark & \checkmark & \checkmark\\
    \hline
    12 & \checkmark ** & \ & \checkmark & \checkmark\\
    \hline
    13 & \ & \checkmark & \ & \checkmark\\
    \hline
    14 & \ & \ & \checkmark & \checkmark\\
    \hline
    15 & \checkmark & \ & \ & \ \\
    \hline
    16 & \checkmark & \ & \ & \checkmark\\
    \hline
    17 & \ & \ & \ & \checkmark\\
    \hline
    18 & \checkmark & \checkmark & \checkmark & \ \\
    \hline
    19 & \checkmark & \ & \ & \ \\
    \hline
    20 & \checkmark & \checkmark & \checkmark & \ \\
    \hline
  \end{tabular}
\]
\caption{comparing the results of checks about top-level theorems and lemmas, auxiliary checks and unit tests}
\label{tbl:list_machine_aprolog_test_comparison}
\end{table}

Table \ref{tbl:list_machine_aprolog_test_comparison} shows that most
mutants are killed by the top-level checks: 12 over 20 cases (not
considering the excessively long running checks), 10 with just the
\emph{soundness} check alone. The score raises up to 15/20 if
properties taken from lemmas are included. Auxiliary checks alone
scored 10 kills (the 10th mutation counterexample is not counted in),
increasing the coverage score to 18/20. Also unit tests signaled the
presence of a bug 10 times.

The \emph{soundness} check is the one that performs better, by killing 10 mutants alone (presuming that check execution time were set with a one minute timeout). It is followed by the \emph{preservation} check with 6 kills and the \emph{progress} check with just 1 killed mutant (other 3 mutants were killed but check execution time was far above a minute). Table \ref{tbl:list_machine_acheck_top_level} summarizes the execution times of each top-level check and relative depth where the counterexample has been found or when depth limit has been reached. Checkmarks indicate that a counterexample was found, while tildes signal that the check run above a minute to find it. In the \emph{preservation} check, the existential search depth was set to 4. Some check was interrupted after several hours of execution without finding any counterexample or reaching the maximum depth.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | c | r | r | c | r | r | c | r | r |}
    \hline
    \ & \multicolumn{3}{c|}{Progress} & \multicolumn{3}{c|}{Preservation} & \multicolumn{3}{c|}{Soundness}\\
    \hline
    Mutant & \checkmark & Depth & Time & \checkmark & Depth & Time & \checkmark & Depth & Time\\
    \hline
    \hline
    \ & \ & 13 & 29'31'' & \ & 13 & 4h13' & \ & 25 & 8.098\ s\\
    \hline
    \hline
    1 & $\sim$ & 13 & 7'31'' & \ & 13 & 32'13'' & \checkmark & 19 & 0.291\ s\\
    \hline
    2 & \ & 13 & 21'00'' & \checkmark & 13 & 5.517\ s & \checkmark & 23 & 1.794\ s\\
    \hline
    3 & \ & 13 & 22'10'' & \checkmark & 13 & 4.632\ s & \checkmark & 23 & 1.811\ s\\
    \hline
    4 & \ & 13 & 25'24'' & \ & 13 & 35'47'' & \ & 25 & 31.20\ s\\
    \hline
    5 & \ & 13 & 22'50'' & \ & 13 & 33'20 & $\sim$ & 25 & 1h12'36''\\
    \hline
    6 & \ & 13 & 19'48'' & \checkmark & 13 & 2.168\ s & \checkmark & 23 & 1.881\ s\\
    \hline
    7 & $\sim$ & 13 & 4'25'' & \ & 13 & 26'11'' & \checkmark & 23 & 1.751\ s\\
    \hline
    8 & \ & 13 & 21'15'' & \ & 13 & 30'51'' & \checkmark & 23 & 1.786\ s\\
    \hline
    9 & \checkmark & 13 & 13.5\ ms & \ & 13 & 31'07'' & \checkmark & 13 & 13.67\ ms\\
    \hline
    10 & \ & 13 & 24'21'' & \ & 13 & 40'11'' & \ & 25 & 30.88\ s\\
    \hline
    11 & $\sim$ & 13 & 3'43'' & \ & 13 & 24'38'' & \ & 25 & 7.755\ s\\
    \hline
    12 & \ & 13 & 20'29'' & $\sim$ & 13 & 22'25'' & \ & 25 & 7.496\ s\\
    \hline
    13 & \ & 13 & 20'38'' & \ & 13 & 29'37'' & \ & 25 & 7.673\ s\\
    \hline
    14 & \ & 13 & 16'20'' & \ & 13 & 29'58'' & \ & 25 & 7.691\ s\\
    \hline
    15 & \ & 13 & 18'05'' & \checkmark & 13 & 4.422\ s & \ & 25 & 7.602\ s\\
    \hline
    16 & \ & 13 & 20'52'' & \checkmark & 13 & 21.66\ s & \ & 25 & 7.655\ s\\
    \hline
    17 & \ & 13 & 14.03 & \ & 13 & 4'18'' & \ & 25 & 7.171\ s\\
    \hline
    18 & \ & \multicolumn{2}{c|}{interrupted} & \checkmark & 12 & 3.142\ s & \checkmark & 12 & 368.0\ ms\\
    \hline
    19 & \ & 13 & 21'09'' & \ & 13 & 1h20'53'' & \checkmark & 19 & 487.1\ ms\\
    \hline
    20 & \ & 13 & 10h17' & \ & \multicolumn{2}{c|}{interrupted} & \checkmark & 13 & 62.00\ ms\\
    \hline
  \end{tabular}
\]
\caption{\emph{progress}, \emph{preservation} and \emph{soundness} check execution times}
\label{tbl:list_machine_acheck_top_level}
\end{table}

As a conclusion, the top-level properties, could provide a good score in killing mutants. Implementing checks for just a few properties can take very short time and effort, an aspect that is more valuable when the abstract machine model changes frequently: top-level checks can be updated to reflect modifications of the model in a straightforward way. If proofs of that theorems have already been outlined, lemmas can be ported to \aCheck\ to increase coverage. To reach a nearly complete coverage, both auxiliary predicates and unit tests could be a valuable option. However, their benefits come with the price of less flexibility when changing the model under test: while the benchmark has 3 top-level checks and 13 lemmas, there are 34 auxiliary checks and 43 unit tests. Unit tests are faster to run (few milliseconds on the reference machine), against the minutes or even hours required to run checks, depending on each check maximum depth.

\aCheck\ does not support a check execution timeout yet. If such feature were present, it would be a reasonable option to set it to one minute for each check. Unit tests execution takes just few milliseconds to complete, while \emph{soundness} check finds a counterexample mostly within 2 seconds. The results discussed above suggest that the \emph{soundness} check together with unit tests may be the most suitable option: 16 mutants out of 20 are killed by this combination, within just 2 seconds of overall execution.

Mutation testing has been a valuable tool to assess actual performances of PBT and unit testing applied to mechanized metatheory. In a real world project of programming language development, it can be applied to get a measure on how well test suites are performing for the model under test. Moreover, the experimental results have shown that checking the statement of the \emph{soundness} theorem is already a good solution for discovering errors in the developed model.

\subsection{Unexpected Results}

Noteworthy is the case of mutation 17, which is killed by a unit test but no check is able to find any counterexample, as reported in Table \ref{tbl:list_machine_aprolog_test_results}. With mutation 17 applied, the \emph{check-block} predicate is \emph{``checking again the first instruction of a sequence instead of the second''}. Listing \ref{lst:aprolog_check_block_mutation_17} shows the mutated code with instruction \texttt{I1} passed to \texttt{check\_block} instead of \texttt{I2}.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:aprolog_check_block_mutation_17},caption={\emph{check-block} predicate with an error introduced by mutation 17}]
pred check_block (program_typing,env,instr).
check_block(Pi,G,instr_halt).
check_block(Pi,G,seq(I1,I2)) :-
    check_instr(Pi,G,I1,G1),
    check_block(Pi,G1,I1).  % I1 instead of I2
check_block(Pi,G,instr_jump(L)) :-
    G' = block_typing_lookup(Pi,L),
    env_sub(G,G').
\end{lstlisting}
\end{figure}

The mutated predicate discards all blocks that are not just a single \textbf{halt} or \textbf{jump} terminal instruction, thus narrowing the set of well-typed programs to those containing no instruction other than the terminal ones. Most checks have the \emph{check-block} predicate between their property preconditions, often in an indirect way by using \emph{check-blocks} or \emph{check-program} instead. Having stricter preconditions does not invalidate such properties, therefore no counterexample is produced and the mutation would remain unnoticed.

The unit test that killed the 17th mutant directly addresses the \emph{check-block} predicate instead, by providing a well-typed program and expecting it to typecheck. The test can be summarized as
\begin{align*}
  \begin{array}{c}
    X \neq Y \quad \mathit{\Gamma} = [(X: \mbox{listcons nil})] \quad \mathit{\Pi} = \{\ \}\\
    \hline
    \checkblock{\Pi}{\Gamma}{\mbox{\textbf{fetch-field}} X 0 Y; \mbox{\textbf{halt}}}
  \end{array}
\end{align*}

When the \textbf{fetch-field} instruction is recursively passed to \emph{check-block} no clause matches it, thus the predicate fails.

To cover this case even with \aCheck, we can generalize the counterexample provided by the unit test that kills the mutant. For example, we can state that a sequence of a well-typed instruction and a well-typed block is a well-typed block, and implement a check as shown in Listing \ref{lst:aprolog_check_killing_mutation_17}, which finds a similar counterexample at depth 5.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:aprolog_check_killing_mutation_17},caption={a check designed to kill mutant 17}]
#check "sequence well-typedness" 6:
    check_instr(Pi,G,I,G'),
    check_block(Pi,G',I')
    =>
    check_block(Pi,G,seq(I,I')).
\end{lstlisting}
\end{figure}
