\chapter{Case Study: the list-machine}
\label{case_study}

Several typed assembly languages have been studied or proposed as examples and benchmarks in the literature, such as Mini-ML \citep{miniml}, WebAssembly \citep{webassembly}, the Ethereum VM \citep{ethereumvm}, and the Racket VM \citep{racketvmtesting}. The benchmark that has been considered in this thesis is a typed assembly language named list-machine. It was developed and proposed as a benchmark for mechanized metatheory by \citep{listmachine}, as a way to compare different proof assistants, starting from Twelf and Coq. The original purpose of the list-machine benchmark was to test proofs of compiler correctness with the mentioned tools for theorem proving. On the other hand, the work of this thesis is focused on model checking the list-machine.

The \aProlog\ language together with \aCheck\ were designed to model check high-level languages, with binding and other forementioned features regarding such languages. The proposed benchmark is somehow a new kind of model to be treated with these tools. Therefore, this benchmark is also a way to assess the expressiveness and usability of \aProlog\ and \aCheck\ in modeling and checking other kinds of languages, such as typed assembly languages.

Model checking of list-machine is implemented as validation of desired properties, which have the same meaning of the theorems proved in \citep{listmachine}, such as \emph{progress}, \emph{preservation}, \emph{soundness} and other lemmas. Because the list-machine typechecker has an algorithmic and a relational implementation in Coq, two different languages have been used to model check the abstract machine in both specifications. The relational implementation has been done in the logic language \aProlog. The functional implementation has been developed in F\#, a strict functional language. This double implementation has also allowed a comparison between PBT made with random generation with the FsCheck library and bounded exhaustive generation with \aCheck.

As forementioned in section \ref{previous_work}, the application of PBT to programming language metatheory has been widely experimented with promising results. The list-machine benchmark has been already involved in formalization and validation through PLT-Redex. The tool examples repository contains a list-machine implementation\footnote{\url{https://github.com/racket/redex/blob/master/redex-examples/redex/examples/list-machine/}}, whose unit tests\footnote{\url{https://github.com/racket/redex/blob/master/redex-examples/redex/examples/list-machine/test.rkt}} have been ported to this thesis benchmark implementation. Moreover, the repository contains some examples of mutation testing\footnote{\url{https://github.com/racket/redex/tree/master/redex-benchmark/redex/benchmark/models/list-machine}}, which have been further investigated and extended in this thesis project.

In this chapter we present the common features shared by the two implementations, functional and relational. The next sections will give an overview on the list-machine specification, its main characteristics, the properties derived from the theorems definitions that have to be checked, and a list of mutations manually introduced into the implementation that should give evidence about the effectiveness of checks over possible errors of any kind.

\section{Static and Dynamic Semantics}
\label{semantics}

The list-machine, as the name suggests, operates only on lists, which are \emph{cons} cells or \emph{nil}. Every value is either nil or the cons of two values. The type system is just an abstraction over pointers, where the nil value can be seen both as the empty list and the null pointer. Formally speaking, a list-machine value is an S-expression with only nil as atom.

\begin{figure}[ht!]
\[
  \begin{array}{lll}
    a_1,\ a_2,\ \dots & :\ A & \mbox{values} \\
    \hline
    \mbox{nil} & :\ A & \mbox{the empty list} \\
    \mbox{cons}(a_1,\ a_2) & :\ A & \mbox{list cell} \\
  \end{array}
\]
\caption{list-machine values}
\label{fig:list_machine_values}
\end{figure}

\emph{Variables} are indexed by natural numbers. The same applies to \emph{labels}, that are used to index instruction blocks.

\begin{figure}[ht!]
\[
  \begin{array}{lll}
    \iota_1,\ \iota_2,\ \dots & :\ I & \mbox{instructions} \\
    \hline
    \mbox{\textbf{jump}}\ l & :\ I & \mbox{jump to label $l$} \\
    \mbox{\textbf{branch-if-nil}}\ v\ l & :\ I & \mbox{if $v = \mbox{nil}$ then jump to $l$} \\
    \mbox{\textbf{fetch-field}}\ v\ 0\ v' & :\ I & \mbox{fetch the head of $v$ into $v'$} \\
    \mbox{\textbf{fetch-field}}\ v\ 1\ v' & :\ I & \mbox{fetch the tail of $v$ into $v'$} \\
    \mbox{\textbf{cons}}\ v_0\ v_1\ v' & :\ I & \mbox{make a cons cell in $v'$} \\
    \mbox{\textbf{halt}} & :\ I & \mbox{stop executing} \\
    \iota_0;\ \iota_1 & :\ I & \mbox{sequential composition} \\
  \end{array}
\]
\caption{list-machine instructions}
\label{fig:list_machine_instructions}
\end{figure}

\emph{Programs} are association lists of labels and instructions. The \emph{program-lookup} relation $p(l)$ fetches the instruction block associated to label $l$.

\begin{figure}[ht!]
\[
  \begin{array}{lll}
    p,\ \dots & :\ P & \mbox{programs} \\
    \hline
    l_n: \iota;\ p & :\ P & \mbox{labeled block} \\
    \mbox{\textbf{end}} & :\ P & \ \\
  \end{array}
\]
\caption{list-machine programs}
\label{fig:list_machine_programs}
\end{figure}

A \emph{store} is a list of bindings of variables and values. The \emph{var-lookup} relation $r(v)$ fetches the value bound to variable $v$.

\begin{figure}[ht!]
\[
  \begin{array}{lll}
    r,\ \dots & :\ R & \mbox{stores} \\
    \hline
    r [v \mapsto a] & :\ R & \mbox{bind $a$ to $v$} \\
    \{\ \} & :\ R & \mbox{empty} \\
  \end{array}
\]
\caption{list-machine stores}
\label{fig:list_machine_stores}
\end{figure}

The definition of binding $r [v \mapsto a]$ given in Figure \ref{fig:list_machine_stores} presumes that the variable $v$ is not in the domain of $r$. To keep this invariant true, an update operator \emph{var-set} is defined, represented as $r [v := a] = r'$, whose inference rules are listed in Figure \ref{fig:list_machine_store_update}.

\begin{figure}[ht!]
\[
  \begin{array}{c}
    \begin{array}{c}
      \\
      \hline
      (r[v \mapsto a]) [v := a'] = (r[v \mapsto a'])
    \end{array}\\[1cm]
    \begin{array}{c}
      v \neq v' \quad r(v') = a'\\
      \hline
      (r[v \mapsto a])(v') = a'
    \end{array}\\[1cm]
    \begin{array}{c}
      \\
      \hline
      \{\ \} [v := a] = \{\ \} [v \mapsto a]
    \end{array}
  \end{array}
\]
\caption{list-machine store update relation}
\label{fig:list_machine_store_update}
\end{figure}

The operational semantics is summarized in Figure \ref{fig:list_machine_small_step_semantics}, representing the small-step relation $\smallstep{r}{\iota}{p}{r'}{\iota'}$. The $p$ meta-variable represents the \emph{program} and $p(l)$ fetches the instruction associated to label $l$. The $r$ meta-variable represents the store where variables are associated to values: $r(v)$ fetches the value assigned to variable $v$.

\begin{figure}[ht!]
\[
  \begin{array}{c}
    \begin{array}{c}
      \\
      \hline
      \smallstep{r}{(\iota_1;\ \iota_2);\ \iota_3}{p}{r}{\iota_1;\ (\iota_2;\ \iota_3)}
    \end{array} \quad \mbox{step-seq}\\[1cm]
    \begin{array}{c}
      r(v) = \mbox{cons}(a_0,\ a_1) \quad r[v' := a_0] = r'\\
      \hline
      \smallstep{r}{(\mbox{\textbf{fetch-field}}\ v\ 0\ v';\ \iota)}{p}{r'}{\iota}
    \end{array} \quad \mbox{step-fetch-field-0}\\[1cm]
    \begin{array}{c}
      r(v) = \mbox{cons}(a_0,\ a_1) \quad r[v' := a_1] = r'\\
      \hline
      \smallstep{r}{(\mbox{\textbf{fetch-field}}\ v\ 1\ v';\ \iota)}{p}{r'}{\iota}
    \end{array} \quad \mbox{step-fetch-field-1}\\[1cm]
    \begin{array}{c}
      r(v_0) = a_0 \quad r(v_1) = a_1 \quad r[v' := \mbox{cons}(a_0,\ a_1)] = r'\\
      \hline
      \smallstep{r}{(\mbox{\textbf{cons}}\ v_0\ v_1\ v';\ \iota)}{p}{r'}{\iota}
    \end{array} \quad \mbox{step-cons}\\[1cm]
    \begin{array}{c}
      r(v) = \mbox{cons}(a_0,\ a_1)\\
      \hline
      \smallstep{r}{(\mbox{\textbf{branch-if-nil}}\ v\ l;\ \iota)}{p}{r}{\iota}
    \end{array} \quad \mbox{step-branch-not-taken}\\[1cm]
    \begin{array}{c}
      r(v) = \mbox{nil} \quad p(l) = \iota'\\
      \hline
      \smallstep{r}{(\mbox{\textbf{branch-if-nil}}\ v\ l;\ \iota)}{p}{r}{\iota'}
    \end{array} \quad \mbox{step-branch-taken}\\[1cm]
    \begin{array}{c}
      p(l) = \iota'\\
      \hline
      \smallstep{r}{\mbox{\textbf{jump}}\ l}{p}{r}{\iota'}
    \end{array} \quad \mbox{step-jump}
  \end{array}
\]
\caption{list-machine small-step semantics}
\label{fig:list_machine_small_step_semantics}
\end{figure}

For example, the \emph{step-fetch-field-1} inference rule describes the machine state required to make a step with the \textbf{fetch-field} instruction and how the state changes after its execution. Fetching a field, the second one in this case, can be done only if the value referenced by the variable $v$ is a cons cell, while with a nil value the instruction is not applicable: this is dictated by the precondition $r(v) = \mbox{cons}(a_0, a_1)$. Moreover, the \textbf{fetch-field} has to be the first element in a sequence of instructions, thus it cannot be the last instruction of a block. The step transition yields a machine state where the new instruction is the next one in the sequence and the store $r'$ is an update of $r$ where the second field $a_1$ is bound to the variable $v'$, as expressed by $r[v' := a_1] = r'$.

The big-step semantics is defined as the Kleene closure of the small-step relation, with the instruction \textbf{halt} that signals the end of a program execution. A program $p$ is said to \emph{run} if it runs in the big-step relation, starting from the instruction at $p(l_0)$ with an initial store containing only the binding $v_0 = \mbox{nil}$, until a \textbf{halt} instruction is reached. Inference rules are given in figure \ref{fig:list_machine_big_step_semantics}.

\begin{figure}[ht!]
\[
  \begin{array}{c}
    \begin{array}{c}
      \smallstep{r}{\iota}{p}{r'}{\iota'} \quad (p, r', \iota') \Downarrow\\
      \hline
      (p, r, \iota) \Downarrow
    \end{array} \quad \mbox{run-step}\\[1cm]
    \begin{array}{c}
      \ \\
      \hline
      (p, r, \mbox{\textbf{halt}}) \Downarrow
    \end{array} \quad \mbox{run-halt}\\[1cm]
    \begin{array}{c}
      [v_0 = \mbox{nil}] = r \quad p(l_0) = \iota \quad (p, r, \iota) \Downarrow\\
      \hline
      p \Downarrow
    \end{array} \quad \mbox{run-prog}
  \end{array}
\]
\caption{list-machine big-step semantics}
\label{fig:list_machine_big_step_semantics}
\end{figure}

The list-machine type system assigns to each variable a list type. The list type is then refined to empty and nonempty lists, to guarantee safety of certain operations, such as \textbf{fetch-field}.

\begin{figure}[ht!]
\[
  \begin{array}{lll}
    \tau,\ \dots & :\ T & \mbox{types} \\
    \hline
    \mbox{nil} & :\ T & \mbox{singleton type containing nil} \\
    \mbox{list}\ \tau & :\ T & \mbox{list whose elements have type $\tau$} \\
    \mbox{listcons}\ \tau & :\ T & \mbox{non-nil list of $\tau$} \\
  \end{array}
\]
\caption{list-machine types}
\label{fig:list_machine_types}
\end{figure}

The type system includes a subtyping relation: nil and listcons are both subtypes of the list type, and type parameters of list and listcons are covariant.

The \emph{least common supertype} $\tau = \tau'\ \sqcup\ \tau''$ of $\tau'$ and $\tau''$ is defined as the smallest type $\tau$ such that both types $\tau'$ and $\tau''$ are subtypes of $\tau$.

An \emph{environment} $\mathit{\Gamma}$ is a mapping between variables and types. Subtyping is extended to environments widthwise and depthwise. Because an environment $\mathit{\Gamma}$ may have multiple mappings of a variable, the predicate \emph{env-ok} checks that an environment is a single-valued function, represented in formulas as $\envok{\Gamma}$.

A \emph{program typing} $\mathit{\Pi}$ is an association list of labeled environments, where $\mathit{\Pi(l) = \Gamma}$ represents the types of the variables when entering a block of the program labeled with $l$.

\begin{figure}[ht!]
\[
  \begin{array}{lll}
    \mathit{\Pi},\ \dots & :\ PT & \mbox{program typing} \\
    \hline
    \{\ \} & :\ PT & \mbox{empty program typing} \\
    l: \mathit{\Gamma}, \mathit{\Pi} & :\ PT & \mbox{block typing}
  \end{array}
\]
\caption{list-machine program typing}
\label{fig:list_machine_program_typing}
\end{figure}

Instruction typing is defined as a judgment $\checkinstr{\Pi}{\Gamma}{\iota}{\Gamma'}$, meaning that the Hoare triple $\mathit{\Gamma\{\iota\}\Gamma}'$ relates precondition $\mathit{\Gamma}$ to postcondition $\mathit{\Gamma'}$ under the program typing $\mathit{\Pi}$. This relation is represented by Figure \ref{fig:list_machine_instruction_typing}. The \emph{check-instr-branch-listcons} typing rule has been marked with an asterisk (*) because the inference rule in \citep{listmachine} contains additional preconditions similar to those in the \emph{check-instr-branch-list} rule, regarding the jump target environment; however in the Coq implementation they are not present, which seems legit because there cannot be a jump with a variable of listcons $\tau$ type; therefore they have been removed here.

\begin{figure}[ht!]
\[
  \begin{array}{c}
    \begin{array}{c}
      \checkinstr{\Pi}{\Gamma}{\iota_1}{\Gamma'} \quad \checkinstr{\Pi}{\Gamma'}{\iota_2}{\Gamma''}\\
      \hline
      \checkinstr{\Pi}{\Gamma}{\iota_1;\ \iota_2}{\Gamma''}
    \end{array} \quad \mbox{check-instr-seq}\\[1cm]
    \begin{array}{c}
      \mathit{\Gamma(v) = \mbox{list}\ \tau \quad \Pi(l) = \Gamma_1 \quad \Gamma[v := \mbox{nil}] = \Gamma' \quad \Gamma' \subset \Gamma_1}\\
      \hline
      \checkinstr{\Pi}{\Gamma}{\mbox{\textbf{branch-if-nil}}\ v\ l}{(v : \mbox{listcons}\ \tau,\ \Gamma')}
    \end{array} \quad \mbox{check-instr-branch-list}\\[1cm]
    \begin{array}{c}
      \mathit{\Gamma(v) = \mbox{listcons}\ \tau}\\
      \hline
      \checkinstr{\Pi}{\Gamma}{\mbox{\textbf{branch-if-nil}}\ v\ l}{\Gamma}
    \end{array} \quad \mbox{check-instr-branch-listcons*}\\[1cm]
    \begin{array}{c}
      \mathit{\Gamma(v) = \mbox{nil} \quad \Pi(l) = \Gamma_1 \quad \Gamma \subset \Gamma_1}\\
      \hline
      \checkinstr{\Pi}{\Gamma}{\mbox{\textbf{branch-if-nil}}\ v\ l}{\Gamma}
    \end{array} \quad \mbox{check-instr-branch-nil}\\[1cm]
    \begin{array}{c}
      \mathit{\Gamma(v) = \mbox{listcons}\ \tau \quad \Gamma[v' := \tau] = \Gamma'}\\
      \hline
      \checkinstr{\Pi}{\Gamma}{\mbox{\textbf{fetch-field}}\ v\ 0\ v'}{\Gamma'}
    \end{array} \quad \mbox{check-instr-fetch-0}\\[1cm]
    \begin{array}{c}
      \mathit{\Gamma(v) = \mbox{listcons}\ \tau \quad \Gamma[v' := \mbox{list}\ \tau] = \Gamma'}\\
      \hline
      \checkinstr{\Pi}{\Gamma}{\mbox{\textbf{fetch-field}}\ v\ 1\ v'}{\Gamma'}
    \end{array} \quad \mbox{check-instr-fetch-1}\\[1cm]
    \begin{array}{c}
      \mathit{\Gamma(v_0) = \tau_0 \quad \Gamma(v_1) = \tau_1}\\
      \mathit{(\mbox{list}\ \tau_0)\ \sqcup\ \tau_1 = \mbox{list}\ \tau \quad \Gamma[v := \mbox{listcons}\ \tau] = \Gamma'}\\
      \hline
      \checkinstr{\Pi}{\Gamma}{\mbox{\textbf{cons}}\ v_0\ v_1\ v}{\Gamma'}
    \end{array} \quad \begin{array}{c}\\\mbox{check-instr-cons}\\\end{array}
  \end{array}
\]
\caption{list-machine instruction typing}
\label{fig:list_machine_instruction_typing}
\end{figure}

Taking again as example the \textbf{fetch-field} instruction, the \emph{check-instr-fetch-1} inference rule describes in which typing environment the instruction is well-typed and how the environment evolves. The precondition $\mathit{\Gamma}(v) = \mbox{listcons}\ \tau$ dictates that the type of variable $v$ must be a non-empty list, so it is safe to fetch a field, the second one in this case. The typing relation then gives an updated environment $\mathit{\Gamma'}$ as postcondition, with the type $\mbox{list} \tau$ bound to the variable $v'$ as stated by $\mathit{\Gamma[v' := \mbox{list}\ \tau] = \Gamma'}$. In fact, it is not foreseeable during typechecking whether the second field of a cons cell is another cons or a nil value.

Instruction level typing does not include the \textbf{halt} and the \textbf{jump} instructions. They are contemplated by the \emph{check-block} relation, whose inference rules are described in Figure \ref{fig:list_machine_block_typing}.

\begin{figure}[ht!]
\[
  \begin{array}{c}
    \begin{array}{c}
      \\
      \hline
      \checkblock{\Pi}{\Gamma}{\mbox{\textbf{halt}}}
    \end{array} \quad \mbox{check-block-halt}\\[1cm]
    \begin{array}{c}
      \checkinstr{\Pi}{\Gamma}{\iota_1}{\Gamma'} \quad \checkblock{\Pi}{\Gamma'}{\iota_2}\\
      \hline
      \checkblock{\Pi}{\Gamma'}{\iota_1; \iota_2}
    \end{array} \quad \mbox{check-block-seq}\\[1cm]
    \begin{array}{c}
      \mathit{\Pi(l) = \Gamma_1 \quad \Gamma \subset \Gamma_1}\\
      \hline
      \checkblock{\Pi}{\Gamma'}{\mbox{\textbf{jump}} l}
    \end{array} \quad \mbox{check-block-jump}
  \end{array}
\]
\caption{list-machine block typing}
\label{fig:list_machine_block_typing}
\end{figure}

The block typing relation is extended to all blocks of a program $p$ with the \emph{check-blocks} relation $\checkblocks{\Pi}{p}$: for each label $l$ in program $p$, the associated block $\iota = p(l)$ is typechecked with its relative environment $\mathit{\Gamma = \Pi(l)}$.

The \emph{typechecking} relation $\checkprogram{p}{\Pi}$ states that a program $p$ typechecks with a program typing $\mathit{\Pi}$ when blocks and environments have matching labels, every block typechecks with its corresponding environment, and the initial environment is $\mathit{\Gamma_0 = (v_0: \mbox{nil})}$.

Types are always associated to variables in the typechecking relation and never directly to values. However, the theorems that are described in the following section need a notion of values matched with types, and more generally of stores matching typing environments. When a store matches an environment, which is represented as $r: \mathit{\Gamma}$, each type bound to a variable in the environment $\mathit{\Gamma}$ has a corresponding value in the store $r$ matching that type.

The $\mbox{step-or-halt}(p,\ r,\ \iota)$ predicate indicates that an instruction $\iota$ can either make a step with $r$ as store, or it is an \textbf{halt} instruction:
\begin{align*}
  \mbox{step-or-halt}(p, r, \iota) \equiv (\exists r', \iota'.\ \smallstep{r}{\iota}{p}{r'}{\iota'}) \vee (\iota = \mbox{\textbf{halt}})
\end{align*}

\section{Checks}
\label{checks}

In the original benchmark of \citep{listmachine}, the authors implemented the abstract machine in Twelf and Coq to verify its type-correctness. The main theorems of interest that have been proven are given below.

\begin{description}
  \item[Progress:] given a well-typed instruction the machine should always be able to advance or halt, given a store that matches the correct environment type.
  \begin{align*}
    \begin{array}{c}
      \checkprogram{p}{\Pi} \quad \checkinstr{\Pi}{\Gamma}{\iota}{\Gamma'} \quad \mathit{r: \Gamma}\\
      \hline
      \mbox{step-or-halt}(p,\ r,\ \iota)
    \end{array}
  \end{align*}
  \item[Preservation:] if a well-typed block can step forward, then exists an environment that typechecks the next instruction.
  \begin{align*}
    \begin{array}{c}
      \checkprogram{p}{\Pi} \quad \envok{\Gamma} \quad \mathit{r: \Gamma} \quad \checkblock{\Pi}{\Gamma}{\iota} \quad \smallstep{r}{\iota}{p}{r'}{\iota'}\\
      \hline
      \exists \mathit{\Gamma'}.\ \envok{\Gamma'}\ \wedge\ \mathit{r': \Gamma'}\ \wedge\ \checkblock{\Pi}{\Gamma'}{\iota'}
    \end{array}
  \end{align*}
  \item[Soundness:] a well-typed program, after an arbitrary amount of steps, either halts or can make another step.
  \begin{align*}
    \begin{array}{c}
      \checkprogram{p}{\Pi} \quad \mathit{\Gamma = \Pi(l_0)} \quad \mathit{\iota = p(l_0)} \quad \kleenestep{r}{\iota}{p}{r'}{\iota'}\\
      \hline
      \mbox{step-or-halt}(p,\ r',\ \iota')
    \end{array}
  \end{align*}
\end{description}

Together with the top level theorems, there is a set of lemmas which verify intermediate results, mostly taken from the Twelf implementation. Such lemmas are not listed here but can be found in the \aProlog\ checks files. The additional lemmas were implemented in this thesis to investigate an important aspect of PBT applied to mechanized metatheory: whether the checks based on top level theorems are sufficient to find even the most subtle bugs, or if checking specific subparts of the implementation is more effective.

In addition to PBT, the PLT-Redex implementation of the list-machine benchmark contains a set of unit tests. We have adopted them to better understand the capability of PBT checks in finding errors by comparing their results with those achieved by unit tests. Some of those tests are parametric, which is a borderline case between unit testing and PBT: parametric unit tests are usually encoded as functions accepting input arguments, which are taken from a finite enumeration of test cases, paired with the expected results, provided by the user.

\section{Mutations}

As described in \citep{smallscopehypothesis}, \emph{``mutation testing is a criterion for assessing the quality of a set of test inputs''}. This consists of two distinct phases:
\begin{enumerate}
  \item initially a set of \emph{mutants} is generated from the original program by applying one or more syntactic modifications, which are categorized by \emph{mutation operators} and are responsible of causing a mutation of a certain kind to the original code;
  \item in the successive step, each mutant is executed with some input and then its output is compared to the one yielded by the original code, and if such outputs are different, then the test input is said to \emph{kill} the mutant.
\end{enumerate}

When mutation testing is realized in combination with unit testing, a unit test is said to kill a mutant if the output is not equal to the one expected by the test. In the PBT context, mutants are considered killed when a property is falsified and a counterexample is produced.

We use mutation testing, as suggested by existing literature on the topic \citep{smallscopehypothesis}, to probe the effective quality of PBT. Both property checks and unit tests were run over a set of mutations of the list-machine source code. Mutations were made "by hand" instead of using an automatic tool because there is no such library for \aProlog: it could be ported from existing Prolog mutation testing implementations such as MutProlog \citep{mutationtestingprolog}, however it was left as a possible future improvement to this work. Anyway, even the PLT-Redex benchmark contains just three mutations, which we have ported to the \aProlog\ and F\# implementation we made. Other mutations were then added, up to twenty different cases. Some mutations were actual bugs introduced unintentionally during the \aProlog\ development (mutations 4 and 5 listed in the table presented at the end of the chapter).

Mutants were generated by following the mutation kinds proposed by \citep[Chapter~16]{softwaretesting}.

\begin{description}
  \item[Value mutations:] involve changing the values of constants or parameters, by incrementing or decrementing the value of a constant, or by swapping the order of parameters.
  \item[Decision mutations:] modify conditions to reflect possible errors in program conditions encoding, for example encoding typos in operators such \texttt{<} and \texttt{<=}, or confusion of logic and arithmetic operators such \texttt{||} for \texttt{\&\&} and \texttt{<} for \texttt{>}.
  \item[Statement mutations:] permute, duplicate or delete certain lines of code, or change operators in expressions.
\end{description}

However these rules are just a wide categorization of mutation operators. More specific mutation operators were defined by following the rules given in \citep{mutationtestingprolog}, which are summarized below.

\begin{description}
  \item[Clause mutations:] predicate deletion, predicate swap, replacement of conjunction by disjunction, or disjunction by conjunction.\footnote{The authors of \citep{mutationtestingprolog} include in this category also cut insertion, removal and permutation. However in the \aProlog\ list-machine implementation we did not make use of cuts.}
  \item[Operator mutations:] arithmetic operator mutation, relational operator mutation.
  \item[Variable mutations:] replacing variable by variable, variable by anonymous variable, anonymous variable by variable.
  \item[Constant mutations:] replacing constant by constant, constant by anonymous variable, anonymous variable by constant, constant by variable, variable by constant.
\end{description}

Table \ref{tbl:list_machine_mutations} contains all of the 20 mutations applied to the original \aProlog\ code, where the first three were taken from the PLT-Redex benchmark and mutations 4 and 5 were actual errors introduced in the \aProlog\ implementation during its development. Some of them are not given a mutation kind from the one listed above, but in such cases they are just a more specific kind where \aProlog\ additional features are involved.

\begin{table}[ht!]
\[
  \begin{tabular}{| l | p{7cm} | p{6cm} |}
    \hline
    ID & Mutation description & Mutation kind\\
    \hline
    \hline
    1 & Confuses the lhs value for the rhs value in cons type rule & Variable: variable by variable\\
    \hline
    2 & var-set may skip a var with matching id (in reduction) & Clause: delete predicate\\
    \hline
    3 & step on cons does not actually update the store & Variable: variable by variable\\
    \hline
    4 & Typo in step with fetch-field on second field (A1 instead of A) & Variable: variable by variable\\
    \hline
    5 & Typo in block-typing-lookup (N' instead of L') & Variable: variable by variable\\
    \hline
    6 & step wrongly reorders the trailing instructions of a sequence & Variable: variable swap\\
    \hline
    7 & Missing step clause for fetch on second field instruction & Clause: delete clause\\
    \hline
    8 & Store is never updated after every step in big-step semantics & Variable: variable by variable\\
    \hline
    9 & Missing halt clause in step-or-halt predicate & Clause: delete clause\\
    \hline
    10 & Missing recursion in env-ok predicate & Clause: delete predicate\\
    \hline
    11 & Wrong variable used for type lookup in env-lookup & Variable: variable by variable\\
    \hline
    12 & Not updating the rest of the environment in env-set & Variable: variable by variable\\
    \hline
    13 & Missing clause for listcons variance in subtyping & Clause: delete clause\\
    \hline
    14 & Missing clause for identity in lub predicate & Clause: delete clause\\
    \hline
    15 & Inverted ty constructor predicates in the third value-has-ty predicate clause & Constant: constant by constant\\
    \hline
    16 & Copy-paste error in check-instr, missing list type to the fetched second field & Constant: constructor by variable\\
    \hline
    17 & Checking again the first instruction of a sequence instead of the second & Variable: variable by variable\\
    \hline
    18 & Actually not checking blocks & Clause: delete predicate\\
    \hline
    19 & Initial environment does not force v0 to be the only initial variable in check-program & Constant: constant by anonymous variable\\
    \hline
    20 & Not setting the initial block typing in check-program & Clause: delete predicate\\
    \hline
  \end{tabular}
\]
\caption{list-machine mutations}
\label{tbl:list_machine_mutations}
\end{table}

Only 11 of the mutants for \aProlog\ made sense for a functional implementation: in fact, encoding stores and environments as maps makes some mutation inapplicable to the F\# code.
