(TeX-add-style-hook
 "example"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("eptcs" "submission" "copyright" "creativecommons")))
   (TeX-run-style-hooks
    "latex2e"
    "eptcs"
    "eptcs10"
    "breakurl"
    "underscore")
   (TeX-add-symbols
    "event"
    "titlerunning"
    "authorrunning")
   (LaTeX-add-bibliographies
    "generic"))
 :latex)

