\chapter{Property-Based Testing}
\label{pbt}

The practice of \emph{property-based testing} originated with the testing library QuickCheck \citep{quickcheck}, developed for the Haskell functional language, was designed to formulate and test properties of programs. Instead of unit testing program functions with isolated data points, the expected properties are defined as functions themselves. The task of the testing library is to generate random data based on the input types of a property, repeating the process until the property fails, or a fixed amount of iterations has completed successfully, or a given timeout is reached.

Properties are not just assertions on input data. When the developer needs to narrow the values of a type given as input, she can define preconditions that the generated values must satisfy. The task of a property checker is to find a value such that, for a given property, the preconditions are satisfied and the postcondition is not. When preconditions are not satisfied, the generated values are discarded and a new test iteration is performed.

In the QuickCheck library, and in all its portings, there is a set of functions and operators that together form a simple DSL for defining properties. The developer can specify preconditions and postconditions in a clear syntax, and use other operators for several different aspects of testing, such as classification of generated values and their labeling that can help in collecting statistics about their distribution.

Other than generating and finding counterexamples to properties that are supposed to hold for every value, the test library is designed to possibly find the smallest counterexample that invalidates a property. This feature, called \emph{shrinking}, allows the developer to recognize the involved bug more easily, by avoiding useless data ramifications that may hide the bug evidence.

Several portings of the QuickCheck library were made to different functional languages, such as ScalaCheck\footnote{\url{https://www.scalacheck.org/}} for Scala, Quviq QuickCheck \citep{quickcheckfunprofit} for Erlang, and FsCheck for F\#, which is also usable from C\# projects. Other libraries were developed keeping the same PBT approach but implementing a different way of generating data, such as exhaustive generators as in the SmallCheck library \citep{smallcheck} or constrained generators in \aCheck\ \citep{alphacheck}, which will be further discussed in this chapter.

\section{Random Generation}
\label{random_generation}

The first approach adopted in PBT was the random generation of data that is fed as input to properties. Libraries as QuickCheck and its derived portings offer a set of operators which work over a monadic abstraction of data generators. Developers can use them to implement generators for user-defined datatypes, or provide alternatives to the library generators for common datatypes.

Random generation of values is usually bound to data size, represented as a natural number which is passed to the generator when generating a sample value. The test library begins checking a property starting with small values, then growing the size parameter during the following iterations. The size parameter is fed to the random generator which should translate it to a value that is bigger when the size grows. However there is not a canonical way to interpret the size parameter and how values should grow. From now on, references to the \emph{size} of a generated value are related to the size parameter, therefore every mention to \emph{bigger} and \emph{smaller} values refers to values generated with a size parameter greater or less than the one used to generate the other value of the comparison.

Defining properties alone may be enough for functions that operate on trivial input types. When property preconditions are too restrictive, randomly generated values may be mostly discarded. In this case, test coverage and effectiveness is affected by the small amount of generated valid inputs that are used to check a property, leading to few run iterations or excessively long execution time of tests. When more structured datatypes and properties with restrictive preconditions are involved, random generation of data needs to be guided. By using the library operators, the developer can design generators which produce data that satisfy specific property preconditions with a higher likelihood.

As mentioned before, the FsCheck library, which was used in this thesis project, is a QuickCheck porting to F\#. It extends QuickCheck features by automatically providing naive generators for all user-defined datatypes, exploiting the reflection capabilities of the .NET platform.

PBT certainly introduces advantages in finding software errors compared to unit testing. However developing data generators may be a difficult task. Not only generators code may contain errors itself and has to be maintained when evolving the model under test, they also have to be \emph{correct}. Formally speaking, as explained in \citep{foundationalpbt}, a generator must be possibly at least \emph{sound} (i.e.\ it generates only values of the desired shape) and \emph{complete} (i.e.\ it can generate all possible values of a desired shape with non-zero probability). Together, these properties define the correctness of a generator.

\subsection{Shrinking}
\label{shrinking}

When a randomly generated counterexample to a property is found, the reached size could have grown during previous test iterations and the data may contain redundant or excessive information that makes more difficult interpreting the counterexample. FsCheck, as QuickCheck, implements another step after generation which is \emph{shrinking}. Basically, shrinking is the process of finding a local minimum of counterexamples for the tested property. After a counterexample is found, the testing library repeatedly calls a \emph{shrinker} for each property argument type. The shrinker purpose is to generate a sequence of values smaller than the given one. Then, the shrunk elements are fed one by one to the property until one of them is recognized as a counterexample or until the list is consumed. If no new counterexample is found, the shrinking process ends, while if one of the shrunk values falsifies the tested property, shrinking is repeated on that value.

Deriving smaller values from a counterexample is not an automatic process: shrinking strategies are delegated to the developer, which has to implement shrinkers for user-defined types. While for small counterexamples with simple datatypes shrinking can be ignored, it may become necessary in later stages of development when more complex counterexamples can emerge from tests.

\section{Exhaustive Generation}

For certain kinds of programs and test properties, counterexamples are small values which contains the necessary data to trigger a bug and make a property check fail. Therefore generating large values and then shrinking the found counterexamples could be an inefficient testing strategy. Moreover, as stated by the so called \emph{small scope hypothesis}, an high proportion of bugs could be found by testing properties about a model with values within some small scope. While the work presented in \citep{smallscopehypothesis} shows promising results applied to testing Java programs, the same principles can be applied to different programming paradigms and languages. For example, the SmallCheck Haskell library \citep{smallcheck} is an alternative to QuickCheck that employs a bounded exhaustive generation strategy instead of a random generation one.

The \emph{exhaustive generation} of values is an approach to data generation that enumerates all possible values of property input types within a given size starting from the smallest ones, proceeding with a complete search strategy, such as iterative deepening, by increasing the maximum data size after all values within the previous one are generated. Exhaustive generation is not completely alternative to random generation: different search strategies can be applied, or custom generators can be provided to guide generation within a subclass of values.

Exhaustive generation guarantees that a counterexample is a global minimum, instead of just a local one like in random generation. However, the amount of values grows exponentially with the generation size and the minimum counterexample may not be reached within a reasonable time.

The most valuable advantage of exhaustive generation, which will be further argued in the following chapters, is that data generation does not have to be necessarily guided with custom-made generators. This advantage is more evident when the program under test evolves and test properties may change: random data generators and shrinkers have to be updated as well to reflect changes in the code, a task that may be time consuming. With exhaustive generation, tests are ready to be run just after the property functions are defined.

\section{Constrained Generation}

In QuickCheck, and generally in most PBT libraries, checks execution is organized with a \emph{generate and test} approach: data generation is a distinct phase that happens before checking a property with those values. In logic programming languages, data generation can be woven with property checking, in a \emph{generate by need} fashion. The strategy adopted by \aCheck\ is the grounding of non-ground variables appearing in a property, by automatically inserting a generator for each one of them, which enumerates all possible values within a maximum backtracking depth.

Several PBT libraries adopted this generation strategy to enhance checks execution, such as QuickChick \citep{foundationalpbt}. In \cite[Paper~III]{duregard}, the author proposes a technique for deriving test data generators from executable predicates as an extension to the QuickCheck library, relying on laziness of the involved predicate to efficiently prune the space of values. This improvement was guided by the idea that handwritten generators are unreliable and labor intensive, usually with an unpredictable distribution of values. In \citep{pltredexconstraintlogic}, the authors present a generic method for randomly generating well-typed expressions, as a tool for the PLT-Redex framework. In \citep{quickcheckmorebugs}, the authors explain a new kind of random data generation strategy which takes in account the bugs already discovered, thus guiding the distribution of generated data in search for new bugs.

\subsection{\aProlog\ and \aCheck}
\label{aprolog}

With reference to programming languages metatheory, \aCheck\ is a bounded model checker for metatheoretic properties of formal systems. Described in \citep{alphacheck}, it applies PBT to the problem of mechanically proving properties of object languages, especially with variable binding, in an automatic fashion and without requiring user expertise in theorem proving.

The \aCheck\ tool operates on formal systems specified using nominal logic written in \aProlog \citep{nominallogic}, a language derived from Prolog with the addition of nominal logic primitives and predicate types. Nominal logic involves $\alpha$-equivalence and name substitution, which is a common trait present in programming languages: formalizing variable binding, for example in the introduction of lambda terms, is made straightforward by the primitives provided by \aProlog. However, this particular feature has not been used in the work of this thesis.

Another advantage is given by the relational style of logic programming, which is very similar to the inference rules structure used in model specifications. Translating such rules to \aProlog\ predicates can be a simple task, and showing that the translation between inference rules and \aProlog\ code is adequate quite as simple.

Unlike Prolog, \aProlog\ is typed: all symbols must be declared explicitly, making model encoding less prone to a wide class of subtle error that can occur when programming in vanilla Prolog. Moreover, \aProlog\ introduces function types: instead of defining a predicate, such as \texttt{pred x(a, b, c)}, it can be translated to a function \texttt{func x(a, b) = c}. Function definitions can be used to explicitly separate input and output variables of a predicate and in general to make code more readable in certain cases.

As mentioned before, \aCheck\ is a bounded model checker which adopts iterative deepening as search strategy. A check is defined by the \texttt{\#check} keyword, a string representing its name and the maximum depth the generators of the bounded model checker shall reach. After those parameters, the property follows: the list of preconditions and the postcondition are separated by an arrow. In these checks not all variables will be ground. For all variables appearing in the check goal, that is the disjunction of all predicates of the precondition \texttt{G} and the negated postcondition \texttt{A}, a generator $\mbox{gen}_{\vec{\tau}}$ is added to the goal as an additional predicate.

Generators, as explained in \citep{alphacheck}, are placed just before the postcondition term \texttt{A}: first all derivation of the hypothesis are generated up to a given depth, then all variables instantiations are sufficiently grounded up to the depth bound, and finally \aCheck\ tests whether the conclusion finitely fails for the current substitution. Variables grounding is required by the \emph{negation-as-failure} check resolution strategy. By inspecting type information and predicate clauses, generation is constrained to values that satisfy the property preconditions. The maximum depth of generated values that is specified after the check name is applied to all involved generators and it is not cumulative.

A check declaration
\begin{align*}
  \texttt{\#check "name" depth: G => A.}
\end{align*}
is translated to the formula
\begin{align*}
  \exists \vec{X}: \vec{\tau}.\ G \wedge\ \mbox{gen}_{\vec{\tau}}(\vec{X}) \wedge\ \neg A
\end{align*}
where $\vec{X}$ is a tuple of variables occurring in the goal $G$ and in the atomic formula $A$. A finite counterexample is a closed substitution $\theta$ providing values for $\vec{X}$ such that $\theta(G)$ is derivable and the conclusion $\theta(A)$ finitely fails. The counterexample size is bound to the depth parameter.

The depth bound is increased after each execution of the check goal, in an iterative deepening fashion, up to the depth limit specified in the check declaration.

\aCheck\ properties do not require any additional coding effort other that writing their statements. Test data is exhaustively generated by generators that are automatically woven into the check. Custom generators can be provided in place of them, by adding to the preconditions list a predicate that grounds the chosen variable and unifies it with the generated values.
