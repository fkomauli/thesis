\chapter{Introduction}

In recent years we have witnessed an increasing attention toward \emph{machine-checked metatheory}. The authors of the PoplMark Challenge \citep{poplmark}, after reviewing automated theorem proving technology applied to different problems concerning \emph{programming languages theory} (PLT for short), concluded that such technology has reached the point where it can begin to be widely used by language researchers. The challenge proposed a set of benchmarks designed to evaluate the state of automated reasoning about the metatheory of programming languages. The final purpose pursued by the authors is \emph{``making the use of proof tools common practice in programming language research''}.

This thesis wants to be a contribution in a complementary approach to formal verification of programming languages metatheory: the use of \emph{property-based testing} \citep{quickcheck}, as a form of \emph{model-checking}, prior to theorem proving to find errors earlier in the verification process. Moreover, the attention is directed toward typed assembly languages and abstract machines \citep[Chapter~4]{attapl} instead of addressing high-level languages.

This chapter summarizes the state of the art of \emph{mechanized metatheory model-checking} and its applications to abstract machines, gives a brief overview of the list-machine benchmark \citep{listmachine}, and explains the reasons of this thesis goals.

\section{Verification versus Validation in PLT}

After the proposal of the PoplMark Challenge \citep{poplmark}, the problem of \emph{mechanized metatheory}, that is formally verifying properties about programming languages using computational tools, gained more focus. Formal machine-checkable proof is the most trustable evidence of correctness of a system. However, existing proof assistant tools have steep learning curves and, moreover, verifying the properties of a nontrivial system through interactive theorem-proving with such proof assistants requires a significant effort. Proof attempts are not the best way to debug an incorrect system, because of the little assistance given by proof assistants in such cases.

The mentioned drawbacks of formal verification with proof assistant systems lead to alternative verification techniques, such as \emph{model-checking} \citep{modelchecking}. In model-checking, the user efforts are limited to the specification of a system and to the description of properties it should satisfy, while searching for counterexamples or determining their non-existence are tasks delegated to an automatic tool. Model-checking evolved from tools ideated only for small finite systems to ones involving advanced techniques for searching the state space efficiently, which are currently adopted by industry, for example to verify hardware designs.

The authors of \aProlog\ and \aCheck\ \citep{alphacheck} advocate \emph{mechanized metatheory model-checking} for analyzing programming languages and related systems as a complement to theorem proving techniques. However, when facing programming languages, systems usually have an infinite state space: verification is not applicable in such cases, but considering a bounded subset of the search space, the search for counterexamples can be automated as well. Bounded model-checking can provide a certain confidence that the design of a system is correct, though not the full confidence that formal verification would give. One of the most appealing outcomes of bounded model-checking is that it provides explicit counterexamples, thus making it more likely to be helpful than verification during the development of a system.

\section{Previous Work}
\label{previous_work}

The development of \aCheck\ was inspired, as forementioned, by mechanized model-checking. However it is not the only system that employs \emph{property-based testing} (from now on referenced as PBT) as a mean of validating programming languages properties.

One of the pioneers in PLT model-checking is PLT-Redex \citep{pltredex0}, a framework providing a DSL for mechanizing semantic models embedded in the Racket programming language. It allows an user to formalize an object language by specifying its syntax and semantics. It supports PBT with random input generation in a way similar to the QuickCheck Haskell library \citep{quickcheck}. PLT-Redex \emph{lightweight mechanization} approach has been showcased in detail with several case studies \citep{pltredex}. However, one of the most drawbacks of PLT-Redex is its lack of support for binders. The tool provides naive random generators for test data, which are guided by grammar definitions. They do not work well when dealing with typed languages and typechecking relational definitions: to alleviate this, the authors introduced a form of constraint logic programming approach \citep{pltredexconstraintlogic}. Well-typed terms are rather sparse and a random generation of terms tends to be wasteful, therefore they construct partial type derivations from the available typing rules that can be applied.

PLT-Redex has been successfully used in several case studies. One worth mentioning is the application of randomized testing to the model of the Racket virtual machine and bytecode verifier \citep{racketvmtesting}. The correctness of the bytecode verification algorithm and machine model was assessed by random testing certain properties, for example the claim that the machine does not get stuck on bytecode accepted by the verifier, and other metatheoretic propositions.

The success of PBT lead several proof assistant systems to adopt randomized testing solutions inspired by QuickCheck: the purpose of applying PBT together with theorem proving is finding errors earlier in the verification process and decreasing the number of failed proofs attempts. One step forward in getting closer PBT and formal verification has been made for the Coq proof assistant: the authors ported the QuickCheck library, naming it QuickChick \citep{foundationalpbt}. Among other theorem proving systems that employed random testing we mention Agda \citep{agdapbt} and Isabelle/HOL \citep{isabelleholpbt}.

\section{PBT for Abstract Machines}

The PoplMark Challenge targets full-scale programming languages, including features such as \emph{binding}, because most languages require a treatment of $\alpha$-equivalence and substitution in their semantics, and \emph{complex inductions}, such as mutually recursive definitions, mutual and simultaneous induction.

On the contrary, this thesis is focused on PBT for abstract machines instead, linked to typed low-level assembly languages, while keeping the same main objective of studying tools and techniques in the scope of mechanized metatheory. The mentioned problems do not apply to low-level languages: generally, such languages are less structured, present a simpler typesystem, and lack the notion of binders which are lost after the compilation process. However, this does not imply that assessing metatheoretic properties about typed assembly languages may be a trivial task.

The metatheory of assembly languages has been already reviewed in \citep[Chapter~4]{attapl}. One approach consists in \emph{type-preserving compilation}: the proof of type-safety of some program is constructed in a high-level language and then a type-preserving compiler transforms the code through successively layers of lower-level languages, down to the target code. Every code transformation goes along with a transformation of the proof. The resulting proof for the low-level language is easier to achieve in this way than to prove type-safety directly on the generated code. The other approach mentioned in \citep[Chapter~4]{attapl} is designing a \emph{typed intermediate language} whose type system makes it possible to encode high-level language features, such as control-flow safety and memory safety into its type system: a well-typed machine state cannot get immediately stuck (progress), and when the machine steps the new state is also well-typed (preservation).

\section{The List-Machine Benchmark}

This thesis is based on a benchmark ideated to compare theorem-proving systems on their ability to express proofs of compiler correctness. The authors of the list-machine model \citep{listmachine}, in contrast to the first PoplMark Challenge, emphasize the connection of proofs to compiler implementations, pointing out that much can be done without binders or $\alpha$-conversion. Following them in the typed assembly languages realm, this thesis faces the problem of applying PBT to such systems.

The list-machine assembly language consists of a small instruction set, such as instructions for fetching values from memory and storing them back, and for conditional and unconditional branches; it includes a simple typesystem that describes list types, together with a subtyping relation. It goes along with two implementations in two different proof assistant systems, Twelf and Coq.

The benchmark was further extended by adding new features, such as indirect jumps. However, this thesis took as reference the original version. The introduction of extensions to the list-machine may be used as a new case study, to better understand how the tools used in this thesis to implement the benchmark deal with the evolution of a model under test.

\section{Project Details}
\label{project_details}

Two different frameworks have been used to model check the abstract machine proposed by the benchmark. A relational implementation has been done in the logic programming language \aProlog\ \citep{nominallogic}. An additional functional implementation has been developed in F\#\footnote{\url{http://fsharp.org/}}, a strict functional language. PBT was used with two different flavors: random generation with the FsCheck\footnote{\url{https://fscheck.github.io/FsCheck/}} library, and bounded exhaustive generation with \aCheck\ \citep{alphacheck}.

This thesis project can be found on BitBucket at
\begin{align*}
  \mbox{\url{https://bitbucket.org/fkomauli/list-machine}}
\end{align*}
The machine used to execute the project scripts is equipped with an Intel$^{\tiny{\textregistered}}$ Core\texttrademark\ i5-4200U CPU with a clock speed of 1.60GHz and 8GB of RAM. The operative system is a 64 bit Ubuntu 17.10 Artful, with Linux kernel version 4.13.0.

\section{Thesis Outline}

Chapter \ref{pbt} briefly illustrates PBT, giving an overview of various implementations of this approach and the involved data generation strategies, such as random or exhaustive generation. Then the \aCheck\ tool is presented, with an introduction on its functionalities and how it performs bounded model checking for metatheoretic properties of formal systems.

Chapter \ref{case_study} presents the list-machine model, proposed as a benchmark for mechanized metatheory in \citep{listmachine}, with a description of its static and dynamic semantics. The process of deriving properties from statements of theorems and lemmas is explained. Finally, an introduction of mutation testing is given, followed by a classification of mutations for Prolog programs.

Chapter \ref{aprolog_implementation} addresses the list-machine implementation in \aProlog\, the definition of properties within \aCheck\ and the translation of unit tests into queries. In the end experimental results of checks execution on mutated versions of the model are discussed.

Chapter \ref{fs_implementation} similarly describes the F\# implementation of the list-machine model. The main subject is the design and implementation of random generators and shrinkers for FsCheck test samples. Then experimental results are presented, showing how checks behaved with the various mutants.

Chapter \ref{conclusions} compares the exhaustive generation approach within a logic programming language of Chapter \ref{aprolog_implementation} with the random one within a functional language treated in Chapter \ref{fs_implementation}. Advantages and disadvantages of both generation strategies and paradigms are explained: aspects such as fast prototyping, code maintenance, achievable results, execution times, and results clarity. Finally, a chronological report of a success story is presented.
