\aProlog~\cite{nominallogicb} is a logic programming language
particularly suited to encoding PL calculi and related systems due to
its support for nominal logic. However, since this case study is
purposely first order only, we did not get to use any of those goodies
and the encoding is many sorted pure Prolog code. In fact, it follows
quite closely the reference Twelf implementation by
Appel~\cite{listmachine}. The only place one would naturally try and
use \emph{name types}, and hence inherit $\alpha$-equivalence, is
encoding of variables and labels. Yet, the machine model calls for
\emph{distinguished} (initial) variable and label $\mathtt{v_0}$ and,
$\mathtt{l_0}$ and this suggests an encoding based on an enumeration
of constants --- remember, we are in the business of bounded model
checking, so we tend to avoid using infinite types as integers. The
only downside is the need for an explicit inequality predicate. We use
association lists for all the environments the list-machine uses (note
the Haskell like syntax for type abbreviations) and standard Prolog
predicates for related operations such as looking up or updating
(non-destructively) such a list. Do not be fooled by the functional
notation, it is flattened at compile time to the equivalent
relation. The complete code can be found at
\url{https://bitbucket.org/fkomauli/list-machine}.

\begin{small}
\begin{lstlisting}
var : type.
v0 : var.
v1 : var.
...
pred not_same_var (var, var).
not_same_var(v0, v1).
not_same_var(v0, v2).
...
type block =   (label,instr).
type program = [(block)].
type store =   [(var,value)].
...
func var_lookup (store,var) = value.
var_lookup([(V,A)|R],V) = A.
var_lookup([(V,A)|R],V1) = A1 :-
  not_same_var(V,V1), A1 = var_lookup(R,V1).
\end{lstlisting}
\end{small}
The small-step semantics is an unsurprisingly translation of the
rules in Fig.~\ref{fig:listmachinesmallstepsemantics} into a predicate
{\texttt{step (program, store, instr, store, instr)}}, where the first
three arguments are inputs. Type checking is slightly more
interesting: we summarize the main type definitions:%  together with
% their (informal) mode declaration, which will become relevant as soon
% as we try to model-check existential properties such as
% \emph{preservation}.
%mode check-instr +Pi +G +I -G.
%mode check-block +Pi +G +I.
%mode check-blocks +Pi +P.
%mode check-program +P +Pi.

\begin{small}
\begin{lstlisting}
pred check_instr   (program_typing,env,instr,env).
pred check_block   (program_typing,env,instr).
pred check_blocks  (program_typing,program).
pred check_program (program,program_typing).
\end{lstlisting}
\end{small}
Note that \texttt{check\_instr(Pi,G,I,G')} will instantiate
\texttt{G'} during execution. All other checks expect fully grounded
inputs, i.e.\ no type inference is possible.

\newcommand{\andd}{\land}
\newcommand{\impp}{\rightarrow}
\subsection{PBT with \aCheck}
\label{ssec:acheck}
$\alpha$Check~\cite{alphacheck} is a tool for checking desired
properties of formal systems implemented in \aProlog. The idea is to
test properties/specifications of the form
$H_1 \andd \cdots \andd H_n \impp A$ by searching \emph{exhaustively}
(up to a bound) for a substitution $\theta$ such that
$\theta(H_1),\ldots,\theta(H_n)$ all hold but the conclusion
$\theta(A)$ does not. In this paper, we identify negation with
\emph{negation-as-failure}, but the tool includes also another
strategy~\cite{Pessina15}. The concrete syntax for a check is
\begin{center}
  \texttt{\#check "name" depth: G => A.}
\end{center}
where \texttt{G} is a goal and \texttt{A} an atom or
constraint\footnote{Since in this paper we make no use of  nominal
  features, this means syntactic equality.}.  As usual in Prolog the
free variables are implicitly universally quantified and \texttt{depth}
is the user-given bound. The above pragma is translated to the formula
\begin{equation}
  \label{query}
  \exists \vec{X}: \vec{\tau}.\ G \wedge\ \mbox{gen}_{\vec{\tau}}(\vec{X}) \wedge\ \neg A
\end{equation}
where $\mbox{gen}_{\vec{\tau}}$ are type directed {exhaustive}
generators automatically compiled by the tool to ground $\vec{X}$, the
intersection of the free variables of $G$ and $A$, needed to make the
use of NF sound. The user can, alternatively, specify her own
generators as \aProlog code, if she feels she needs to implement a
smarter generation strategy, as we will see shortly and in
Section~\ref{ssec:tniq}.

A query such as (\ref{query}) amounts to a \emph{derivation-first}
approach, which generates all ``finished'' derivations of the
hypothesis $G$ up to a given depth, considers all sufficiently ground
instantiations of variables, and finally tests whether the conclusion
finitely fails for the resulting substitution.
%
\aCheck implements a simple-minded iterative deepening search strategy over a
hard-wired notion of bound, which roughly coincides with the number
of clauses that can used in the derivation of each of the premises

Let's look at a check to be more concrete, \emph{progress}. While
\aProlog does not have first class existentials and disjunction, it is
a basic Prolog exercise to code it:
\begin{small}
\begin{lstlisting}
pred step_or_halt (program,store,instr).
step_or_halt(P,R,instr_halt).
step_or_halt(P,R,I) :- step(P,R,I,R',I').

#check "progress" 10: check_program(P,Pi), check_block(Pi,G,I), store_has_type(R,G)
                         => step_or_halt(P,R,I).
\end{lstlisting}
\end{small}

\begin{sloppypar}
Keeping in mind that the tool add generators for \texttt{P,R,I} before
trying to refute \verb|step_or_halt(P, R, I)|, informal mode analysis
for \texttt{step} tells us that \texttt{R',I'} will be ground when the
partial proof tree for the check is built.
\end{sloppypar}

%
The same approach will not work for \emph{preservation}:
\begin{small}
\begin{lstlisting}
pred exists_env (program_typing,store,instr).
exists_env(Pi,R,I) :-
  store_has_type(R,G), env_ok(G), check_block(Pi,G,I).

#check "pres" 20 : check_program(P,Pi), step(P,R,I,R',I'), env_ok(G),
                   store_has_type(R,G),check_block(Pi,G,I) 
                       => exists_env(Pi,R',I').
\end{lstlisting}
\end{small}
This because \verb|store_has_type(R,G)| expects \texttt{G} to be
ground and we are in effect trying to use \aProlog for an impossible
type \emph{inference} task. The solution is to write a specialized
generator \verb| build_env| for type environments (and recursively, for
types and variables). The peculiarity is that we need to add a
\emph{local} depth bound, so that smallish environments can be built
independently from the hard-wired bound, which is additively
distributed along all the atoms in the check.
\begin{small}
\begin{lstlisting}
pred exists_env_b (int,program_typing,store,instr).
exists_env_b(N,Pi,R,I) :-
  N > 0, exists_env_b(N - 1,Pi,R,I).
exists_env_b(N,Pi,R,I) :-
  N >= 0, build_env(N,G), store_has_type(R,G), env_ok(G), check_block(Pi,G,I).

#check "pres_b" 20 : ... => exists_env_b(4,Pi,R',I').
\end{lstlisting}
\end{small}
Please see Section~\ref{sec:exper} for empirical results. Even
discounting our obvious bias, specifying and validating properties
(``spec'n'check'') in \aCheck is dead simple, requires very little effort
and more than often turns out to be pretty useful.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pbt"
%%% End:

%  LocalWords:  Twelf Appel Haskell PBT pragma env spec'n'check
