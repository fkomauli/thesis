(TeX-add-style-hook
 "pbtam"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("eptcs" "submission" "copyright" "creativecommons")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "eptcs"
    "eptcs10"
    "breakurl"
    "underscore"
    "url"
    "xspace"
    "amssymb"
    "amsmath"
    "amsthm"
    "listings"
    "proof"
    "graphicx"
    "cancel"
    "pgfplots")
   (TeX-add-symbols
    '("bibinfo" 2)
    '("doi" 1)
    '("urlalt" 2)
    '("href" 2)
    '("url" 1)
    '("bibitemdeclare" 2)
    '("envok" 1)
    '("checkprogram" 2)
    '("checkblocks" 2)
    '("checkblock" 3)
    '("checkinstr" 4)
    '("kleenestep" 5)
    '("smallstep" 5)
    "event"
    "aProlog"
    "aCheck"
    "andd"
    "impp"
    "surnamestart"
    "surnameend"
    "urlprefix"
    "titlerunning"
    "authorrunning")
   (LaTeX-add-labels
    "sec:lm"
    "ssec:synt"
    "ssec:sem"
    "fig:listmachinesmallstepsemantics"
    "fig:ibtype"
    "ssec:checks"
    "ssec:mut-test"
    "fig:thirdmutation"
    "fig:sixteenthmutation"
    "aprolog"
    "ssec:acheck"
    "query"
    "fs"
    "sec:exper"
    "ssec:value-has"
    "ssec:exp"
    "fig:comparisonchart"
    "fig:theoremschart"
    "sec:ongoing"
    "ssec:lm2.0"
    "ssec:tniq"
    "sec:end")
   (LaTeX-add-bibitems
    "ifc"
    "listmachine"
    "BaeldeGMNT07"
    "BlanchetteBN11"
    "Bulwahn12"
    "CERVESATO2002"
    "alphacheck"
    "Pessina15"
    "nominallogicb"
    "ChihaniMR13"
    "quickcheck"
    "Feat"
    "FachiniM17"
    "FetscherCPHF15"
    "redexlm"
    "JiaH11"
    "Klein12"
    "PBTAM"
    "LampropoulosPP18"
    "MomiglianoP03"
    "foundationalpbt"
    "softwaretesting"
    "attapl"
    "mutationtestingprolog"
    "RobersonHDB08"
    "Clerks"
    "SmallCheck"
    "TOR"
    "csmith"))
 :latex)

