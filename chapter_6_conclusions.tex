\chapter{Conclusions}
\label{conclusions}

Chapter \ref{aprolog_implementation} described how PBT with bounded exhaustive generation was applied to the list-machine model. Similarly, Chapter \ref{fs_implementation} explained in detail how PBT was implemented for a F\# encoding of the abstract machine. Separate conclusions were drawn for each implementation, while in this chapter both approaches are compared together: first a summary of advantages and disadvantages, that were experienced in \aProlog\ and F\# implementations, is explained in detail; finally, a chronological report of a success story is presented.

\section{Functional-Random VS Logic-Exhaustive PBT}

After the separate evaluation of \aCheck\ and FsCheck experimental results, some conclusion can be drawn by comparing them and by analyzing the work done for each implementation. The F\# list-machine development took more time than the \aProlog\ one, mostly because of the generators and shrinkers design and implementation, the more promising results were achieved by the \aProlog\ list-machine version.

\subsection{Fast prototyping}

The list-machine implementation in F\# was a translation, with some adjustments, from the Coq code referenced in \citep{listmachine} which contains an algorithmic version of typechecking. In a real world case, an implementation would have as only reference the inference rules written on paper. Such rules can be encoded straightforwardly in a relational style, by using a logic programming language like \aProlog. In fact, one of the most valuable characteristics of the relational style of encoding is the similarity with the inference rules given as formulas, and more importantly, it allows us to show that the translation is adequate in a seamless way.

Writing properties in a logic language and in a functional one does not show much differences. PBT libraries for functional languages provide a DSL for writing properties with a clear and concise syntax. However, dealing with existential quantification may present some difficulties in both paradigms.

While properties can be immediately checked with exhaustive generators, when using random generation non trivial properties need a custom generator to be adequately tested. As widely explained in Chapter \ref{fs_implementation}, writing custom generators could be a time expensive task, even requiring more efforts than implementing the abstract machine model itself.

\subsection{Code maintenance}

An abstract machine evolves with time: the typesystem may be extended to include security features or to gain more expressiveness, language construct may be added or modified to adapt the programming language to new idioms. Features may be added in an iterative way to first verify that they are compatible with the already implemented ones. For example, the list-machine model has been extended in \citep{listmachine} with the addition of indirect jumps. When the code that defines a model changes, test code has to be aligned with its modifications.

PBT with exhaustive generators benefits from the possible absence of custom defined generators, while custom random generators are often strictly required when checking more complex properties. Several lines of code have to be adapted when the model under test changes. Moreover, while more and more features are added to the abstract machine, the complexity of random generators grows making their implementation more prone to errors.

Custom generators are not the only additional code an user has to implement. Shrinkers are necessary to build smaller counterexamples, containing just the necessary features that trigger an error and falsify a property. When the model under test evolves, even shrinkers must be adapted to the introduced changes.

\subsection{Head to Head Comparison}

Properties representing lemmas in \citep{listmachine} and auxiliary tests did not perform well in the FsCheck implementation. The comparison of \aCheck\ and FsCheck top-level properties results for each mutation ported to F\# reveals that checks written in \aProlog\ performed slightly better than the relative ones implemented in F\#. This conclusion can be viewed as the consequence of two factors:
\begin{itemize}
  \item most properties, if falsifiable, have small counterexamples as hypothesized by \citep{smallscopehypothesis} and thus the generation of large input values is not strictly necessary;
  \item custom random data generators may not be coherent with the model under test, for example when a generator contains an error, or the data distribution is not adequate and possible counterexamples are never supplied to a falsifiable property.
\end{itemize}

Table \ref{tbl:list_machine_test_execution_results} compares the results of killing mutants by top-level checks in both implementations. Checkmarks annotated with (**) indicate a check that runs for more than a minute to find a counterexample. As can be seen, \aCheck\ \emph{soundness} check almost doubles the kills made by its FsCheck counterpart.

\begin{table}[ht!]
\[
  \begin{tabular}{| l || c | c || c | c || c | c |}
    \hline
    \ & \multicolumn{2}{c|}{Progress} & \multicolumn{2}{c|}{Preservation} & \multicolumn{2}{c|}{Soundness}\\
    \hline
    Mutant & \aCheck & FsCheck & \aCheck & FsCheck & \aCheck & FsCheck\\
    \hline
    \hline
    1 & \checkmark ** & \ & \ & \checkmark & \checkmark & \ \\
    \hline
    3 & \ & \ & \checkmark & \checkmark & \checkmark & \checkmark\\
    \hline
    6 & \ & \ & \checkmark & \ & \checkmark & \ \\
    \hline
    7 & \checkmark ** & \ & \ & \ & \checkmark & \checkmark\\
    \hline
    8 & \ & \ & \ & \ & \checkmark & \checkmark\\
    \hline
    9 & \checkmark & \checkmark & \ & \ & \checkmark & \checkmark\\
    \hline
    13 & \ & \ & \ & \ & \ & \ \\
    \hline
    15 & \ & \ & \checkmark & \checkmark & \ & \ \\
    \hline
    16 & \ & \ & \checkmark & \checkmark & \ & \ \\
    \hline
    17 & \ & \ & \ & \ & \ & \ \\
    \hline
    18 & \ & \ & \checkmark & \ & \checkmark & \ \\
    \hline
  \end{tabular}
\]
\caption{comparison of top-level checks results in \aCheck\ and FsCheck}
\label{tbl:list_machine_test_execution_results}
\end{table}

\subsection{Execution time}

Here we do not want to compare how fast checks are executed by \aCheck\ and FsCheck: a comparison between two different paradigms based on execution times cannot give useful results, especially when the registered duration vary in ranges between milliseconds and hours. Moreover, \aProlog\ is a non optimized interpreter still in a prototyping phase, thus it cannot be placed aside a widespread industrial-level language like F\#. We concentrate instead on how certain causes influence the duration of checks validation.

The factor that mostly affects check execution time is the number of generated input samples that are supplied to a property. While with FsCheck a maximum test iterations count can be configured (this thesis project uses a limit of 10,000 iterations), the checks written with \aCheck\ can only specify a maximum depth for the exhaustive generation. Such parameter is not closely tied to execution time, because the search space of values may exponentially explode at any depth.

Table \ref{tbl:list_machine_fscheck_test_execution_times} shows that top-level checks implemented with FsCheck run within seconds. Executing all implemented checks takes about 5 minutes overall. The execution times of \aCheck\ checks listed in Table \ref{tbl:list_machine_acheck_top_level} vary from milliseconds to hours within the same depth bound, while testing different mutants. However, when the \emph{soundness} check kills a mutant, the counterexample is found mostly within 2 seconds. \aCheck\ checks are skippable with \texttt{Ctrl+C}: the user can manually interrupt any check, waiting a bunch of seconds before skipping to the next property. This process could be automated by adding a check execution time limit as an improvement to the \aCheck\ implementation, but it is not in the scope of this thesis.

\subsection{Results clarity}

Writing shrinkers that transform large counterexamples to ones small enough to be studied in detail in search of errors is not a trivial task. If not well planned, the chosen shrinking strategy may mostly produce some non optimal local minimum. On the other hand, counterexamples found while employing exhaustive generation with an iterative deepening search strategy are always a global minimum, thus they do not require a shrinker to be further reduced.

\section{Not Only Mutants}
\label{not_only_mutants}

One case worth mentioning was an inconsistency of a judgment stated in \citep{listmachine}. The involved predicate is \emph{value-has-ty}, which is needed by the \emph{preservation} theorem: in fact, the type-checker attributes types to variables, but never types to values. This predicate is referenced by the $\mathit{r: \Gamma}$ precondition in the \emph{preservation} hypothesis, where types associated to variables in an environment $\mathit{\Gamma}$ have to typecheck with values associated to the relative variable in a store $r$, as in \citep{listmachine}.

The inference rules of the \emph{value-has-ty} predicate are shown in figure \ref{fig:list_machine_value_has_ty}.

\begin{figure}[ht!]
\[
  \begin{array}{c}
    \begin{array}{c}
      \ \\
      \hline
      \mbox{nil}:\ \mbox{nil}
    \end{array}\\[.5cm]
    \begin{array}{c}
      \ \\
      \hline
      \mbox{nil}:\ \mbox{list}\ \tau
    \end{array}\\[.5cm]
    \begin{array}{c}
      \ \\
      \hline
      \mbox{cons}(a_0,\ a_1):\ \mbox{listcons}\ \tau
    \end{array}\\[.5cm]
    \begin{array}{c}
      a:\ \mbox{listcons}\ \tau\\
      \hline
      a:\ \mbox{list}\ \tau
    \end{array}
  \end{array}
\]
\caption{\emph{value-has-ty} inference rules}
\label{fig:list_machine_value_has_ty}
\end{figure}

The first implementation of the \emph{preservation} property with \aCheck\ did not find any counterexample. That was caused by a bug contained in the implementation of the existential quantifier present in the property postcondition. Then, counterexamples emerged with the FsCheck version of the property. Listing \ref{lst:list_machine_value_has_ty_counterexample} represents a clear counterexample found by the \emph{preservation} check.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_value_has_ty_counterexample},caption={a counterexample to the \emph{preservation} property found by FsCheck}]
p = Prog [
  (Id 0, Seq (
    Cons (Name 0, Name 0, Name 0),
    Jump (Id 1)
  ));
  (Id 1, Seq (
    FetchField (Name 0, Head, Name 0),
    Jump (Id 2)
  ));
  (Id 2, Halt)
]

pi = PI [
  (Id 0, TEnv [(Name 0, Ty_nil)]);
  (Id 1, TEnv [(Name 0, Ty_listcons Ty_nil)]);
  (Id 2, TEnv [(Name 0, Ty_nil)])
]

l = Id 1
i = Seq (FetchField (Name 0, Head, Name 0), Jump (Id 2))
g = TEnv [(Name 0, Ty_listcons Ty_nil)]
r = Env [(Name 0, Cell (Cell (Nil, Nil), Nil))]

// after step
i' = Jump (Id 2)
r' = Env [(Name 0, Cell (Nil, Nil))]
\end{lstlisting}
\end{figure}

The counterexample was further investigated by analyzing the possible shapes an environment $\mathit{\Gamma'}$ must have to satisfy the property postconditions:

\begin{itemize}
  \item $\mathit{\Gamma'} = [(v_0,\ \mbox{nil})]$: an environment with this shape satisfies the \emph{check-block} predicate, because the target environment of the \texttt{Jump (Id 2)} instruction is a supertype of $\mathit{\Gamma'}$ (actually it is the same), but it is not a compatible environment for the store $r'$, so this case is excluded;
  \item $\mathit{\Gamma'} = [(v_0,\ \mbox{listcons}\ \tau)]$: on the contrary, this environment has the correct type for store $r'$, for any $\tau$ as follows from the inference rules, but $\mathit{\Gamma'}$ is not a sub-environment of the jump target environment;
  \item $\mathit{\Gamma'} = [(v_0,\ \mbox{list}\ \tau)]$: in this case, the same reasoning of the previous point can be applied.
\end{itemize}

The addition of more entries to environment $\mathit{\Gamma'}$ does not affect the points above. The removal of $v_0$ instead, that is $\mathit{\Gamma'} = [\ ]$, is not applicable because such environment is not a sub-environment of the jump target environment. Therefore no $\mathit{\Gamma'}$ exists such that the postconditions are satisfied.

This counterexample was ported to \aProlog: the query represented in Listing \ref{lst:list_machine_value_has_ty_counterexample_aprolog} applied to it confirmed the reasoning explained above.

\begin{figure}[ht!]
\begin{lstlisting}[label={lst:list_machine_value_has_ty_counterexample_aprolog},caption={verifying the counterexample within \aProlog}]
? Pi = [
    (l0, [(v0 ,ty_nil)]),
    (l1, [(v0, ty_listcons(ty_nil))]),
    (l2, [(v0, ty_nil)])
  ],
  I = instr_jump(l2),
  R = [(v0, value_cons(value_nil, value_nil))],
  G = [(v0, T)],
  env_ok(G),
  store_has_type(R, G),
  not(check_block(Pi,G,I)).
% Yields "Yes.", with T = ty_listcons(ty_nil)
\end{lstlisting}
\end{figure}

After this evidence, the \emph{preservation} property for \aCheck\ was reformulated by using an exhaustive bounded search for the existential quantifier. The new version of the check began finding counterexamples, therefore the inference rules of \emph{value-has-ty} were changed by introducing a recursive call to the listcons case, which are shown in Figure \ref{fig:list_machine_new_value_has_ty}: the last rules now take in consideration the types of the values contained by a cons.

\begin{figure}[ht!]
\[
  \begin{array}{c}
    \begin{array}{c}
      \ \\
      \hline
      \mbox{nil}: \mbox{nil}
    \end{array}\\[.5cm]
    \begin{array}{c}
      \ \\
      \hline
      \mbox{nil}: \mbox{list}\ \tau
    \end{array}\\[.5cm]
    \begin{array}{c}
      a_0: \tau \quad a_1: \mbox{list}\ \tau \\
      \hline
      \mbox{cons}(a_0, a_1): \mbox{listcons}\ \tau
    \end{array}\\[.5cm]
    \begin{array}{c}
      a_0: \tau \quad a_1: \mbox{list}\ \tau \\
      \hline
      \mbox{cons}(a_0, a_1): \mbox{list}\ \tau
    \end{array}
  \end{array}
\]
\caption{\emph{value-has-ty} new inference rules}
\label{fig:list_machine_new_value_has_ty}
\end{figure}

This change was also confirmed by inspecting the Coq list-machine implementation, which defines the \emph{value-has-ty} predicate following the same updated inference rules.

\section{Future Work}

The list-machine model used as reference in this thesis was further developed in \citep{listmachine}, by adding \emph{indirect jumps} to its instruction set. Evolving our list-machine implementations could be a chance of studying how the respective tools respond to specifications changes of the model under test. What we already expect, is that the F\# version will be harder to adapt to the new requirements because of custom data generators and shrinkers, which have to be updated too.

In addition to type soundness we want to address more intensional properties: we consider implementing an abstract machine with dynamic  \emph{secure information-flow control} (IFC), a benchmark proposed in \citep{ifc} for assessing PBT random generation strategies. \citep{ifc}  claim that, for the hardest-to-find bugs, the minimal counterexamples are too large and well beyond the scope of naive exhaustive testing. Thus we may want to find out how well \aCheck\ can perform in this challenging case study.

More ambitiously, we may try to re-discover some of the bugs that were present in the specification of \emph{WebAssembly}, as found in its remarkable formalization in Isabelle \citep{webassembly}.

On the foundational level we will explore ways to improve \aCheck, along the following lines:
\begin{itemize}
\item employ sample \emph{size}, i.e.\ the number of constructors appearing in a value, instead of \emph{depth} as a measure for data generation bound, in a way similar to \emph{Feat} \citep{feat};
\item add \emph{random} testing, possibly changing the search behavior of the tool with a notion of random \emph{backchaining}, following \citep{pltredexconstraintlogic}, or modularizing search from iterative-deepening to more sophisticated search strategies as in \emph{Tor} \citep{tor};
\item finally, adding a way to \emph{explain} the origin of counterexamples found by the tool, that is a notion of \emph{declarative debugging} \citep{algorithmicdebugging}, so as to help the \emph{semantic engineer} to locate and repair her specifications.
\end{itemize}
