Does this sound familiar? You are in the middle of a long but supposedly
straightforward formal proof trying to drag your favourite proof
assistant %-- or at least, it used like that until this very moment --
to confirm the blindingly obvious truth of the result, when you get
stuck in an unprovable part of the derivation; you realize that the
statement needs to be adjusted or more commonly that there is
something afoul, typically a banality, in your specification. If only
you had a way to realize that the theorem was unprovable before
wasting precious time in a doomed proof attempt, perhaps some kind of
\emph{testing} \dots

\begin{sloppypar}
This is where, with due respect to Dijkstra and his ``thou shall not
test'' commandment, \emph{validation} prior to theorem proving comes
in: as a matter of fact, such tools have been available for some time
in the leading proof assistants,
e.g.~\cite{BlanchetteBN11,foundationalpbt}.
\end{sloppypar}
In this paper we are not concerned with the issue of verification vs.\
validation in general, but in a particular domain: the mechanization
in a logical framework of the meta-theory (MMT) of programming languages
(PL) and related calculi. To fix ideas, think about the formal
verification of compiler correctness~\cite{Leroy09}. As most
practitioners may testify, the main properties of interest are well
known and have mathematically shallow proofs. The difficulty lies in
the potential magnitude of the cases that one must consider and in the
trickiness that some encodings require. Here, very minor mistakes in
the specification, even at the level of what we would consider a typo,
may severely frustrate the verification effort, to the point to make
it not cost-effective.
%
Further, we aim to support the designer of PL artifacts in her work in
\emph{developing}, and eventually formally proving correct, such
calculi, rather than concentrating on representations that we already know to
be correct.


The technique we adopt is \emph{property-based testing}
(PBT), which successfully combines two very old ideas: automatic test
data generation and  refuting executable specifications. Pioneered
by \emph{QuickCheck} for functional programming~\cite{quickcheck}, PBT
tools are now available for pretty much every programming language and
having a growing impact in
industry~\cite{quickcheckfunprofit}\footnote{An exception is the logic
  programming world, where the porting of QuickCheck to
  SWI-Prolog~(\url{http://www.swi-prolog.org/pack/list?p=quickcheck})
  seems broken, nor has the work in~\cite{QCProlog} been released.}.
%
To make this concrete, consider the following
snippet of a (faulty) Prolog specification of an ordered list and a check
validating the preservation of order by insertion (we are using the concrete syntax of \aCheck, more on this later):
\begin{small}
\begin{lstlisting}
ordered([]).
ordered([X]). 
ordered([X,Y|Xs]):-less_equal(X,Y),ordered(Xs). % should be ordered([Y|Xs]).

insert(X,[],[X]).
insert(X,[Y|Ys],[X,Y|Ys]):- less_equal(X,Y).
insert(X,[Y|Ys],[Y|Zs])  :- greater(X,Y), insert(X,Ys,Zs).

#check "ins_corr" 15: ordered(Xs), insert(X,Xs,Ys) => ordered(Ys).

Checking depth 1 2 3 4 5 6 7 8 9 10. Total: 0.012 s:

X = z 
Xs = [z,s(z),z] 
Ys = [z,z,s(z),z] 
\end{lstlisting}
\end{small}
The tool reports a minimal counterexample to the conjecture,
namely a substitution that verifies the antecedent but not the
consequent.


%\smallskip

PBT is also applicable at the meta-programming level, that
is where the properties of interest are the theorems that some PL
artifact (think again about a compiler) must satisfy. Significant examples
in this area can be found
in~\cite{Klein12,FachiniM17}. % of tools such as \emph{PLT-Redex}
% (\url{https://redex.racket-lang.org})

$\alpha$Check~\cite{alphacheck} is a light-weight PBT tool built on top of $\alpha$Prolog~\cite{nominallogicb}, a
logic programming language based on \emph{nominal} logic. The latter
is particularly suited to represent object logics where \emph{binders}
and associated notions such as $\alpha$-equivalence, (declarative)
generation of \emph{new} names, capture-avoiding substitutions are
paramount. In contrast to QuickCheck, $\alpha$Check uses its logic
programming engine to perform \emph{exhaustive symbolic} search for
counterexamples, as we elaborate further in Section~\ref{ssec:acheck}.
%

In this paper, we take \aCheck beyond the comfort of PBT of high-level
object languages, which have been investigated extensively
(\url{https://github.com/aprolog-lang/checker-examples}), and address
the validation of \emph{abstract machines} and (typed) assembly
languages~\cite{attapl}. This domain seems more challenging from a
validation standpoint, since instructions operating at a low level
provide less ``structure'' while searching for counterexamples, which
then tend to be substantially more complex. Further, it seems hard to
generate meaningful sequences of machine states and machine runs so
that properties may be validated with a reasonable coverage. Similar
remarks have appeared in the context of PBT of C
programs~\cite{csmith}. Not coincidentally, the authors of~\cite{ifc}
suggests that ``[counterexamples to properties such as dynamic
non-interference in abstract machines may be] well beyond the scope of
naive exhaustive testing'' (op.cit.\ pag.~12).

We validate with \aCheck Appel and al.'s list-machine
benchmark~\cite{listmachine} (CIVmark, \textbf{C}ompiler
\textbf{I}mplementation \textbf{V}erification). We concentrate mostly
on its base version $1.0$, but we also touch on $2.0$, see
Section~\ref{ssec:lm2.0}. CIVmark is conceived as a benchmark for
``machine-checked proofs about real compilers''.  Two implementations,
including sketch of proofs of type soundness in the proof assistants
Twelf and Coq come with the paper~\cite{listmachine}.

% A version of this model is also part of the benchmarks distribution of
% PLT-Redex~\cite{redexlm}
% and as such being subjected to some  PBT. All the
% more reasons to tackle with two very distinct PBT's approaches, which
% fits well with the existing formalizations:
% \begin{metanote}
%   change
% \end{metanote}
% \begin{enumerate}
% \item An encoding in the nominal logic programming
%   $\alpha$Prolog~\cite{nominallogicb}, to be tested with its model
%   checker \aCheck~\cite{alphacheck};
% \item A functional encoding with the PBT library
%   \emph{FSCheck} for \emph{F\#}.
% \end{enumerate}
%

PBT's data generation strategy comes in other flavours, beyond the
exhaustive one taken by \aCheck: random~\cite{quickcheck} and  the
combination of the two based on functional enumeration~\cite{Feat},
with an ever increasing emphasis on coroutining the generation and
testing phases~\cite{FetscherCPHF15,LampropoulosPP18}.  % An analysis of
% some of those strategies in a functional setting as applied to MMT was
% carried out in~\cite{FachiniM17}.
% put back the above
%
\aCheck is unique in taking the logic programming way, which naturally
supports many of the optimizations currently researched in the
functional setting; the latter is re-discovering logic programming
features such as mode inference~\cite{bulwahn2011smart} or
CLP~\cite{FetscherCPHF15}. Another contribution of this paper is to compare the
merits of exhaustivity-based PBT in a logic programming style versus
the more usual randomized functional setting popularized by
QuickCheck. It is also a stress test for \aCheck, since CIVmark does
not exploit any of the features offered by nominal logic that makes
\aProlog effective. % ,
% which are all devoted to high-level languages, including some studied
% in~\cite{Klein12}; instead, the CIVmark benchmark brings to the fore
% the naivete of \aProlog's search strategy.

A testing approach is ``good'' only if it uncovers bugs, and so we
did: we falsified the type preservation property as presented in the
paper~\cite{listmachine}, caused by an incorrect specification of
typing for values, see Section~\ref{ssec:value-has}.  This is
particularly striking, considering the paper comes with two
\emph{formalized} proofs of the main theorems claimed for the
list-machine model.  The mystery disappears once we realized that the
Coq implementation of that judgment was different from what was
reported in the paper.  We also found several typos and ambiguities in
the typing rules in the published paper, but this is not unusual where
there is no connection between the text in the paper and the model
verified by the proof assistant.

That was encouraging, but to assess further the efficacy of our
approach, we resorted to  \emph{mutation
  testing}~\cite{softwaretesting} of the given debugged model. To
gauge the trade-off between exhaustive and randomized data generation,
we encoded the same model in the strict functional language \emph{F\#}
and tested with its checker \emph{FsCheck}
(\url{https://fscheck.github.io/FsCheck}). As we will see in
Section~\ref{sec:exper}, exhaustive generation, in all its naivety,
tends to be more cost effective than pure random PBT. Finally, we
report on some ongoing work on the list-machine $2.0$ and on replaying
the first section of~\cite{ifc}. Here again, exhaustive PBT shows
its colours.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "cilc18"
%%% End:

%  LocalWords:  NitPick Quickcheck QuickChick PBT MMT PLT si parva
%  LocalWords:  licet componere magnis Appel CIVmark ompiler POPLMark
%  LocalWords:  mplementation erification cdr Twelf FSCheck FsCheck
%  LocalWords:  QuickCheck CLP SWI
