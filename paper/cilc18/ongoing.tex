Here we very briefly report on extending the list machine model to its
2.0 version, see
\url{http://www.cs.princeton.edu/~appel/listmachine/2.0/} and on
replicating some of the results in~\cite{ifc}. % In both cases, we fixed
% \aCheck as the testing approach: in the first case, to showcase how
% nice it is not having to adapt generators and shrinkers and in the
% second one because the random approach has already been thoroughly
% investigated by much smarter people than us.
\subsection{List-machine 2.0}
\label{ssec:lm2.0}

In the final part of the paper~\cite{listmachine}, the model is
extended to cater for \emph{indirect} jumps. This entails some small
but far-reaching changes: we coerce labels into values and replace the
\textit{nil} value with the initial label $\mathtt{l_0}$. We therefore
generalize the \textbf{jump} instruction and add a way to get the
current label. This yields the following changes to the operational
semantics:
\begin{small}
  \[
    \begin{array}{ccc}
\infer[\mbox{step-jump}]{\smallstep{r}{\mbox{\textbf{jump}}\ v}{p}{r}{\iota'}}{r(v) = l\quad p(l) = \iota'}
      &&
         \infer[\mbox{step-get-lab}]{\smallstep{r}{\mbox{\textbf{get-label}}\ l\ v ; \iota}{p}{r'}{\iota}}{r[v := l] = r'}
    \end{array}
    \]
\end{small}
We also modify the type system:
\[
\begin{small}
  \begin{array}{c}
    \infer[  \mbox{check-block-jump2}]{ \checkblock{\Pi}{\Gamma}{\mbox{\textbf{get-label}} \ l\ v ;\  \mbox{\textbf{jump }} v}}
    { \Pi(l) = \Gamma_1 \quad \Gamma \subset \Gamma_1\quad v\not\in \mathrm{dom(\Gamma_1)}}
  \end{array}
  \end{small}
\]

Updating the \aProlog implementation to reflect those changes is a
matter of half an hour and we refer to the online documentation for
all the details
(\url{https://bitbucket.org/fkomauli/list-machine/branch/2.0}). As the
paper does not spell out the soundness proof, the first thing we did
was running our PBT suite to validate our implementation of the
extension \dots and preservation as formulated beforehand fails! The
pretty-printed counterexample
%
\begin{small}
\[
  \begin{array}{l}
  p\ =\ L_0:\ \mbox{\textbf{halt}}\\
  \Pi\ =\ L_0:\ [v_0\ \mapsto\ \mbox{nil}]\\
  \iota\ =\ \mbox{\textbf{get-label}}\ L_0\ v_1;\ \mbox{\textbf{jump}}\ v_1\\
  \iota'\ =\ \mbox{\textbf{jump}}\ v_1
  \end{array}
\]
\end{small}
points this time not a \emph{specification} error, but the fact that the
\emph{statement} of the theorem has to be generalized as well. In
fact, in the new typing rule both instructions have to occur in
sequence: if we take a single step after \textbf{get-label}, no
typing rule will match the remaining \textbf{jump}, thus making the
typing requirement in the existential conclusion fail. We modify the
statement of preservation so as to try another optional step to ``get
over'' the \textbf{jump}.

\begin{small}
\begin{lstlisting}
pred step_at_most_once (program, store, instr, store, instr).
step_at_most_once(P, R, I, R, I).
step_at_most_once(P, R, I, R', I') :- step(P, R, I, R', I').

pred exists_env_opt(int, program, program_typing, store, instr).
exists_env_opt(N,P,Pi,R,I) :-
    step_at_most_once(P, R, I, R', I'),
    exists_env_for_store_and_block(N, Pi, R', I').

#check "preservation" 13 :
 ... % as before
      => exists_env_opt(5, P, Pi, R', I').
\end{lstlisting}
\end{small}
%
After this modification, \aCheck did not find any other
counterexamples w.r.t.\ the $2.0$ model.

\subsection{Testing non-interference, not so quickly}
\label{ssec:tniq}
\input tniq
% In addition to type soundness we want to address more intensional properties: we consider implementing an abstract machine with dynamic \emph{secure information-flow control} (IFC), a benchmark proposed in~\cite{ifc} for assessing PBT random generation strategies. \cite{ifc} claim that, for the hardest-to-find bugs, the minimal counterexamples are too large and well beyond the scope of naive exhaustive testing. Thus we may want to find out how well \aCheck\ can perform in this challenging case study.

% The abstract machine is composed by a program counter, a stack, and a random-access memory of a given immutable size. It operates on integer values, labeled either \emph{low} (public) or \emph{high} (private), and its instruction set includes: \emph{noop}, \emph{push}, \emph{pop}, \emph{load}, \emph{store}, \emph{add}, and \emph{halt}. More instructions are added in subsequent versions of the machine, such as \emph{jump}. The property that the abstract machine must satisfy is \emph{end-to-end noninterference}: two \emph{indistinguishable} programs that both reach a \emph{halt} instruction must have two indistinguishable halting states, that is for each memory position the contained values must be either labeled \emph{high} or equal.

% In \cite[Section 2.4]{ifc}, the authors list a first draft of the
% inference rules for the \emph{step} relation. Within a minute of check
% execution time, the \emph{end-to-end noninterference} property is falsified. This naive approach worked
% for the first two bugs presented in the paper.
% To find the third bug, we introduced a generator that yields more structured programs, such as sequences of \emph{push} and \emph{store} instructions so values in memory can change during execution, a necessary condition to find distinguishable states.
% The fourth bug was out of reach of \aCheck, but it was instead found using a plain \aProlog query. After the last bug has been fixed, the query does not find any counterexample and completes in about 5 minutes.
% The IFC benchmark authors introduced three more errors to the four bugs mentioned before, to better assess their PBT implementation. We experimented the previous approaches (naive check, check with a generator and query with a generator) with those errors too, where the last mentioned approach was the most successful in exposing all the introduced errors.

% The second version of the abstract machine includes a \emph{jump} instruction, which sets the PC with the value on top of the stack. The \emph{end-to-end noninterference} property run without a generator did not find any counterexample after nearly one hour of execution time. The program generator used by the query had to be adapted to the new definition of the abstract machine. A new generator was designed to include \emph{jump} instructions and to manage numbers size directly: \emph{push} parameters can be limited upto the program length to make them safe jump addresses. After about 25 minutes the query finds a counterexample similar to the one represented in \cite[Figure 11]{ifc}.
% The solution proposed by the authors of the benchmark is to label the PC too. With the new definition, machine states are indistinguishable when either both PC are labeled \emph{high}, or when bot PC are labeled \emph{low} and stores and programs are indistinguishable. After about 15 seconds of execution, the query finds a counterexample where the machines terminate with different PC labels. However in \cite[Section 5.2]{ifc} the authors weaken the \emph{end-to-end noninterference} property by considering only ending states that are both halting and with a PC labeled \emph{low}. With this new weakened property, the \aProlog\ query is not capable of finding a counterexample within several hours of execution.

%In our opinion, \aProlog\ and \aCheck\ still have to be refined, but can already produce some valuable results.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pbt"
%%% End:

%  LocalWords:  IFC PBT Peano upto ccc dom
