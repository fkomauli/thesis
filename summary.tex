\documentclass{article}

\usepackage[a4paper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[hidelinks]{hyperref}
\usepackage{url}
\usepackage{amssymb,amsmath,amsthm}
\usepackage{listings}
\usepackage{natbib}
\usepackage{graphicx}

\newcommand{\aProlog}{{$\alpha$Prolog}}
\newcommand{\aCheck}{{$\alpha$Check}}

\begin{document}

\title{Property-Based Testing Abstract Machines}
\author{Francesco \sc{Komauli}}

\maketitle

In recent years we have witnessed an increasing attention toward \emph{machine-checked meta-theory}. The authors of the POPLMark Challenge \citep{poplmark}, after reviewing  theorem proving technology applied to different problems concerning \emph{programming languages theory}, concluded that such technology has reached the point where it can begin to be widely used by language researchers. The Challenge proposed a set of benchmarks designed to evaluate the state of automated reasoning about the meta-theory of programming languages. The final purpose pursued by the authors is \emph{``making the use of proof tools common practice in programming language research''}.

Formal machine-checkable proofs are the most trustable evidence of
correctness of a software model. However, existing proof assistant tools have
steep learning curves and, moreover, verifying the properties of a
non-trivial model through interactive theorem-proving with such proof
assistants requires a significant effort. Proof attempts are not the
best way to debug an incorrect model, because of the little
assistance given by proof assistants in such cases. The drawbacks of
formal verification with proof assistants lead to alternative
verification techniques, such as \emph{model-checking}
\citep{modelchecking}. In model-checking, the user efforts are limited
to the specification of a model and to the description of properties
it should satisfy, while searching for counterexamples or determining
their non-existence are tasks delegated to an automatic tool, which
employ advanced techniques for searching the finite state space
efficiently. However, programming languages models live
in infinite state space and model-checking techniques have to
be adapted, by introducing a bound which limits the search to a subpart
of the state space. Bounded model-checking provides some confidence that the design
of a model is correct, though not the full confidence that formal
verification would give. One of the most appealing outcomes of bounded
model-checking is that it provides explicit counterexamples, thus
making it more likely to be helpful than verification during the
development of a model.

A form of bounded model-checking is \emph{property-based testing}, which originated with the testing library QuickCheck \citep{quickcheck}, developed for the Haskell functional language and designed to formulate and test properties of programs. Instead of unit testing program functions with isolated data points, the expected properties are defined as functions themselves. The task of the testing library is to generate random data based on the input types of a property, repeating the process until the property fails, or a fixed amount of iterations has completed successfully, or a given timeout is reached. The task  of a property checker is to find a value such that, for a given property, the preconditions are satisfied and the postcondition is not.

Several portings of the QuickCheck library were made to different
functional languages, such as
ScalaCheck\footnote{\url{https://www.scalacheck.org/}} for Scala,
Quviq QuickCheck \citep{quickcheckfunprofit} for Erlang, and FsCheck\footnote{\url{https://fscheck.github.io/FsCheck/}}
for F\#, which is also usable from C\# projects. Other libraries have
been developed keeping the same PBT idea, but implementing a
different way of generating data, such as exhaustive generators as in
the SmallCheck library \citep{smallcheck} or constrained generators in
\aCheck\ \citep{alphacheck}.

\smallskip

This thesis wants to be a contribution in this complementary approach to
formal verification of programming languages meta-theory: the use of
PBT prior to theorem proving to find errors earlier
in the verification process. Moreover, the attention is directed
towards \emph{typed assembly languages} and \emph{abstract machines}
\citep[Chapter~4]{attapl} instead of addressing high-level languages.

Our work focus on the \emph{list-machine} benchmark
\citep{listmachine}, formulated to compare theorem-proving systems on
their ability to express proofs of \emph{compiler correctness}. The
list-machine assembly language consists of a small instruction set,
comprising instructions for fetching values from memory and storing them
back, and for (un)conditional branches; it includes a simple type
system that classifies list types, together with a subtyping
relation. The list-machine has been also used as a benchmark by
PLT-Redex \citep{pltredex0}, the leading environment for experimenting
with programming language design.

\medskip

We have realized two separate implementations of the list-machine:
\begin{itemize}
\item a relational implementation with the logic programming language
  \aProlog\ \citep{nominallogic}, and his model checker
  \aCheck\ \citep{alphacheck};
\item a functional implementation developed in F\#, a functional
  language of the SML lineage, with random generation offered by
  FsCheck library.
\end{itemize}

\aProlog\ and \aCheck\ were designed to model check high-level
languages. The proposed benchmark is somehow a new challenge, which
goes out of their ``comfort'' zone, providing therefore a challenge to
their expressiveness and usability. But the main reason that motivated
the realization of two different implementations was to experimentally
validate the  merits of bounded exhaustive generation strategy
against the random generation approach:
\begin{itemize}
  \item according to the \emph{small scope hypothesis} \citep{smallscopehypothesis}, falsifiable properties are likely to have small counterexamples, thus the generation of large input values is not strictly necessary;
  \item writing custom generators for random data tends to be a time
    expensive task, even requiring more efforts than implementing the
    abstract machine model itself;
  \item while more features are added to the abstract machine, the complexity of random generators grows making their implementation more prone to errors;
  \item custom random data generators may not be coherent with the
    model under test, for example when a generator contains an error,
    or the data distribution is not adequate and possible
    counterexamples end up never being supplied to a falsifiable
    property.
\end{itemize}

In the original benchmark \citep{listmachine}, the authors implemented
the abstract machine in the proof assistants Twelf and Coq, to verify
the overall soundness of the type system. They proved the main theorems
of interest, namely \emph{progress},
\emph{preservation}, together with a set of lower level lemmas which
verify implementation details. We used the statements of theorems and
lemmas as properties to be checked. We also investigated an important
aspect of PBT applied to mechanized meta-theory: whether the checks
based on top level theorems are sufficient to find even the most
subtle bugs, or if checking specific sub-parts of the implementation
is more effective.

The PLT-Redex \citep{pltredex0} implementation of the list-machine
benchmark contains a small set of unit tests. We have used them to assess
 the capability of PBT  in finding errors by comparing
their results with those achieved by unit tests. In other terms, we have
used \emph{mutation testing} to probe the quality of PBT.
%
We inserted mutations ``by hand'', because there is no automatic tool,
nor theory, for our problem domain, and anyway, even the PLT-Redex
benchmark contains just three manual mutations% , which we have ported to the
% \aProlog\ and F\# implementation
. We added other mutations, some of
them being  actual bugs we introduced unintentionally while
implementing the \aProlog\ version.

%\medskip

 We then empirically evaluated the performance of the two
testing strategies with our mutations. Interestingly, the more
promising results were achieved by the \aProlog\ list-machine
version. In fact, checks written in \aProlog\ performed slightly
better than the relative ones implemented in F\#. % After separate conclusions were drawn for each implementation, w
We can give a comparison of advantages and disadvantages that were experienced in each version:

\begin{description}
\item[Fast prototyping] Inference rules written on paper are the main
  reference for implementers that want to encode the specified
  model. One of the most valuable characteristics of the relational
  style of encoding is the similarity with such inference rules given
  as formulas, and more importantly, it allows us to show that the
  translation is adequate in a straightforward way.
%
  Writing properties in a logic language and in a functional one does
  not show much differences: PBT libraries for functional languages
  provide a DSL for writing properties akin to Horn logic. However,
  dealing with existential quantification presented some difficulties
  in both paradigms. While properties can be immediately checked with
  the exhaustive generation approach, when using random generation non
  trivial properties need a custom generator to be adequately
  tested. Writing custom generators could be a time expensive task,
  even requiring more efforts than implementing the abstract machine
  model itself. In fact, The F\# list-machine development took more
  time than the \aProlog\ one, mostly because of the design and
  implementation of those generators and shrinkers.
\item[Code maintenance] An abstract machine evolves with time: the
  type system may be extended for example to include security features or to gain
  more expressiveness; language construct may be added or modified to
  adapt the programming language to new idioms. PBT with exhaustive
  generators can be seamlessly extended, while custom random generators and shrinkers
 needs to be re-written, typically in a non-modular fashion: several
  lines of code have to be adapted when the model under test changes
  and, while more and more features are added to the abstract machine,
  the complexity of random generators and shrinkers grows making their
  implementation more prone to errors.
\item[Execution time] Here we do not want to compare how fast checks
  are executed by \aCheck\ and FsCheck, given the obvious difference
  between a industrial-strength compiled language and an academic
  prototype of an interpreter; rather, to understand how each generation
  strategy affects  execution time of checks. The most relevant factor is
  the number of generated input samples that are supplied to a
  property. While with FsCheck a maximum test iterations count can be
  configured, the checks written with \aCheck\ can only specify a
  maximum depth for the exhaustive generation. Such parameter is not
  closely tied to execution time, because the search space of values
  may exponentially explode at any depth: even if within the same
  depth bound, checks duration may vary from milliseconds to hours.
\item[Results clarity] Writing shrinkers that transform large
  counterexamples to ones small enough to be studied in detail in
  search of errors is not a trivial task. If not well planned, the
  chosen shrinking strategy may mostly produce some non optimal local
  minimum. On the other hand, counterexamples found while employing
  exhaustive generation with an iterative deepening search strategy
  are always a global minimum, thus they do not require a shrinker to
  be further reduced.
\end{description}

\smallskip

The thesis has just scratched the surface of the possibilities of PBT
w.r.t.\ the meta-theory of low-level languages. We now list some
avenues for future work.
\begin{itemize}
\item The list-machine model used as reference in this thesis was
  further developed in \citep{listmachine}, by adding \emph{indirect
    jumps} to its instruction set. Evolving our list-machine
  implementations represents a chance of studying how the respective
  tools respond to specifications changes of the model under test. In
  our expectation, the F\# version will fare worse, given the need to
  update custom data generators and shrinkers.
\item In addition to type soundness we want to address more intensional
  properties: we consider implementing an abstract machine with
  dynamic  \emph{secure information-flow
    control} (IFC), a benchmark proposed in \citep{ifc} for assessing
  PBT random generation strategies. \citep{ifc}  claim that, for the hardest-to-find bugs, the minimal
  counterexamples are too large and well beyond the scope of naive
  exhaustive testing. Thus we may want to find out how well
  \aCheck\ can perform in this challenging case study.

\item More ambitiously, we may try to re-discover some of the bugs
  that were present in the specification of \emph{WebAssembly}, as
  found in its remarkable formalization in Isabelle~\citep{webassembly}.

\end{itemize}



\bibliography{thesis}
\bibliographystyle{apalike}

\end{document}
