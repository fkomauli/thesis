Reasons of space %and the desire not to bore the reader silly
suggest
to  present only a selection of all the experiments that we have
carried out. Full details can be found in~\cite{PBTAM}.

\subsection{A cautionary tale}
\label{ssec:value-has}

A testing approach is any good only if it uncovers bugs, but we were
not expecting to find any in the model presented in the paper, which
came equipped with a type soundness proof formalized in two proof
assistants. So, imagine our surprise when \aCheck came up with this
counterexample to the type preservation check \texttt{pres} (pretty-printed here for better reading):
\[
\begin{array}{l}
  p = (l_0: \mbox{\textbf{cons}}(v_0,v_0,v_0);\ \mbox{\textnormal{jump}}\ l_1);\ (l_1: \mbox{\textbf{fetch-field}}(v_0,0,v_0);\ \mbox{\textbf{jump}}\ l_2);\ (l_2; \mbox{\textbf{halt}})\\[.2cm]
  \Pi = (l_0: [v_0 \mapsto \mbox{nil}]);\ (l_1: [v_0 \mapsto \mbox{listcons nil}]);\ (l_2: [v_0 \mapsto \mbox{nil}])\\[.2cm]
  r = [v_0 \mapsto \mbox{cons}(\mbox{cons}(\mbox{nil},\mbox{nil}),\mbox{nil})]\\[.2cm]
  \iota = \mbox{\textbf{fetch-field}}(v_0,0,v_0);\ \mbox{\textbf{jump}}\ l_2\qquad (\mbox{\emph{block referenced by}}\ l_1)
\end{array}
\]
which after a single step from block $l_1$ yields:
\[
\begin{array}{lcr}
  r' = [v_0 \mapsto \mbox{cons}(\mbox{nil},\mbox{nil})] &&
  \iota' = \mbox{\textbf{jump}}\ l_2
\end{array}
\]
However, there can be no $\Gamma'$ satisfying the postconditions: any
such type environment must contain either the binding
$[v_0 : \mbox{listcons}\ \tau]$ or
$[v_0 : \mbox{list}\ \tau]$ to accommodate $r'$, but both would
not be compatible with what is required by the jump.

% The counterexample was further
% investigated by analyzing the possible shapes an environment $\Gamma'$
% must have to satisfy the property postconditions:
% \begin{itemize}
%   \item $\Gamma' = [v_0 \mapsto \mbox{nil}]$: an environment with this shape
%   satisfies the \emph{check-block} predicate, because the target environment of
%   the $\mbox{jump}\ l_2$ instruction is a supertype of $\Gamma'$ (actually it
%   is the same), but it is not a compatible environment for the store $r'$, so
%   this case is excluded;
%   \item $\Gamma' = [v_0 \mapsto \mbox{listcons}\ \tau]$: on the contrary, this
%   environment has the correct type for store $r'$, for any $\tau$ as follows
%   from the inference rules, but $\Gamma'$ is not a sub-environment of the jump
%   target environment;
%   \item $\Gamma' = [v_0 \mapsto \mbox{list}\ \tau]$: in this case, the same
%   reasoning of the previous point can be applied.
% \end{itemize}

% The addition of more entries to environment $\Gamma'$ does not affect the points
% above. The removal of $v_0$ instead, that is $\Gamma' = [\ ]$, is not applicable
% because such environment is not a sub-environment of the jump target
% environment. Therefore no $\Gamma'$ exists such that the postconditions are
% satisfied.

After some soul searching, we zeroed on the encoding of
 the judgment \emph{value-has-ty}, (page 481 of~\cite{listmachine}).
\[
\begin{array}{c}
  \infer[]{ \mbox{nil}:\ \mbox{nil}}{}\qquad
  \infer[]{  \mbox{nil}:\ \mbox{list}\ \tau}{}\qquad
  \infer[]{      \mbox{cons}(a_0,\ a_1):\ \mbox{listcons}\ \tau}{}\qquad
  \infer[]{   a:\ \mbox{list}\ \tau}{a:\ \mbox{listcons}\ \tau}
\end{array}
\]
% \[
%   \begin{array}{c c c c}
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{nil}:\ \mbox{nil}
%     \end{array}&\qquad
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{nil}:\ \mbox{list}\ \tau
%     \end{array}&\qquad
%     \begin{array}{c}
%       \ \\
%       \hline
%       \mbox{cons}(a_0,\ a_1):\ \mbox{listcons}\ \tau
%     \end{array}&\qquad
%     \begin{array}{c}
%       a:\ \mbox{listcons}\ \tau\\
%       \hline
%       a:\ \mbox{list}\ \tau
%     \end{array}
%   \end{array}
% \]
This is an essential component of the definition of store typing
$r: \Gamma$, which is the case iff for all $v\in \mathrm{dom}(r)$, it
holds $r(v) : \Gamma(v)$. The \emph{listcons} case looks fishy, since
it makes no assumption about the types of $a_0$ and $a_1$. Once we
changed that case to:
$$ \infer[]{      \mbox{cons}(a_0,\ a_1):\ \mbox{listcons}\ \tau}{      a_0 : \tau \quad a_1 : \mbox{list}\ \tau}$$
the counterexample failed to show up.  This change was also confirmed
by inspecting the Coq implementation, which defines
\emph{value-has-ty} exactly like that --- the Twelf ``proof'' is
  left as an exercise and the above typing judgment undefined.

We found more issues with the paper: less dramatically, the
\emph{check-instr-branch-listcons} typing rule contains additional
preconditions regarding the jump target environment similar to those
in the \emph{check-instr-branch-list} rule; however they are absent
in the Coq implementation, as they should, considering the
proof of equivalence with the algorithmic version of type checking,
where they are notably absent. Several typos are also present, viz.\ rule
\emph{check-instr-cons0} (Section 8.1) and in the type checking
algorithm (instruction
$\mathtt{typecheck\_instr\ \Pi\ \Gamma\ \iota}$). Typos were spotted
through formalization, not PBT.


The moral is, of course, that if there is no formal connection between
a formalization and the paper reporting it --- Isabelle/HOL and literate
Agda being the precious exceptions --- you may want to be skeptical of the
latter. Similar findings appear in~\cite{Klein12}.



\subsection{Mutation analysis}
\label{ssec:exp}



%and whether they were killed by the top-level checks vs.\ the unit tests.


\begin{table}[ht!]
\[
  \begin{tabular}{| l || c | r | r || c | r | r || c | r | r || c |}
    \hline
    \ & \multicolumn{3}{c||}{Progress} & \multicolumn{3}{c||}{Preservation} & \multicolumn{3}{c||}{Soundness} & UT\\
    \hline
    Mutant & $\otimes$ & Depth & Time (s) & $\otimes$ & Depth & Time (s)& $\otimes$ & Depth & Time (s) & $\otimes$ \\
    \hline
    \hline
    \textit{none} & \ & 13 & 1771 & \ & 13 & 15180 & \ & 25 & 8.098 & \\
    \hline
    \hline
    1 & $\oslash$ & 13 & 451.0 & \ & 13 & 1933 & $\otimes$ & 19 & 0.2910 & $\otimes$\\
    \hline
    2 & \ & 13 & 1260 & $\otimes$ & 13 & 5.517 & $\otimes$ & 23 & 1.794 & \\
    \hline
    3 & \ & 13 & 1330 & $\otimes$ & 13 & 4.632 & $\otimes$ & 23 & 1.811 & $\otimes$\\
    \hline
    4 & \ & 13 & 1524 & \ & 13 & 2147s & \ & 25 & 31.20 & \\
    \hline
    5 & \ & 13 & 1370 & \ & 13 & 2000 & $\oslash$ & 25 & 4356 & \\
    \hline
    6 & \ & 13 & 1188 & $\otimes$ & 13 & 2.168 & $\otimes$ & 23 & 1.881 & $\otimes$\\
    \hline
    7 & $\oslash$ & 13 & 265.0 & \ & 13 & 1571 & $\otimes$ & 23 & 1.751 & $\otimes$\\
    \hline
    8 & \ & 13 & 1275 & \ & 13 & 1851 & $\otimes$ & 23 & 1.786 & \\
    \hline
    9 & $\otimes$ & 13 & 0.01350 & \ & 13 & 1867 & $\otimes$ & 13 & 0.01367 & \\
    \hline
    10 & \ & 13 & 1461 & \ & 13 & 2411 & \ & 25 & 30.88 & \\
    \hline
    11 & $\oslash$ & 13 & 223.0 & \ & 13 & 1478 & \ & 25 & 7.755 & $\otimes$\\
    \hline
    12 & \ & 13 & 1229 & $\oslash$ & 13 & 1345 & \ & 25 & 7.496 & $\otimes$\\
    \hline
    13 & \ & 13 & 1238 & \ & 13 & 1777 & \ & 25 & 7.673 & $\otimes$\\
    \hline
    14 & \ & 13 & 980.0 & \ & 13 & 1798 & \ & 25 & 7.691 & $\otimes$\\
    \hline
    15 & \ & 13 & 1085 & $\otimes$ & 13 & 4.422 & \ & 25 & 7.602 & \\
    \hline
    16 & \ & 13 & 1252 & $\otimes$ & 13 & 21.66 & \ & 25 & 7.655 & $\otimes$\\
    \hline
    17 & \ & 13 & 14.03 & \ & 13 & 258.0 & \ & 25 & 7.171 & $\otimes$\\
    \hline
    18 & \ & \multicolumn{2}{c|}{interrupted} & $\otimes$ & 12 & 3.142 & $\otimes$ & 12 & 0.3680 & \\
    \hline
    19 & \ & 13 & 1269 & \ & 13 & 4853 & $\otimes$ & 19 & 0.4871 & \\
    \hline
    20 & \ & 13 & 37020 & \ & \multicolumn{2}{c|}{interrupted} & $\otimes$ & 13 & 0.06200 & \\
    \hline
  \end{tabular}
\]
\caption{Mutation testing for the list-machine}
\label{tbl:checktimes}
\end{table}


We used our ``home-baked'' mutation testing to assess \aCheck's
ability to kill mutants.  Checks are divided into three groups:
top-level theorems as in our Section~\ref{ssec:checks}, intermediate
lemmas collected in Section 8 of~\cite{listmachine} and auxiliary
checks regarding low-level details of the machine specification. We
have also ported from PLT-Redex~\cite{redexlm} a few dozens unit
tests, as an additional baseline in our analysis. Since those are
just Prolog queries exercising the static and dynamic semantics of the
machine, we have made them \emph{parametric}, so that exhaustive
generation of those parameters would be more far-reaching. The
experimental results are collected in Table~\ref{tbl:checktimes}: rows
list the mutations considered and whether they were killed by our test
suite (marked by $\otimes$). There are columns for the top-level
checks, detailing the time spent (in seconds)
and the maximal depth we allowed, while the rightmost column gives
information about unit tests.
%
We configured the tool so as to make the execution of each check stays withing
ten seconds\footnote{Checks executed on machine with
  Intel® Core™ i5-4-200U CPU, clock speed of 1.60GHz and 8GB of RAM,
  with 64 bit Ubuntu 17.10 Artful and Linux kernel 4.13.0.}, as it is good practice to try and keep the execution time of a test suite
reasonably short, so that it can be run after every change in the model.
%
Exception to this discipline are the \emph{progress} and
\emph{preservation} checks, whose search space size tend to
explode.
% Those checks make execution time of the \aCheck\ suite not very predictable.
% We therefore imposed a one minute  time-out:
Checks that found a counterexample but exceeded a further one minute time-out are
marked with $\oslash$ instead of $\otimes$.
Unit tests (UT) of course, even in a parametric fashion, execute in just few seconds.

% PBT performs fairly well, while being more robust w.r.t.\ code
% evolution and requiring far less programming effort.
As better shown in  Appendix~\ref{tbl:comparison}, top-level checks
kill 12 mutants (withing the strict time out), 10 with  the
\emph{soundness} check alone. The score raises to 15/20 if lemmas are
included and to 18/20 with lower level lemmas. In this sense PBT outperforms
unit tests, which have a 1/2 killing ratio.

\smallskip We are also very interested in comparing exhaustive and
\emph{random} generation for MMT of abstract machines. We thus ported
the benchmark to \emph{F\#} and its PBT tool \emph{FsCheck}. We have
no space to detail this implementation, for which we refer again
to~\cite{PBTAM}. We simply note that, as by now well-known in the
literature~\cite{ifc,Klein12}, random PBT requires a lot of ingenuity
in crafting custom-made generators to ensure even a basic level of
coverage in testing; further, we had to write \emph{shrinkers} to
provide small readable counterexamples. Existentials properties are
also hard to code.

In summary, the FsCheck implementation of PBT is quick (as promised,
although not as much as you would expect), not only because of the
efficiency of the host language, but also thanks to the flexibility of
the configuration of how checks are executed.  The whole FsCheck BPT
suite completes in around 5 minutes: roughly 10 seconds are taken by
top-level checks and about 40 seconds by the intermediate lemmas,
while the remaining time is taken by the auxiliary checks.

We do not compare times to find counterexamples in \aCheck
vs.~FsCheck, because it is a serious case of oranges and apples.  What
we can reasonably compare (Fig.~\ref{fig:comparisonchart}) is the rate of mutants killed by the two
testing approaches and the \aCheck\ implementation comes marginally
ahead. %  Lemmas and low-level checks did not perform well, while
% top-level theorems were capable of killing most mutants. In fact, the
% \emph{soundness} property was the one which killed most and it is a
% good countermeasure against possible errors contained in the
% model.%  This is consistent to the PLT-Redex model, where soundness is
% % the only property checked.
%
Note that we have to discard roughly half of the mutations, as they do
not apply to a functional encoding of the machine. While we do not
want to read too much in such a limited experimentation, it is safe to
say that \aCheck keeps its ground, considering how inexpensive it is
to set up.



\pgfplotstableread[row sep=\\,col sep=&]{
    Suite            & aCheck & FsCheck \\
    Theorems         & 9      & 7      \\
    Lemmas           & 2      & 1      \\
    Auxiliary Checks & 2      & 1      \\
    Unit Tests       & 7      & 7      \\
    }\datacomparison
\begin{small}
\begin{figure}[t]
\begin{tikzpicture}
    \begin{axis}[
            ybar,
            bar width=1cm,
            width=\textwidth,
            height=.5\textwidth,
            legend style={at={(0.5,1)},
                anchor=north,legend columns=-1},
            symbolic x coords={Theorems,Lemmas,Auxiliary Checks,Unit Tests},
            xtick=data,
            nodes near coords,
            nodes near coords align={vertical},
            ymin=0,ymax=11,
            ylabel={Killed Mutants},
        ]
        \addplot table[x=Suite,y=aCheck]{\datacomparison};
        \legend{\aCheck, FsCheck}
        \addplot table[x=Suite,y=FsCheck]{\datacomparison};
    \end{axis}
\end{tikzpicture}
\caption{Mutation analysis with \aCheck and FsCheck}
\label{fig:comparisonchart}
\end{figure}
\end{small}


\pgfplotstableread[row sep=\\,col sep=&]{
    Suite            & aCheck & FsCheck \\
    Theorems         & 9      & 7      \\
    Lemmas           & 2      & 1      \\
    Auxiliary Checks & 2      & 1      \\
    Unit Tests       & 7      & 7      \\
    }\datacomparison
\begin{small}
\begin{tikzpicture}
    \begin{axis}[
            ybar,
            bar width=1cm,
            width=\textwidth,
            height=.5\textwidth,
            legend style={at={(0.5,1)},
                anchor=north,legend columns=-1},
            symbolic x coords={Theorems,Lemmas,Auxiliary Checks,Unit Tests},
            xtick=data,
            nodes near coords,
            nodes near coords align={vertical},
            ymin=0,ymax=11,
            ylabel={Killed Mutants},
        ]
        \addplot table[x=Suite,y=aCheck]{\datacomparison};
        \legend{\aCheck, FsCheck}
        \addplot table[x=Suite,y=FsCheck]{\datacomparison};
    \end{axis}
\end{tikzpicture}
\end{small}


%\pgfplotstableread[row sep=\\,col sep=&]{
%    Theorem      & aCheck & FsCheck \\
%    Progress     & 1      & 1       \\
%    Preservation & 5      & 4       \\
%    Soundness    & 7      & 4       \\
%    Together     & 9      & 7       \\
%    }\theoremscomparison
%    \begin{small}
%\begin{figure}[t]
%\begin{tikzpicture}
%    \begin{axis}[
%            ybar,
%            bar width=1cm,
%            width=\textwidth,
%            height=.5\textwidth,
%            legend style={at={(0.5,1)},
%                anchor=north,legend columns=-1},
%            symbolic x coords={Progress,Preservation,Soundness,Together},
%            xtick=data,
%            nodes near coords,
%            nodes near coords align={vertical},
%            ymin=0,ymax=11,
%            ylabel={Killed Mutants},
%        ]
%        \addplot table[x=Theorem,y=aCheck]{\theoremscomparison};
%        \legend{\aCheck, FsCheck}
%        \addplot table[x=Theorem,y=FsCheck]{\theoremscomparison};
%    \end{axis}
%\end{tikzpicture}
%\caption{Mutation analysis for top-level theorems}
%\label{fig:theoremschart}
%\end{figure}
%\end{small}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pbt"
%%% End:

%  LocalWords:  ty FsCheck cex listcons dom Twelf typecheck PBT sep
%  LocalWords:  postcondition postconditions supertype aCheck PLT BPT
%  LocalWords:  MMT lcr
