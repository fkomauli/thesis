The granddaddy of PBT being QuickCheck, it is natural to look into a
functional implementation of the benchmark to establish a baseline for
\aCheck's efficacy. We chose
\emph{FsCheck} (\url{https://fscheck.github.io/FsCheck}), partly because
we were familiar with it, but mostly because, differently from other
libraries, we hoped we could leverage FsCheck's ability to provide
automatic data generation for any datatype via~.Net reflection to
start spec'n'check without further ado. How wrong we were.

The functional implementation of the machine is unremarkable: we use
F\#'s maps for all sorts of environments and integers for variables and
labels. The \texttt{step} function is total, since type-checking see to that. The type-checking
function follows the imperative specification in the paper (Section 8.2).

This is what a check such as \emph{progress} looks like in
FsCheck's DSL, keeping in mind that  \verb|==>| operationally means:
(lazily) pass to the post-condition all the tests that satisfies the
pre-conditions and discard the other up to a (configurable) limit:
\begin{small}
\begin{verbatim}
      (typecheck-program pi p) && (typecheck-block pi g i) && (store-has-type r g)
         ==>  lazy (step-or-halt p r i)
\end{verbatim}
\end{small}

Having carefully read~\cite{ifc}, we were expecting low coverage for
checks as the above, since uniform distributions are not likely to
find data that has to satisfy severe constraints; still, we were not
quite ready for the number we got: \emph{zero}. FsCheck, as typical in
adaptations of QuichCheck, provides a monadic language to write your
own generators and so we did. In~\cite{ifc}, the emphasis was writing
generators so that the machine would run longer without getting stuck;
here we need to produce well-typed programs, and this is far from
immediate, since we have no type-inference whatsoever. For the
progress property, we need to generate simultaneously a program
\texttt{p}, a program typing \texttt{pi} that type-checks with
\texttt{p}, a store \texttt{r} compatible with a type environment
\texttt{g}, a label \texttt{l} that belongs to program \texttt{p} and
the instruction \texttt{i} associated to label \texttt{l}.

Rather than showing the code, which is available in the
repo~\url{https://bitbucket.org/fkomauli/list-machine/branch/fsharp},
we sketch some of the strategy. The starting point is giving more bias
to \texttt{cons} instructions, so as to populate the store giving a
chance for branching and fetching to do something interesting.  We
 choose an instruction only if we know it is safe to execute in
the given type environment. With the instruction itself, the
generators produces the environment updated after the execution of the
instruction. We then build sequence of non-terminal
instructions. Jumps are delicate: a jump is always directed forward to
a random label with id greater than the current block. If the chosen
landing block has an incompatible typing environment or there is no
label to jump forward to, then an \textbf{halt} instruction is
generated instead. And this is just part of the reasoning behind it.

The smart generators developed for this benchmark consists of over a
hundred lines of very dense F\# code. Even if it sounds like a small
number, the time spent behind it was dozens of hour dedicated to
studying data correlation and distribution to generate meaningful
objects. The process continued by improving distributions and fixing
bugs that were found out by analyzing coverage. Though not a formal
proof, we also validated with \emph{FsCheck} itself the soundness of
generators, as far as  providing data that satisfies the
constraints we imposed. Completeness is out of the question, but even
in systems such as QuickChick~\cite{foundationalpbt} where such proofs
are expressible, this is far from automatic.

%\paragraph{Shrinkers}


\emph{Shrinkers} are a necessary evil of random generation, transforming
large counterexamples into smaller ones that can be understood and
acted upon. The QuickCheck philosophy across the board is to put in the
hand of the user all the hard choices and implementing shrinkers is one.
%
% Thanks to smart generators, checks started to use more structured sample
% programs which satisfied all preconditions and counterexamples began to emerge.
% However, the features that caused the check failures were more likely to be
% contained in large counterexamples, consisting of programs with dozens of
% instructions. Thus we were enforced to developed custom \emph{shrinkers} to reduce
% counterexamples size and isolate such fetaures, so we could make them evident.
%
While shrinking blocks can be achieved by removing non-terminal
instructions, we must safeguard several sources of \emph{correlation}
between data: for example, programs and program typings cannot be
shrunk independently, as we must ensure that they still type-check. For
the latter we wrote a type inference function to generate them together from
scratch. This strategy worked well enough to reduce counterexamples to
sizes sufficiently close to the minimal ones  easily found
with the iterative deepening approach of \aCheck.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pbt"
%%% End:

%  LocalWords:  PBT QuickCheck FsCheck datatype spec'n'check pre repo
%  LocalWords:  FsCheck's QuickChick
