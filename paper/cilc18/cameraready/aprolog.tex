\aProlog~\cite{nominallogicb} is a logic programming language
particularly suited to encoding PL calculi and related systems due to
its support for nominal logic. However, since this case study is
purposely first order only, we did not get to use any of those goodies
and the encoding is many sorted pure Prolog code. In fact, it follows
quite closely the reference Twelf implementation by
Appel~\cite{listmachine}. The only place one would naturally try and
use \emph{name types}, and hence inherit $\alpha$-equivalence, is in
the encoding of variables and labels. Yet, the machine model calls for
\emph{distinguished} (initial) variable and label $\mathtt{v_0}$ and,
$\mathtt{l_0}$ and this suggests an encoding based on an enumeration
of constants --- remember, we are in the business of bounded model
checking, so we tend to avoid using infinite types as integers. The
only downside is the need for an explicit inequality predicate. We use
association lists for all the environments the list-machine uses (note
the Haskell like syntax for lists in type abbreviations) and standard
Prolog predicates for related operations such as looking up or
updating (non-destructively) such a list. Do not be fooled by the
functional notation: it is flattened at compile time to the equivalent
relation. While \aProlog supports polymorphism, the checker does not,
as term generations is type-driven. Note, this is a good thing: any
PBT tool for Prolog will have to come up with some type information,
see~\cite{QCProlog}, which struggles exactly in this regard.


We show some highlights, while the complete code can be found at
\url{https://bitbucket.org/fkomauli/list-machine}.

\begin{small}
\begin{lstlisting}
var : type.
v0 : var.
v1 : var.
...
pred not_same_var (var, var).
not_same_var(v0, v1).
not_same_var(v0, v2).
...
type block =   (label,instr).
type program = [(block)].
type store =   [(var,value)].
...
func var_lookup (store,var) = value.
var_lookup([(V,A)|R],V) = A.
var_lookup([(V,A)|R],V1) = A1 :-
  not_same_var(V,V1), A1 = var_lookup(R,V1).
\end{lstlisting}
\end{small}
The operational semantics encoding is an unsurprisingly translation of
the rules  reported in the electronic appendix into a predicate {\texttt{step
    (program, store, instr, store, instr)}}, where the first three
arguments are inputs. We show the rules mentioned in the previous
Section.

\begin{small}
\begin{lstlisting}
pred step (program, store, instr, store, instr).
step(P, R, seq(instr_fetch_field(V1, zf, V2), I), R', I) :-
    value_cons(A, _) = var_lookup(R, V1),
    R' = var_set(R, V2, A).
step(P, R, seq(instr_fetch_field(V1, sf, V2), I), R', I) :-
    value_cons(_, A) = var_lookup(R, V1),
    R' = var_set(R, V2, A).
step(P, R, seq(instr_cons(V1, V2, V3), I), R', I) :-
    A1 = var_lookup(R, V1),
    A2 = var_lookup(R, V2),
    R' = var_set(R, V3, value_cons(A1, A2)).
...
\end{lstlisting}
\end{small}

Type checking is slightly more interesting: we summarize the main type
definitions with a sample of typechecking instructions. We add their mode declarations, which will become
relevant as soon as we try to test existential properties such
as \emph{preservation}. Note that mode checking is not implemented
yet, but it is in Twelf, which we \emph{mutate mutandis} inherited
from.
\begin{small}
\begin{lstlisting}
pred check_instr (program_typing,env,instr,env).
% mode check-instr +Pi +G +I -G.
check_instr(Pi, G, instr_fetch_field(V, zf, V'), G') :-
    (ty_listcons(T), _) = env_lookup(G, V),
    G' = env_set(G, V', T).
check_instr(Pi, G, instr_fetch_field(V, sf, V'), G') :-
    (ty_listcons(T), _) = env_lookup(G, V),
    G' = env_set(G, V', ty_list(T)).
check_instr(Pi, G, instr_cons(V0, V1, V), G') :-
    (T0, _) = env_lookup(G, V0),
    (T1, _) = env_lookup(G, V1),
    ty_list(T) = lub(ty_list(T0), T1),
    G' = env_set(G, V, ty_listcons(T)).
...
pred check_block (program_typing,env,instr).
%mode check-block +Pi +G +I.
pred check_blocks (program_typing,program).
%mode check-blocks +Pi +P.
pred check_program (program,program_typing).
%mode check-program +P +Pi.
\end{lstlisting}
\end{small}


\newcommand{\andd}{\land}
\newcommand{\impp}{\rightarrow}
\subsection{PBT with \aCheck}
\label{ssec:acheck}
$\alpha$Check~\cite{alphacheck} is a tool for checking desired
properties of formal systems implemented in \aProlog. The idea is to
test properties of the form
$H_1 \andd \cdots \andd H_n \impp A$ by searching \emph{exhaustively}
(up to a bound) for a substitution $\theta$ such that
$\theta(H_1),\ldots,\theta(H_n)$ hold but the conclusion
$\theta(A)$ does not. In this paper, we identify negation with
\emph{negation-as-failure}, but the tool includes also another
strategy based on negation elimination~\cite{Pessina15}. The concrete syntax for a check is
\begin{center}
  \texttt{\#check ``name'' depth: G => A.}
\end{center}
where \texttt{G} is a goal and \texttt{A} an atom or
constraint --- since in this paper we make no use of  nominal
  features, the latter means syntactic equality.  As usual in Prolog the
free variables are implicitly universally quantified and \texttt{depth}
is the user-given bound. The above pragma is translated to the formula
\begin{equation}
  \label{query}
  \exists \vec{X}: \vec{\tau}.\ G \wedge\ \mbox{gen}_{{\tau_1}}({X_1})\wedge\ \ldots \wedge\ \mbox{gen}_{{\tau_n}}({X_n}) \wedge\ \neg A
\end{equation}
where $\mbox{gen}_{{\tau_i}}(X_i)$ are type directed {exhaustive}
generators automatically compiled by the tool to ground the free
variables of $A$, needed to make the use of NF sound. The user
can, alternatively, specify her own generators as \aProlog code, if
she feels she needs to implement a smarter generation strategy, as we
will see shortly and in Section~\ref{ssec:tniq}.

A query such as (\ref{query}) builds all ``finished'' derivations of the
hypothesis $G$ up to the given depth, considers all sufficiently ground
instantiations of variables, and finally tests whether the conclusion
finitely fails for the resulting substitution.
%
\aCheck implements a simple-minded iterative deepening search strategy over a
hard-wired notion of bound, which roughly coincides with the number
of clauses that can be used in the derivation of each of the premises.

Let's look more closely at a check, \emph{progress}. While
\aProlog does not have first class existentials and disjunction, it is
a basic Prolog exercise to code it:
\begin{small}
\begin{lstlisting}
pred step_or_halt (program,store,instr).
step_or_halt(P,R,instr_halt).
step_or_halt(P,R,I) :- step(P,R,I,R',I').

#check "progress" 10: 
    check_program(P, Pi), check_block(Pi, G, I), store_has_type(R, G)
       => step_or_halt(P, R, I).
\end{lstlisting}
\end{small}

\begin{sloppypar}
Keeping in mind that the tool add generators for \texttt{P,R,I} before
trying to refute \verb|step_or_halt(P, R, I)|,  mode analysis for
\texttt{step} suggests that \texttt{R',I'} will be ground when the
partial proof tree for the check is built.
\end{sloppypar}
The same approach will not work for \emph{preservation}:
\begin{small}
\begin{lstlisting}
pred exists_env (program_typing,store,instr).
exists_env(Pi,R,I) :-
  store_has_type(R,G), env_ok(G), check_block(Pi,G,I).

#check "pres" 20 :
    check_program(P, Pi), step(P, R, I, R', I'),
    env_ok(G), store_has_type(R, G), check_block(Pi,G,I)
       => exists_env(Pi, R', I').
\end{lstlisting}
\end{small}
This because \verb|store_has_type(R,G)| expects \texttt{G} to be
ground and we are in effect trying to use \aProlog for an impossible
type \emph{inference} task that makes the checker loop. The solution is
to write a specialized generator \verb|build_env| for type
environments (and recursively, for types and variables). The
peculiarity is that we need to add a \emph{local} depth bound, so that
smallish environments can be built independently from the hard-wired
bound, which is additively distributed along all the atoms in the
check.
\begin{small}
\begin{lstlisting}
pred exists_env_b (int,program_typing,store,instr).
exists_env_b(N,Pi,R,I) :-
  N > 0, exists_env_b(N - 1,Pi,R,I).
exists_env_b(N,Pi,R,I) :-
  N = 0, build_env(N,G), store_has_type(R,G), env_ok(G), check_block(Pi,G,I).

#check "pres_b" 20: ... % as before
                        => exists_env_b(4,Pi,R',I').
\end{lstlisting}
\end{small}
%We  report some empirical result  next. % Section~\ref{sec:exper}.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "cilc18"
%%% End:

%  LocalWords:  Twelf Appel Haskell PBT pragma env spec'n'check
%  LocalWords:  polymorphism mutandis
